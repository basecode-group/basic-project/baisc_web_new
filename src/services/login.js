import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

export async function fakeAccountLogin(params) {
  return request(`${http}/sys/n/oauth/login`, {
    method: 'get',
    params: params,
  });
}

export async function getAccountLogOut(mobile) {
  return request(`${http}/sys/n/oauth/logout`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  });
}

export async function authority() {
  return request(`${http}/sys/n/oauth/login/verify`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  });
}
