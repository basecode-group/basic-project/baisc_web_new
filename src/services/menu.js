import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

// 获取菜单列表
export async function getMenuList(params) {
  return request(`${http}/sys/menu`,{
    params: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 根据id查询菜单详情
export async function getMenuById(params) {
  return request(`${http}/sys/menu/${params.id}`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 保存菜单
export async function saveMenu(params) {
  return request(`${http}/sys/menu`,{
    method: 'POST',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 更新菜单
export async function updateMenu(params) {
  return request(`${http}/sys/menu/${params.id}`,{
    method: 'PUT',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 获取最大排序值
export async function getMaxSort(params) {
  return request(`${http}/sys/menu/max/sort`,{
    params: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 删除用户
export async function deleteMenu(params) {
  return request(`${http}/sys/menu/${params.id}`,{
    method: 'DELETE',
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 根据用户查询菜单
export async function getMenuForUser(params) {
  return request(`${http}/sys/menu/get/for/user`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

//根据角色获取菜单
export async function getMenu4Role(params) {
  return request(`${http}/sys/menu/get/for/role/${params.roleId}`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 保存角色菜单
export async function saveMenu4Role(params) {
  return request(`${http}/sys/menu/save/menu/role/${params.roleId}`,{
    method: 'post',
    data: params,
    requestType: 'form',
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}
