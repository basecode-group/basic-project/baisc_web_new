import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

// 上传网络文件
export async function uploadWebFile(params) {
  return request(`${http}/sys/grant/file/snwFile`,{
    method: 'POST',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}
