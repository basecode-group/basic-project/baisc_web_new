import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';
const { http } = defaultSettings;

// 查询当前用户
export async function queryCurrent() {
  return request(`${http}/sys/user/get`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  });
}

// 查询用户列表
export async function queryUserList(params) {
  return request(`${http}/sys/user`,{
    params: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 根据id查询用户详情
export async function getUserById(params) {
  return request(`${http}/sys/user/${params.id}`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 保存用户
export async function saveUser(params) {
  return request(`${http}/sys/user`,{
    method: 'POST',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 更新用户
export async function updateUser(params) {
  return request(`${http}/sys/user/${params.id}`,{
    method: 'PUT',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 删除用户
export async function deleteUser(params) {
  return request(`${http}/sys/user/${params.id}`,{
    method: 'DELETE',
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 重置密码
export async function resetPwd(params) {
  return request(`${http}/sys/user/reset/pwd/${params.id}`,{
    method: 'PUT',
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 修改密码
export async function updatePwd(params) {
  return request(`${http}/sys/user/update/pwd`,{
    method: 'PUT',
    data: params,
    requestType: 'form',
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}
