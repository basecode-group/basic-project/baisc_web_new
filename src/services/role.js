import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

// 查询角色列表
export async function getRoleList(params) {
  return request(`${http}/sys/role`,{
    params: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 根据id查询角色
export async function getRoleById(params) {
  return request(`${http}/sys/role/${params.id}`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 保存角色
export async function saveRole(params) {
  return request(`${http}/sys/role`,{
    method: 'POST',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 更新角色
export async function updateRole(params) {
  return request(`${http}/sys/role/${params.id}`,{
    method: 'PUT',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 删除角色
export async function deleteRole(params) {
  return request(`${http}/sys/role/${params.id}`,{
    method: 'DELETE',
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 查询角色选择下拉框
export async function getRoleSelectData(params) {
  return request(`${http}/sys/role/select/data`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 获取用户授权角色
export async function getRoleUserAuth(params) {
  return request(`${http}/sys/role/get/user/auth/${params.roleId}`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 保存用户授权信息
export async function saveRoleUserAuth(params) {
  return request(`${http}/sys/role/save/user/auth/${params.roleId}`,{
    method: 'POST',
    data: params,
    requestType: 'form',
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}
