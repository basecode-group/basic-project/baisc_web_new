import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

// 字典列表查询
export async function getDictList(params) {
  return request(`${http}/sys/dict`,{
    params: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 查询字典详情
export async function getDictById(params) {
  return request(`${http}/sys/dict/${params.id}`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 保存字典
export async function saveDict(params) {
  return request(`${http}/sys/dict`,{
    method: 'POST',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 更新字典
export async function updateDict(params) {
  return request(`${http}/sys/dict/${params.id}`,{
    method: 'PUT',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 删除字典
export async function deleteDict(params) {
  return request(`${http}/sys/dict/${params.id}`,{
    method: 'DELETE',
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 获取最大排序值
export async function getMaxSort(params) {
  return request(`${http}/sys/dict/max/sort`,{
    params: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 根据类型获取字典列表
export async function getDictListByType(params) {
  return request(`${http}/sys/dict/list/by/type/${params.type}`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}
