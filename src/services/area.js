import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

// 查询行政区划列表
export async function getAreaList(params) {
  return request(`${http}/sys/area`,{
    params: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 根据id查询行政区划
export async function getAreaById(params) {
  return request(`${http}/sys/area/${params.id}`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 保存行政区划
export async function saveArea(params) {
  return request(`${http}/sys/area`,{
    method: 'POST',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 更新行政区划
export async function updateArea(params) {
  return request(`${http}/sys/area/${params.id}`,{
    method: 'PUT',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// 删除行政区划
export async function deleteArea(params) {
  return request(`${http}/sys/area/${params.id}`,{
    method: 'DELETE',
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}
