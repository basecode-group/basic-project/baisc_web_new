import request from '@/utils/request';

// swagger 资源获取
export async function getSwaggerResource(params) {
  return request(`${params.url}/swagger-resources`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

// swagger get api
export async function getSwaggerApi(params) {
  return request(`${params.url}`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}
