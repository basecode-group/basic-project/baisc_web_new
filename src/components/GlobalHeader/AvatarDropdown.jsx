import {LogoutOutlined, KeyOutlined} from '@ant-design/icons';
import {Menu, Form, Modal, Input, Spin} from 'antd';
import React from 'react';
import {connect} from 'dva';
import {router} from 'umi';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';
import SelectPhoto from "@/pages/common/SelectPhoto";

class AvatarDropdown extends React.Component {
  state = {
    visible: false,
  }

  formRef = React.createRef();

  onLogOut(){
    const {dispatch} = this.props;
    if (dispatch) {
      dispatch({
        type: 'login/logout',
      });
    }
  };

  onUpdatePwd(){
    this.setState({visible: true})
  }

  componentWillMount() {
    const {dispatch} = this.props;
    if (dispatch) {
      dispatch({
        type: 'user/fetchCurrent',
      });
    }
  }

  // 修改密码
  onSubmit() {
    console.log(this.formRef)
    this.formRef.current.validateFields().then(values => {
      const { dispatch } = this.props;
      if (dispatch) {
        dispatch({
          type: 'user/updatePwd',
          params: {
            ...values
          }
        }).then(res => {
          if (res){
            this.setState({ visible: false });
            this.formRef.current.resetFields();
          }
        })
      }
    })
  }

  render() {
    const {
      currentUser = {
        avatar: '',
        name: '',
      },
      menu,
      loading,
    } = this.props;

    const menuHeaderDropdown = (
      <Menu className={styles.menu} selectedKeys={[]}>
        <Menu.Item key="updatePwd" onClick={this.onUpdatePwd.bind(this)}>
          <KeyOutlined/>
          修改密码
        </Menu.Item>
        <Menu.Item key="logout" onClick={this.onLogOut.bind(this)}>
          <LogoutOutlined/>
          退出登录
        </Menu.Item>
      </Menu>
    );

    return (
      <span>
        <HeaderDropdown overlay={menuHeaderDropdown}>
          <span className={`${styles.action} ${styles.account}`}>
            <SelectPhoto photo={currentUser.photo} key={currentUser.photo} size="mini"/>
            <span className={styles.name} style={{marginLeft: 10}}>{currentUser.name}</span>
          </span>
        </HeaderDropdown>
        {/* 修改密码 */}
        <Modal
          title="修改密码"
          visible={this.state.visible}
          onOk={this.onSubmit.bind(this)}
          onCancel={() => this.setState({visible: false})}
          width={600}
        >
          <Form {...layout} ref={this.formRef}>
            <Form.Item name="oldPwd" label="旧密码" hasFeedback rules={[{required: true, message: '旧密码不能为空'}]}>
              <Input.Password/>
            </Form.Item>
            <Form.Item name="newPwd"
                       rules={[
                         {required: true, message: '请输入密码'},
                         {min: 6, message: '最少输入6位数密码'},
                       ]}
                       validateFirst
                       hasFeedback
                       label="密码">
              <Input.Password/>
            </Form.Item>
            <Form.Item name="confirm"
                       rules={[
                         {required: true, message: '请输入确认密码'},
                         {min: 6, message: '最少输入6位数密码'},
                         ({getFieldValue}) => ({
                           validator(rule, value) {
                             if (!value || getFieldValue('newPwd') === value) {
                               return Promise.resolve();
                             }
                             return Promise.reject('两次输入的密码不匹配');
                           },
                         }),
                       ]}
                       validateFirst
                       hasFeedback
                       label="确认密码">
              <Input.Password/>
            </Form.Item>
          </Form>
        </Modal>
      </span>
    )
  }
}

const layout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 7},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 12},
  },
};

export default connect(({user, loading}) => ({
  currentUser: user.currentUser,
  loading: loading.models.user,
}))(AvatarDropdown);
