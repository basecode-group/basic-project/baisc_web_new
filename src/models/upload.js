import { getDictListByType } from '@/services/dict';
import { uploadWebFile } from '@/services/upload'

/**
 * 文件上传查询
 * @type {{effects: {}, namespace: string, reducers: {}, state: {}}}
 */
const uploadModel = {
  namespace: 'upload',
  state: {
    uploadUrl: '',
    visitUrl: '',
    fileUrl: '',
  },
  effects: {
    *getUploadConfig(_, { call, put }){
      const dictResp = yield call(getDictListByType, { type: 'upload_config_type' });
      if (dictResp.code === "00000") {
        const {result} = dictResp;
        yield put({
          type: 'setUploadUrl',
          result: result,
        });
      }
    },
    // 上传网络文件
    * uploadWebFile({ params }, {call, put}) {
      const response = yield call(uploadWebFile, params);
      if (response.code === "00000"){
        yield put({
          type: 'setFileUrl',
          result: response.result.filesMap.file[0].path,
        });
        return response;
      }
    },
  },
  reducers: {
    setUploadUrl(state, { result }){
      const upload_url = result.filter(item => "upload_url" === item.label)
      const uploadUrl = upload_url[0] !== undefined ? upload_url[0].value : 'http://123.57.173.38:8010';
      const visit_url = result.filter(item => "visit_url" === item.label)
      const visitUrl = visit_url[0] !== undefined ? visit_url[0].value : 'http://123.57.173.38';
      return {
        ...state,
        uploadUrl: uploadUrl,
        visitUrl: visitUrl,
      }
    },
    setFileUrl(state, { result }){
      return {
        ...state,
        fileUrl: result,
      }
    }
  },
};
export default uploadModel;
