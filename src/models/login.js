import { stringify } from 'querystring';
import { router } from 'umi';
import { fakeAccountLogin, getAccountLogOut, authority } from '@/services/login';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';

const Model = {
  namespace: 'login',
  state: {
    status: undefined,
    currentUser: {},
    authority: [],
  },
  effects: {
    *login({ payload }, { call, put }) {
      const response = yield call(fakeAccountLogin, payload);
      if (response.code === '00000') {
        // Login successfully
        yield put({
          type: 'changeLoginStatus',
          payload: response,
        });
        // 跳转
        window.location.href = '/';
      }
    },

    *authority(_,{ call, put }) {
      const response = yield call(authority);
      yield put({
        type: 'changeAuth',
        payload: response,
      });
    },

    *logout(_, { call, put }) {
      // 退出登录
      yield call(getAccountLogOut)
      const { redirect } = getPageQuery();
      if (window.location.pathname !== '/user/login' && !redirect) {
        router.replace({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        });
      }
    },
  },
  reducers: {
    changeLoginStatus(state, { payload }) {
      // 设置token
      localStorage.setItem('Authorization', payload.result.tails.access_token);
      return { ...state, currentUser: payload.result};
    },
    changeAuth(state, { payload }) {
      setAuthority(payload.result);
      return { ...state, authority: payload.result};
    }
  },
};
export default Model;
