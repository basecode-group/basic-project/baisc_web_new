import {
  getMenuList,
  getMenuForUser,
  deleteMenu,
  saveMenu,
  updateMenu,
  getMaxSort,
  getMenuById,
  getMenu4Role,
  saveMenu4Role,
} from '@/services/menu';
import { message } from 'antd';

const menuModel = {
  namespace: 'menu',
  state: {
    menuList: [],
    list: [],
    query: {
      pageNum: 1,
      pageSize: 10
    },
    maxSort: 10,
  },
  effects: {
    // 获取菜单列表
    *getMenuList({ params }, { call, put }){
      const response = yield call(getMenuList, params);
      if (response.code === "00000"){
        yield put({
          type: 'setMenu',
          result: response.result,
        });
      }
    },
    *getMenuById({ params }, { call }){
      const response = yield call(getMenuById, params);
      if (response.code === "00000"){
        return response;
      }
      return ;
    },
    // 根据用户查询菜单
    *getMenuForUser(_,{ call, put}){
      const response = yield call(getMenuForUser);
      if (response.code === "00000"){
        yield put({
          type: 'setMenuList',
          result: response.result,
        });
        return response;
      }
    },
    //保存菜单
    *saveMenu({params}, { call, put }){
      const response = yield call(saveMenu, params);
      if (response.code === "00000"){
        message.success("新建菜单成功");
        // 刷新菜单
        yield put({
          type: 'getMenuList',
          params: {},
        });
      }
    },
    //保存菜单
    *updateMenu({params}, { call, put }){
      const response = yield call(updateMenu, params);
      if (response.code === "00000"){
        message.success("编辑菜单成功");
        // 刷新菜单
        yield put({
          type: 'getMenuList',
          params: {},
        });
      }
    },
    // 获取最大排序值
    *getMaxSort({params}, { call, put }){
      const response = yield call(getMaxSort, params);
      if (response.code === "00000"){
        yield put({
          type: 'setMaxSort',
          result: response
        })
        return response;
      }
    },
    // 删除菜单
    *deleteMenu({ params }, { call, put }){
      const response = yield call(deleteMenu, params);
      if (response.code === "00000"){
        message.success("菜单删除成功");
        // 刷新菜单
        yield put({
          type: 'getMenuList',
          params: {},
        });
      }
    },
    // 根据角色获取菜单
    * getMenu4Role({ params }, {call, put}) {
      const response = yield call(getMenu4Role, params);
      if (response.code === "00000"){
        return response;
      }
    },
    // 保存角色菜单
    * saveMenu4Role({ params }, {call, put}) {
      const response = yield call(saveMenu4Role, params);
      if (response.code === "00000"){
          message.success("菜单授权成功！")
      }
    },
  },
  reducers: {
    // 设置菜单
    setMenu(state, { result }){
      return {
        ...state,
        list: result,
      }
    },
    // 菜单列表
    setMenuList(state, { result }){
      return {...state, menuList: result}
    },
    setMaxSort(state, { result }){
      return {...state, maxSort: result.maxSort}
    },
  },
};
export default menuModel;
