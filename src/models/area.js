import {
  getAreaList,
  getAreaById,
  saveArea,
  updateArea,
  deleteArea,
} from '@/services/area';
import { message } from 'antd';

const areaModel = {
  namespace: 'area',
  state: {
    list: [],
    pagination: {
      pageSize: 10,
      current: 1,
      total: 0,
    },
  },
  effects: {
    // 获取行政区划列表
    * getAreaList({ params }, {call, put}) {
      const { parentId, pageSize } = params;
      console.log(parentId, pageSize)
      params['pageSize'] = pageSize !== undefined ? pageSize : 20;
      params['parentId'] = parentId !== undefined ? parentId : '1c4031d4aafd4480ae6d4943dbe4f173';
      console.log(params)
      const response = yield call(getAreaList, params);
      if (response.code === "00000"){
        yield put({
          type: 'setAreaList',
          result: response.result,
        });
      }
    },
    // 根据id获取行政区划详情
    * getAreaById({ params }, {call, put}) {
      const response = yield call(getAreaById, params);
      if (response.code === "00000"){
          return response;
      }
    },
    // 保存行政区划
    * saveArea({ params }, {call, put}) {
      const response = yield call(saveArea, params);
      if (response.code === "00000"){
          message.success('新建行政区划成功');
          yield put({
            type: 'getAreaList',
            params: {
              parentId: params.parentId
            }
          });
          return response;
      }
    },
    // 更新行政区划
    * updateArea({ params }, {call, put}) {
      const response = yield call(updateArea, params);
      if (response.code === "00000"){
          message.success('编辑行政区划成功');
          yield put({
            type: 'getAreaList',
            params: {
              parentId: params.parentId
            }
          });
          return response;
      }
    },
    // 删除行政区划
    * deleteArea({ params }, {call, put}) {
      const response = yield call(deleteArea, params);
      if (response.code === "00000"){
          message.success('删除行政区划成功');
          yield put({
            type: 'getAreaList',
            params: {
              parentId: params.parentId
            }
          });
          return response;
      }
    },
  },
  reducers: {
    setAreaList(state, {result}){
      return  {
        ...state,
        pagination: {
          pageSize: result.size,
          current: result.number + 1,
          total: result.totalElements,
        },
        list: result.content
      }
    }
  },
};
export default areaModel;
