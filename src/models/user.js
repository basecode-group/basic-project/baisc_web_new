import {
  queryCurrent,
  queryUserList,
  getUserById,
  saveUser,
  updateUser,
  deleteUser,
  resetPwd,
  updatePwd,
} from '@/services/user';
import { message } from 'antd';

const UserModel = {
  namespace: 'user',
  state: {
    currentUser: {},
    list: [],
    pagination: {
      pageSize: 10,
      current: 1,
      total: 0,
    },
  },
  effects: {
    * fetchCurrent(_, {call, put}) {
      const response = yield call(queryCurrent);
      yield put({
        type: 'saveCurrentUser',
        payload: response,
      });
    },
    // 查询用户列表
    * getUserList({params}, {call, put}) {
      const response = yield call(queryUserList, params);
      if (response.code === "00000") {
        yield put({
          type: 'setUserList',
          result: response.result,
        });
      }
    },
    // 根据id查询用户详情
    * getUserById({params}, {call, put}) {
      const response = yield call(getUserById, params);
      if (response.code === "00000") {
        return response;
      }
    },
    // 保存用户
    * saveUser({params}, {call, put}) {
      const response = yield call(saveUser, params);
      if (response.code === "00000") {
        message.success("新建用户成功");
        yield put({
          type: 'getUserList',
          params: {},
        });
      }
    },
    // 更新用户
    * updateUser({params}, {call, put}) {
      const response = yield call(updateUser, params);
      if (response.code === "00000") {
        message.success("编辑用户成功");
        yield put({
          type: 'getUserList',
          params: {},
        });
      }
    },
    // 删除用户
    * deleteUser({ params }, {call, put}) {
      const response = yield call(deleteUser, params);
      if (response.code === "00000") {
        message.success("删除用户成功");
        yield put({
          type: 'getUserList',
          params: {},
        });
      }
    },
    // 重置密码
    * resetPwd({ params }, {call, put}) {
      const response = yield call(resetPwd, params);
      if (response.code === "00000") {
        message.success("重置密码成功");
        yield put({
          type: 'getUserList',
          params: {},
        });
      }
    },
    // 修改密码
    * updatePwd({ params }, {call, put}) {
      const response = yield call(updatePwd, params);
      if (response.code === "00000") {
        message.success("密码修改成功");
        return response;
      }
    },
  },
  reducers: {
    saveCurrentUser(state, action) {
      return {...state, currentUser: action.payload.result || {}};
    },
    setUserList(state, {result}) {
      return {
        ...state,
        pagination: {
          pageSize: result.size,
          current: result.current,
          total: result.total,
        },
        list: result.records,
      }
    }
  },
};
export default UserModel;
