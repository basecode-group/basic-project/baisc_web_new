import {
  getDictList,
  getDictById,
  saveDict,
  updateDict,
  deleteDict,
  getMaxSort,
  getDictListByType,
} from '@/services/dict';
import { message } from 'antd';

const dictModel = {
  namespace: 'dict',
  state: {
    list: [],
    pagination: {
      pageSize: 10,
      current: 1,
      total: 0,
    },
    dictListData: [],
  },
  effects: {
    // 查询字典列表
    *getDictList({ params }, { call, put}){
      const response = yield call(getDictList,params);
      if (response.code === "00000"){
        yield put({
          type: 'setDictList',
          result: response.result
        })
      }
    },
    *getDictById({ params }, { call }){
      const response = yield call(getDictById, params);
      if (response.code === "00000"){
          return response;
      }
    },
    *getMaxSort({ params }, { call }){
      const response = yield call(getMaxSort, params);
      if (response.code === "00000"){
        return response;
      }
    },
    *saveDict({ params }, { call, put }){
      const response = yield call(saveDict, params);
      if (response.code === "00000"){
        message.success("新建字典成功");
        yield put({
          type: 'getDictList',
          params: { parentId: params.parentId }
        });
      }
    },
    *updateDict({ params }, { call, put }){
      const response = yield call(updateDict, params);
      if (response.code === "00000"){
        message.success("编辑字典成功");
        yield put({
          type: 'getDictList',
          params: { parentId: params.parentId }
        });
      }
    },
    *deleteDict({ params }, { call, put }){
      const response = yield call(deleteDict, params);
      if (response.code === "00000"){
        message.success("删除字典成功");
        yield put({
          type: 'getDictList',
          params: { parentId: params.parentId }
        });
      }
    },
    // 根据类型获取字典列表
    * getDictListByType({ params }, {call, put}) {
      const response = yield call(getDictListByType, params);
      if (response.code === "00000"){
          yield put({
            type: 'setDictListData',
            result: response.result,
          });
      }

    },
  },
  reducers: {
    // 设置列表
    setDictList(state, { result }){
      return {
        ...state,
        pagination: {
          pageSize: result.size,
          current: result.number + 1,
          total: result.totalElements,
        },
        list: result.content
      }
    },
    setDictListData(state, { result }){
      return {
        ...state,
        dictListData: result,
      }
    }
  },
};
export default dictModel;
