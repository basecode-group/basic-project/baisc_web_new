import { getSwaggerResource, getSwaggerApi } from '@/services/swagger';
import { getDictListByType } from '@/services/dict';

const swaggerModel = {
  namespace: 'swagger',
  state: {
    resource: [],
    visitUrl: '',
  },
  effects: {
    // 查询swagger 资源
    *getSwaggerResource(_, { call, put }){
      // 查询字典
      const dictResp = yield call(getDictListByType, { type: 'swagger_api_url' });
      if (dictResp.code === "00000"){
        const { result } = dictResp;
        const resourceUrl = result.filter(item => "resource_swagger"=== item.label)[0].value;
        const visitUrl = result.filter(item => "visit_swagger"=== item.label)[0].value;
        yield put({
          type: 'setVisitUrl',
          visitUrl : visitUrl,
        });
        // 调用swagger
        const response = yield call(getSwaggerResource, { url: resourceUrl });
        if (response !== undefined){
          response.forEach(item => item.url = visitUrl+item.url)
          yield put({
            type: 'setResource',
            result: response,
          });
          return response;
        }
      }
    },
    * getSwaggerApi({ params }, { call }) {
      const response = yield call(getSwaggerApi, params);
      return response;
    }
  },
  reducers: {
    setVisitUrl(state, { visitUrl }) {
      return {...state, visitUrl };
    },
    setResource(state, { result }) {
      return {...state, resource: result };
    },
  },
};
export default swaggerModel;
