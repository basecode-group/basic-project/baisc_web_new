import {
  getRoleList,
  getRoleById,
  saveRole,
  updateRole,
  deleteRole,
  getRoleSelectData,
  getRoleUserAuth,
  saveRoleUserAuth,
} from '@/services/role';
import {message} from 'antd';

const roleModel = {
  namespace: 'role',
  state: {
    list: [],
    pagination: {
      pageSize: 10,
      current: 1,
      total: 0,
    },
    roleTypeData: [],
    roleData: {
      tails: {}
    },
    authUser: {
      checked: [],
      userList: [],
    }
  },
  effects: {
    // 角色列表查询
    * getRoleList({params}, {call, put}) {
      const response = yield call(getRoleList, params);
      if (response.code === "00000") {
        yield put({
          type: 'setRoleList',
          result: response.result,
        });
      }
    },
    // 根据id 查询角色
    * getRoleById({params}, {call, put}) {
      const response = yield call(getRoleById, params);
      if (response.code === "00000") {
        yield put({
          type: 'setRoleData',
          result: response.result,
        });
        return response;
      }
    },
    // 查询角色选择下拉框
    * getRoleSelectData(_, {call, put}) {
      const response = yield call(getRoleSelectData);
      if (response.code === "00000") {
        yield put({
          type: 'setRoleTypeData',
          result: response.result,
        });
      }
    },
    // 保存角色
    * saveRole({params}, {call, put}) {
      const response = yield call(saveRole, params);
      if (response.code === "00000") {
        message.success("新建角色成功");
        yield put({
          type: 'getRoleList',
          params: {}
        });
      }
    },
    // 更新角色
    * updateRole({params}, {call, put}) {
      const response = yield call(updateRole, params);
      if (response.code === "00000") {
        message.success("编辑角色成功");
        yield put({
          type: 'getRoleList',
          params: {}
        });
      }
    },
    // 删除角色
    * deleteRole({params}, {call, put}) {
      const response = yield call(deleteRole, params);
      if (response.code === "00000") {
        message.success("删除角色成功");
        yield put({
          type: 'getRoleList',
          params: {}
        });
      }
    },
    // 获取用户授权角色
    * getRoleUserAuth({params}, {call, put}) {
      const response = yield call(getRoleUserAuth, params);
      if (response.code === "00000") {
        yield put({
          type: 'setAuthUser',
          result: response.result,
        });
        return response;
      }
    },
    // 保存用户授权信息
    * saveRoleUserAuth({ params }, {call, put}) {
      const response = yield call(saveRoleUserAuth, params);
      if (response.code === "00000"){
          message.success("用户授权成功");
      }
    },
  },
  reducers: {
    setRoleList(state, {result}) {
      return {
        ...state,
        pagination: {
          pageSize: result.size,
          current: result.current,
          total: result.total,
        },
        list: result.records,
      }
    },
    setRoleTypeData(state, {result}) {
      return {...state, roleTypeData: result}
    },
    setRoleData(state, {result}) {
      return {
        ...state,
        roleData: result,
      }
    },
    setAuthUser(state, {result}) {
      return {...state, authUser: result}
    }
  },
};
export default roleModel;

