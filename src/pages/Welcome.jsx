import React, { useState, useEffect } from 'react';
import { connect } from 'dva';
import { Redirect } from 'umi';

/**
 * 欢迎页面
 *
 * @author zhangby
 * @date 1/3/20 1:16 pm
 */
function Welcome(props) {
  const { dispatch } = props;
  const [ url, setUrl ] = useState('/sys/user')

  // 初始化菜单，选择第一个菜单跳转
  useEffect(() => {
    if (dispatch) {
      dispatch({
        type: 'menu/getMenuForUser',
      }).then(res => {
        const menuList = res.result;
        // 获取链接
        for (let i = 0; i < menuList.length; i++){
          const menu = menuList[i];
          if (menu !== undefined && menu.children !== undefined){
            setUrl(menu.children[0].path);
            break
          }
        }
      })
    }
  })

  return (
    <div>
      <Redirect to={url}/>
    </div>
  )
}

export default connect(({ menu }) => ({
  menuList: menu.menuList,
}))(Welcome);
