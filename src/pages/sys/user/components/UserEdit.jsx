import React, {useState, useEffect} from 'react';
import {connect} from 'dva';
import {EditOutlined} from '@ant-design/icons';
import {Button, Drawer, Form, Input, Select, Spin} from 'antd';
import SelectPhoto from "@/pages/common/SelectPhoto";

const { Option } = Select;

/**
 * 新建用户
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function UserEdit(props, ref) {
  const [visible, setVisible] = useState(false);
  const [photo, setPhoto] = useState("{\"select\":true,\"key\":0}")
  const {dispatch, roleTypeData, roleList, userId, loading} = props;
  const [form] = Form.useForm();

  // 页面加载
  useEffect(() => {
    if (dispatch) {
      dispatch({
        type: 'dict/getDictListByType',
        params: { type: 'role_type' }
      });
      dispatch({
        type: 'role/getRoleSelectData'
      });
    }
  },[]);

  // 初始化
  const preInit = () => {
    form.resetFields();
    setVisible(true);
    if (dispatch) {
      dispatch({
        type: 'user/getUserById',
        params: { id: userId }
      }).then(res => {
        if (res){
          const { result } = res;
          form.setFieldsValue({
            name: result.name,
            loginName: result.loginName,
            mobile: result.mobile,
            email: result.email,
            userType: result.userType,
            roleIds: result.tails.roleIds,
          });
          setPhoto(result.photo)
        }
      });
    }
  }

  // 提交
  const onSubmit = () => {
    // 表单验证
    form.validateFields().then(values => {
      if (dispatch) {
        dispatch({
          type: 'user/updateUser',
          params: {
            ...values,
            id: userId,
            photo: photo,
            roleId: values.roleIds.join(",")
          }
        }).then(() => {
          setVisible(false);
        })
      }
    })
  }

  return (
    <span>
      <span onClick={() => preInit()} style={{ cursor: 'pointer'}}>
        {
          props.children ?
            props.children
            :
            <Button type="primary" shape="circle" icon={<EditOutlined />} />
        }
      </span>
      {/* 弹出窗口 */}
      <Drawer
        title="编辑用户"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={650}
        footer={
          <div style={{textAlign: 'right',}}>
            <Button onClick={() => setVisible(false)} style={{marginRight: 8}}>
              取消
            </Button>
            <Button onClick={onSubmit} type="primary">
              确定
            </Button>
          </div>
        }
      >
        <Spin spinning={loading}>
          <Form{...layout} form={form}>
            <div style={{ textAlign: 'center', marginBottom: 20 }}>
              <SelectPhoto isEdit={true} key={photo} photo={photo} renderFun={val => setPhoto(val)}/>
            </div>
            <Form.Item name="name" label="用户名" rules={[{required: true, message: '用户名不能为空'}]}>
              <Input/>
            </Form.Item>
            <Form.Item name="loginName" label="登录名" rules={[{required: true, message: '登录名不能为空'}]}>
              <Input/>
            </Form.Item>
            <Form.Item name="mobile" label="联系方式">
              <Input/>
            </Form.Item>
            <Form.Item name="email" label="电子邮箱">
              <Input/>
            </Form.Item>
            <Form.Item name="userType" label="用户类型" rules={[{required: true, message: '请选择用户类型'}]}>
              <Select>
                {roleTypeData.map((item, key) => (
                  <Option key={key} value={item.value}>
                    {item.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item name="roleIds" label="所属角色" rules={[{required: true, message: '请选择所属角色'}]}>
              <Select mode="multiple">
                {roleList.map((item, key) => (
                  <Option key={key} value={item.value}>
                    {item.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Form>
        </Spin>
      </Drawer>
    </span>
  )

}

const layout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 7},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 12},
  },
};

export default connect(({user, dict, role, loading}) => ({
  loading: loading.models.user || loading.models.dict || loading.models.role,
  roleTypeData: dict.dictListData,
  roleList: role.roleTypeData,
}))(UserEdit);
