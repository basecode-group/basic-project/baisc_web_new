import React, {useState, useEffect} from 'react';
import {connect} from 'dva';
import {PageHeaderWrapper} from '@ant-design/pro-layout';
import {DownOutlined, UpOutlined, SearchOutlined, DeleteOutlined, QuestionCircleOutlined, KeyOutlined } from '@ant-design/icons';
import {Card, Row, Col, Input, Button, Form, Select, Table, Alert, Tag, Popconfirm, Modal} from 'antd';
import UserAdd from "@/pages/sys/user/components/UserAdd";
import UserEdit from "@/pages/sys/user/components/UserEdit";
import SelectPhoto from "@/pages/common/SelectPhoto";

const { Option } = Select;
const { confirm } = Modal;


/**
 * 用户管理
 * @param props
 * @param ref
 * @returns {*}
 */
function index(props, ref) {
  const [rows, setRows] = useState([]);
  const [isShow, setIsShow] = useState(false);
  const {dispatch, loading, list, pagination, roleTypeData} = props;
  const [form] = Form.useForm();

  // 初始化
  useEffect(() => {
    queryUserList();
    if (dispatch) {
      dispatch({
        type: 'dict/getDictListByType',
        params: { type: 'role_type' }
      });
    }
  }, []);

  // 查询用户
  const queryUserList = params => {
    if (dispatch) {
      dispatch({
        type: 'user/getUserList',
        params: params,
      });
    }
  }

  // 删除
  const onDelete = id => {
    if (dispatch) {
      dispatch({
        type: 'user/deleteUser',
        params: {
          id: id
        }
      });
    }
  }

  // 批量删除
  const batchDelete = () => {
    confirm({
      title: '确定要删除选择的记录吗?',
      icon: <QuestionCircleOutlined style={{color: 'red'}}/>,
      content: `已选择${rows.length}项记录。`,
      onOk() {
        if (dispatch) {
          dispatch({
            type: 'user/deleteUser',
            params: {
              id: rows.join(","),
            },
          }).then(() => {
            setRows([]);
          })
        }
      }
    });
  }

  // 重置密码
  const resetPwd = () => {
    confirm({
      title: '确定选择的记录要重置密码吗?',
      icon: <QuestionCircleOutlined style={{color: 'red'}}/>,
      content: `已选择${rows.length}项记录。`,
      onOk() {
        if (dispatch) {
          dispatch({
            type: 'user/resetPwd',
            params: {
              id: rows.join(","),
            },
          }).then(() => {
            setRows([]);
          })
        }
      }
    });
  }

  // columns
  const columns = [
    {
      title: '头像',
      dataIndex: 'photo',
      align: 'center',
      render: val => <SelectPhoto photo={val} key={val} size="small"/>
    },
    {
      title: '用户名',
      dataIndex: 'name',
      align: 'center',
      render: (val, record) => (
        <UserEdit userId={record.id}>
          <Button type="link">{val}</Button>
        </UserEdit>
      )
    },
    {
      title: '登录名',
      dataIndex: 'loginName',
      align: 'center',
      render: val => <Tag.CheckableTag>{val}</Tag.CheckableTag>
    },
    {
      title: '用户类型',
      dataIndex: 'tails',
      align: 'center',
      render: (val, record) => <Tag color={roleColors[record.userType % 5]}>{val.userTypeLabel}</Tag>
    },
    {
      title: '联系方式',
      dataIndex: 'mobile',
      align: 'center',
      render: val => <Tag.CheckableTag>{val}</Tag.CheckableTag>
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      align: 'center',
      render: val => <Tag.CheckableTag>{val}</Tag.CheckableTag>
    },
    {
      title: '创建时间',
      dataIndex: 'createDate',
      align: 'center',
      render: val => <Tag.CheckableTag>{val}</Tag.CheckableTag>
    },
    {
      title: '操作',
      align: 'center',
      dataIndex: 'id',
      render: ( id , record ) => (
        <div style={{ minWidth: 120 }}>
          <UserEdit userId={id} />
          {/* 删除 */}
          <Popconfirm
            placement="topRight"
            onConfirm={() => onDelete(id)}
            title="确定要删除记录吗？"
            icon={<QuestionCircleOutlined style={{color: 'red'}}/>}
          >
            <Button type="primary" style={{ marginLeft: 10 }} danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </div>
      )
    },
  ]

  return (
    <span>
      <PageHeaderWrapper extra={[
        <span key="pwd">
          {
            rows.length > 0 ?
              <Button onClick={resetPwd} icon={<KeyOutlined />}>
                重置密码
              </Button>
              : null
          }
        </span>,
        <span key="delete">
          {
            rows.length > 0 ?
              <Button type="primary" onClick={batchDelete} danger icon={<DeleteOutlined />}>
                批量删除
              </Button>
              : null
          }
        </span>,
        <UserAdd key="add"/>
      ]}>
        <Card>
          <Form form={form} onFinish={queryUserList}>
            <Row gutter={10}>
              <Col xs={24} sm={16} md={12} lg={8}>
               <Form.Item name="name" label="用户名">
                <Input style={{width: 220}} placeholder="用户名"/>
               </Form.Item>
              </Col>
              <Col xs={24} sm={16} md={12} lg={8}>
               <Form.Item name="loginName" label="登录名">
                <Input style={{width: 220}} placeholder="登录名"/>
               </Form.Item>
              </Col>
              {
                isShow ?
                  <Col xs={24} sm={16} md={12} lg={8}>
                    <Form.Item name="mobile" label="联系方式">
                      <Input style={{width: 220}} placeholder="联系方式"/>
                    </Form.Item>
                  </Col>
                : null
              }
              {
                isShow ?
                  <Col xs={24} sm={16} md={12} lg={8}>
                    <Form.Item name="email" label="&nbsp;&nbsp;邮&nbsp;&nbsp;箱">
                      <Input style={{width: 220}} placeholder="邮箱"/>
                    </Form.Item>
                  </Col>
                : null
              }
              {
                isShow ?
                  <Col xs={24} sm={16} md={12} lg={8}>
                    <Form.Item name="userType" label="&nbsp;&nbsp;类&nbsp;&nbsp;型">
                      <Select style={{ width: 220 }} placeholder="全部" allowClear>
                        {roleTypeData.map((item, key) => (
                          <Option key={key} value={item.value}>
                            {item.label}
                          </Option>
                        ))}
                      </Select>
                    </Form.Item>
                  </Col>
                : null
              }
              <Col xs={24} sm={16} md={12} lg={8}>
                <Form.Item>
                  <Button type="primary" htmlType="submit" icon={<SearchOutlined/>}>查询</Button>
                  <Button icon={isShow ? <UpOutlined/> : <DownOutlined/>}
                          onClick={() => setIsShow(!isShow)}
                          type="link">
                    {isShow ? '收起' : '展开'}
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
          {/* 标签 */}
          <Alert
            style={{ margin: '20px 0 20px 0' }}
            message={
              <div>
                已选择{' '}
                <span style={{ color: '#1690ff', fontWeight: 600, margin: '0 5px' }}>
                  {rows.length}
                </span>{' '}项
                <span style={{ color: '#1690ff', marginLeft: 30, cursor: 'pointer' }}
                      onClick={() => setRows([])}>清空</span>
              </div>
            }
            type="info"
            showIcon
          />
          {/* 列表 */}
          <Table
            defaultExpandAllRows
            dataSource={list}
            loading={loading}
            columns={columns}
            rowKey={ "id" }
            rowSelection={{
              selectedRowKeys: rows,
              onChange: keys => setRows(keys),
            }}
            pagination={
              {
                pageSize: pagination.pageSize,
                current: pagination.current,
                total: pagination.total,
                onChange: (page, pageSize) => {
                  queryUserList({pageNum: page, pageSize: pageSize})
                },
              }
            }
          />
        </Card>
      </PageHeaderWrapper>
    </span>
  )
}

const roleColors = ['red', 'cyan', 'green', 'blue', 'geekblue'];

export default connect(({user, dict, loading}) => ({
  loading: loading.models.user || loading.models.dict,
  list: user.list,
  pagination: user.pagination,
  roleTypeData: dict.dictListData
}))(index);
