import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { Button, Modal, Form, Input } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

/**
 * 新建行政区划
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function AreaAdd(props,ref) {
  const [ visible, setVisible ] = useState(false);
  const [ form ] = Form.useForm();
  const { dispatch } = props;

  // 初始化
  const preInit = () => {
    form.resetFields();
    setVisible(true);
  }

  // 提交
  const onSubmit = () => {
    form.validateFields().then(values => {
      if (dispatch) {
        dispatch({
          type: 'area/saveArea',
          params: {
            ...values,
            parentId: '1c4031d4aafd4480ae6d4943dbe4f173',
          }
        }).then(res => {
          if (res) {
            setVisible(false);
          }
        })
      }
    })
  }

  return (
    <span>
      <Button type="primary" onClick={preInit} icon={<PlusOutlined />}>
       新建
      </Button>
      <Modal
        title="新建行政区划"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false) }
        width={600}
      >
        <Form {...layout} form={form}>
         <Form.Item name="name" label="区域名称" rules={[{ required: true, message: '区域名称不能为空' }]}>
          <Input />
         </Form.Item>
         <Form.Item name="shortName" label="区域简称" rules={[{ required: true, message: '区域简称不能为空' }]}>
          <Input />
         </Form.Item>
         <Form.Item name="code" label="区域编码" rules={[{ required: true, message: '区域编码不能为空' }]}>
          <Input />
         </Form.Item>
        </Form>
      </Modal>
    </span>
  )
}

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

export default connect(({area, loading}) => ({
  loading: loading.models.area,
}))(AreaAdd);
