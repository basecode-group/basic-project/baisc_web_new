import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { Button, Modal, Form, Input, Spin } from 'antd';
import { EditOutlined } from '@ant-design/icons';

/**
 * 新建行政区划
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function AreaDetailEdit(props, ref) {
  const [ visible, setVisible ] = useState(false);
  const [ form ] = Form.useForm();
  const { dispatch, areaId, loading, parentId, parentName  } = props;

  // 初始化
  const preInit = () => {
    form.resetFields();
    setVisible(true);
    if (dispatch) {
      dispatch({
        type: 'area/getAreaById',
        params: { id: areaId }
      }).then(res => {
        form.setFieldsValue({
          name: res.result.name,
          code: res.result.code,
        })
      })
    }
  }

  // 提交
  const onSubmit = () => {
    form.validateFields().then(values => {
      if (dispatch) {
        dispatch({
          type: 'area/updateArea',
          params: {
            ...values,
            parentId: parentId,
            id: areaId,
          }
        }).then(res => {
          if (res) {
            setVisible(false);
          }
        })
      }
    })
  }

  return (
    <span>
      <span onClick={() => preInit()} style={{ cursor: 'pointer'}}>
          {
            props.children ?
              props.children
              :
              <Button type="primary" shape="circle" icon={<EditOutlined />} />
          }
      </span>
      <Modal
        title="编辑行政区划"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false) }
        width={600}
      >
        <Spin spinning={loading}>
          <Form {...layout} form={form}>
           <Form.Item label="父级地区">
            <Input disabled value={parentName}/>
           </Form.Item>
           <Form.Item name="name" label="区域名称" rules={[{ required: true, message: '区域名称不能为空' }]}>
            <Input />
           </Form.Item>
           <Form.Item name="code" label="区域编码" rules={[{ required: true, message: '区域编码不能为空' }]}>
            <Input />
           </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </span>
  )
}

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

export default connect(({area, loading}) => ({
  loading: loading.models.area,
}))(AreaDetailEdit);
