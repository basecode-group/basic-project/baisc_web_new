import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import router from 'umi/router';
import { Card, Row, Col, Input, Table, Button, Popconfirm, Tag, Modal } from 'antd';
import { DeleteOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import AreaDetailAdd from "@/pages/sys/area/components/AreaDetails/components/AreaDetailAdd";
import AreaDetailEdit from "@/pages/sys/area/components/AreaDetails/components/AreaDetailEdit";
import Link from 'umi/link';

const { Search } =  Input;
const { confirm } = Modal;

/**
 * 区域详情
 * @param props
 * @param ref
 * @returns {*}
 */
function index(props,ref) {
  const [ rows, setRows ] = useState([]);
  const { params } = props.match;
  const { dispatch, loading, list, pagination } = props;

  // 初始化
  useEffect(() => {
    queryAreaList({});
  },[params.areaId]);

  // 查询行政区划列表
  const queryAreaList = param => {
    if (dispatch) {
      dispatch({
        type: 'area/getAreaList',
        params: {...param, parentId: params.areaId},
      });
    }
  }

  // 删除行政区划
  const onDelete = id => {
    if (dispatch) {
      dispatch({
        type: 'area/deleteArea',
        params: { id: id, parentId: params.areaId }
      });
    }
  }

  const batchDelete = () => {
    confirm({
      title: '确定要删除选择的记录吗?',
      icon: <QuestionCircleOutlined style={{color: 'red'}}/>,
      content: `已选择${rows.length}项记录。`,
      onOk() {
        if (dispatch) {
          dispatch({
            type: 'area/deleteArea',
            params: {
              id: rows.join(","),
              parentId: params.areaId
            },
          }).then(() => {
            setRows([]);
          })
        }
      }
    });
  }

  // 表数据
  const columns = [
    {
      title: '区域名称',
      dataIndex: 'name',
      align: 'center',
      render: (val, record) =>  <Link to={`/sys/area/detail/${`${params.areaName} | ${record.name}`}/${record.id}`}> {val}</Link>
    },
    {
      title: '区域编码',
      dataIndex: 'code',
      align: 'center',
    },
    {
      title: '区域类型',
      dataIndex: 'type',
      align: 'center',
      render: val => (
        <Tag color="blue">
          {
            val === '1' ? '国家'
              : val === '2' ? '省份'
              : val === '3' ? '市级'
                : val === '4' ? '县级'
                  : val === '5' ? '街道'
                    : val === '6' ? '居委会' : '其他'
          }
        </Tag>
      )
    },
    {
      title: '创建时间',
      dataIndex: 'createDate',
      align: 'center',
      width: 200,
      render: val => <Tag.CheckableTag>{val}</Tag.CheckableTag>
    },
    {
      title: '操作',
      align: 'center',
      dataIndex: 'id',
      render: ( id , record ) => (
        <div>
          <AreaDetailEdit areaId={id} parentId={params.areaId} parentName={params.areaName} />
          {/* 删除 */}
          <Popconfirm
            placement="topRight"
            onConfirm={() => onDelete(id)}
            title="确定要删除记录吗？"
            icon={<QuestionCircleOutlined style={{color: 'red'}}/>}
          >
            <Button type="primary" style={{ marginLeft: 10 }} danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </div>
      )
    },
  ];

  return (
    <span>
      <PageHeaderWrapper
        title="行政区划详情"
        onBack={() => router.goBack(-1)}
        subTitle={ params.areaName }>
        <Card>
          {/* 查询 */}
          <Row style={{ marginBottom: 20 }}>
            <Col span={12}>
              <AreaDetailAdd parentId={params.areaId} parentName={params.areaName} />
              {
                rows.length > 0 ?
                  <Button type="primary" onClick={batchDelete} style={{ marginLeft: 10}} danger icon={<DeleteOutlined />}>
                    批量删除
                  </Button>
                  : null
              }
            </Col>
            <Col span={12} style={{ textAlign: 'right'}}>
              <Search placeholder="模糊搜索" style={{ maxWidth: 500 }}
                      onSearch={value => queryAreaList({ keyword: value})} enterButton />
            </Col>
          </Row>
          {/* 列表 */}
          <Table
            defaultExpandAllRows
            dataSource={list}
            loading={loading}
            columns={columns}
            rowKey={ "id" }
            rowSelection={{
              selectedRowKeys: rows,
              onChange: keys => setRows(keys),
            }}
            pagination={
              {
                ...pagination,
                onChange: (page, pageSize) => {
                  queryAreaList({pageNum: page, pageSize: pageSize})
                },
                showSizeChanger: true,
                pageSizeOptions: ['10','20','40','50','100'],
                onShowSizeChange: (page, pageSize) => {
                  queryAreaList({pageNum: page, pageSize: pageSize})
                },
                showTotal: val => `共 ${val} 条记录`,
              }
            }
          />
        </Card>
      </PageHeaderWrapper>
    </span>
  )
}


export default connect(({area, loading}) => ({
  loading: loading.models.area,
  list: area.list,
  pagination: area.pagination,
}))(index);
