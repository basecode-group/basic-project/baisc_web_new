import React, {useState, useEffect} from 'react';
import {connect} from 'dva';
import {UsergroupAddOutlined} from '@ant-design/icons';
import {Button, Drawer, Spin, Row, Col, Tag, Switch, Transfer} from 'antd';
import IconFont from "@/pages/common/IconFont";

/**
 * 角色用户授权
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function AuthUser(props, ref) {
  const [visible, setVisible] = useState(false);
  const [targetKeys, setTargetKeys] = useState([]);
  const { dispatch, loading, roleId, role, authUser } = props;

  // 初始化
  const preInit = () => {
    setVisible(true)
    if (dispatch) {
      dispatch({
        type: 'role/getRoleById',
        params: { id: roleId }
      });
      dispatch({
        type: 'role/getRoleUserAuth',
        params: { roleId: roleId }
      }).then(res => {
        setTargetKeys(res.result.checked);
      })
    }
  }

  // 保存
  const onSubmit = () => {
    if (dispatch) {
      dispatch({
        type: 'role/saveRoleUserAuth',
        params: {
          roleId: roleId,
          userIds: targetKeys.join(",")
        }
      }).then(() => {
        setVisible(false);
      })
    }
  }
  return (
    <span style={{...props.style}}>
      <Button type="primary" shape="circle" onClick={preInit} icon={<UsergroupAddOutlined/>}/>
      {/* 弹出窗口 */}
      <Drawer
        title="新建菜单"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={650}
        footer={
          <div style={{textAlign: 'right',}}>
            <Button onClick={() => setVisible(false)} style={{marginRight: 8}}>
              取消
            </Button>
            <Button onClick={onSubmit} type="primary">
              确定
            </Button>
          </div>
        }
      >
        <Spin spinning={loading}>
          <h3>角色信息</h3>
          <Row style={{ marginTop: 30 }}>
            <Col span={12}>
              <IconFont type="icon-yonghu" style={{ fontSize: '1.5em', float: 'left', marginRight: 10 }}></IconFont>
              <DescriptionItem title="角色名称" content={<span style={{ color: '#eb2f96' }}>{role.name}</span>} />
            </Col>
            <Col span={12}>
              <IconFont type="icon-yingwen" style={{ fontSize: '1.4em', float: 'left', marginRight: 10, marginLeft: 2 }}></IconFont>
              <DescriptionItem title="英文名称" content={<span style={{ color: '#eb2f96' }}>{role.enname}</span>} />
            </Col>
            <Col span={12}>
              <IconFont type="icon-biaoji" style={{ fontSize: '1.5em', float: 'left', marginRight: 10 }}></IconFont>
              <DescriptionItem title="角色类型" content={<Tag color="volcano">{role.tails.roleTypeLabel}</Tag>} />
            </Col>
            <Col span={12}>
              <IconFont type="icon-jiandangzhuangtai-copy-copy" style={{ fontSize: '1.7em', float: 'left', marginRight: 8 }}></IconFont>
              <DescriptionItem title="是否可用" content={<Switch checked={role.useable === '1'} size="small" />} />
            </Col>
          </Row>
          <h3 style={{ margin: '20px 0' }}>用户授权</h3>
          <Transfer
            titles={['用户列表', '选中用户']}
            dataSource={authUser.userList}
            targetKeys={targetKeys}
            showSearch
            listStyle={{
              width: 280,
              height: 450,
              marginTop: 10,
            }}
            render={item => <span>{item.name}（ {item.loginName} ）</span>}
            onChange={targetKeys => setTargetKeys(targetKeys)}
          />
        </Spin>
      </Drawer>
    </span>
  )
}

const DescriptionItem = ({ title, content }) => (
  <div
    style={{
      fontSize: 14,
      lineHeight: '22px',
      marginBottom: 0,
      color: 'rgba(0,0,0,0.65)',
    }}
  >
    <p
      style={{
        marginRight: 8,
        marginBottom: 13,
        display: 'inline-block',
        color: 'rgba(0,0,0,0.85)',
      }}
    >
      {title}:
    </p>
    {content}
  </div>
);


export default connect(({role, loading}) => ({
  loading: loading.models.role,
  role: role.roleData,
  authUser: role.authUser,
}))(AuthUser);
