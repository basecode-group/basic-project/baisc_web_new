import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { PlusOutlined } from '@ant-design/icons';
import { Button, Modal, Form, Input, Select, Switch, Spin } from 'antd';

const { Option } = Select;


/**
 * 新建角色
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function RoleAdd(props,ref) {
  const [ visible, setVisible ] = useState(false);
  const [useable, setUseable] = useState(true);
  const [ form ] = Form.useForm();
  const { dispatch, roleTypeData, loading } = props;

  // 初始化
  const preInit = () => {
    form.resetFields();
    setVisible(true);
    if (dispatch) {
      dispatch({
        type: 'dict/getDictListByType',
        params: { type: 'role_type' }
      });
    }
  }

  // 保存
  const onSubmit = () => {
    form.validateFields().then(values => {
      if (dispatch) {
        dispatch({
          type: 'role/saveRole',
          params: {
            ...values,
            useable: values.useable ? "1" : "0",
          }
        }).then(() => {
          setVisible(false);
        });
      }
    })
  }

  return (
    <span>
      <Button type="primary" onClick={preInit} icon={<PlusOutlined />}>
       新建
      </Button>
      <Modal
        title="新建角色"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false) }
        width={600}
      >
        <Spin spinning={loading}>
          <Form
           {...layout}
           form={form}
          >
           <Form.Item name="name" label="角色名称" rules={[{ required: true, message: '角色名称不能为空' }]}>
            <Input />
           </Form.Item>
           <Form.Item name="enname" label="英文名称" rules={[{ required: true, message: '英文名称不能为空' }]}>
            <Input />
           </Form.Item>
           <Form.Item name="roleType" label="角色类型" rules={[{ required: true, message: '请选择角色类型' }]}>
             <Select>
               {
                 roleTypeData.map((item, key) => (
                   <Option value={item.value} key={key}>{item.label}</Option>
                 ))
               }
             </Select>
           </Form.Item>
            <Form.Item label="是否可用">
              <Switch defaultChecked checked={useable} onChange={val => setUseable(val)}/>
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </span>
  )
}

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

export default connect(({role, dict, loading}) => ({
  loading: loading.models.role || loading.models.dict,
  roleTypeData: dict.dictListData
}))(RoleAdd);
