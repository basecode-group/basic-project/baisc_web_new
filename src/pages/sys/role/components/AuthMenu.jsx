import React, {useState, useEffect} from 'react';
import {connect} from 'dva';
import { BarsOutlined } from '@ant-design/icons';
import {Button, Drawer, Spin, Row, Col, Tag, Switch, Tree} from 'antd';
import IconFont from "@/pages/common/IconFont";
import AntIcon from "@/pages/common/AntIcon";

const { TreeNode } = Tree;

/**
 * 角色菜单授权
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function AuthMenu(props, ref) {
  const [ visible, setVisible ] = useState(false);
  const [ selectedKeys, setSelectedKeys] = useState([]);
  const { dispatch, loading, roleId, role, menuList } = props;

  // 初始化
  const preInit = () => {
    setVisible(true);
    if (dispatch) {
      dispatch({
        type: 'role/getRoleById',
        params: { id: roleId }
      });
      dispatch({
        type: 'menu/getMenu4Role',
        params: { roleId: roleId }
      }).then(res => {
        setSelectedKeys(res.result);
      })
      dispatch({
        type: 'menu/getMenuList',
      });
    }
  }

  // 提交
  const onSubmit = () => {
    if (dispatch) {
      dispatch({
        type: 'menu/saveMenu4Role',
        params: {
          roleId: roleId,
          menuIds: selectedKeys.join(",")
        }
      }).then(() => {
        setVisible(false);
      })
    }
  }

  const renderTreeNodes = data =>
    data.map(item => {
      if (item.children) {
        return (
          <TreeNode
            title={<div><AntIcon type={item.icon} style={{ marginRight: 5 }}/><span>{item.name}</span></div>}
            key={item.id} dataRef={item}>
            {renderTreeNodes(item.children)}
          </TreeNode>
        );
      }
      // eslint-disable-next-line max-len
      return <TreeNode title={<div><AntIcon type={item.icon} style={{ marginRight: 5 }}/><span>{item.name}</span></div>} key={item.id}/>;
    });


  return (
    <span>
      <Button type="primary" shape="circle" onClick={preInit} icon={<BarsOutlined />}/>
      {/* 弹出窗口 */}
      <Drawer
        title="菜单授权"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={ 650 }
        footer={
          <div style={{textAlign: 'right',}}>
            <Button onClick={() => setVisible(false)} style={{ marginRight: 8 }}>
              取消
            </Button>
            <Button onClick={onSubmit} type="primary">
              确定
            </Button>
          </div>
        }
      >
        <Spin spinning={loading}>
          <h3>角色信息</h3>
          <Row style={{ marginTop: 30 }}>
            <Col span={12}>
              <IconFont type="icon-yonghu" style={{ fontSize: '1.5em', float: 'left', marginRight: 10 }}></IconFont>
              <DescriptionItem title="角色名称" content={<span style={{ color: '#eb2f96' }}>{role.name}</span>} />
            </Col>
            <Col span={12}>
              <IconFont type="icon-yingwen" style={{ fontSize: '1.4em', float: 'left', marginRight: 10, marginLeft: 2 }}></IconFont>
              <DescriptionItem title="英文名称" content={<span style={{ color: '#eb2f96' }}>{role.enname}</span>} />
            </Col>
            <Col span={12}>
              <IconFont type="icon-biaoji" style={{ fontSize: '1.5em', float: 'left', marginRight: 10 }}></IconFont>
              <DescriptionItem title="角色类型" content={<Tag color="volcano">{role.tails.roleTypeLabel}</Tag>} />
            </Col>
            <Col span={12}>
              <IconFont type="icon-jiandangzhuangtai-copy-copy" style={{ fontSize: '1.7em', float: 'left', marginRight: 8 }}></IconFont>
              <DescriptionItem title="是否可用" content={<Switch checked={role.useable === '1'} size="small" />} />
            </Col>
          </Row>
          <h3 style={{ margin: '20px 0' }}>菜单授权</h3>
          <Tree
            checkable
            defaultExpandAll
            key={loading}
            checkedKeys={selectedKeys}
            style={{ marginLeft: 30 }}
            onCheck={checkedKeys => setSelectedKeys(checkedKeys)}
          >
              {renderTreeNodes(menuList)}
            </Tree>
        </Spin>
      </Drawer>
    </span>
  )
}

const DescriptionItem = ({ title, content }) => (
  <div
    style={{
      fontSize: 14,
      lineHeight: '22px',
      marginBottom: 0,
      color: 'rgba(0,0,0,0.65)',
    }}
  >
    <p
      style={{
        marginRight: 8,
        marginBottom: 13,
        display: 'inline-block',
        color: 'rgba(0,0,0,0.85)',
      }}
    >
      {title}:
    </p>
    {content}
  </div>
);

export default connect(({role, menu, loading}) => ({
  loading: loading.models.role || loading.models.menu,
  role: role.roleData,
  menuList: menu.list,
}))(AuthMenu);
