import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Row, Col, Input, Table, Tag, Switch, Popconfirm, Button, Alert, Modal } from 'antd';
import { QuestionCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import RoleAdd from "@/pages/sys/role/components/RoleAdd";
import RoleEdit from "@/pages/sys/role/components/RoleEdit";
import AuthMenu from "@/pages/sys/role/components/AuthMenu";
import AuthUser from "@/pages/sys/role/components/AuthUser";

const {Search} = Input;
const { confirm } = Modal;

/**
 * 角色管理
 * @param props
 * @param ref
 * @returns {*}
 */
function index(props,ref) {
  const [ rows, setRows ] = useState([]);
  const {dispatch, role, loading } = props;

  // 初始化
  useEffect(() => {
    queryRoleList({});
  },[]);

  // 查询用户列表
  const queryRoleList = params => {
    if (dispatch) {
      dispatch({
        type: 'role/getRoleList',
        params: params
      });
    }
  }

  const batchDelete = () => {
    confirm({
      title: '确定要删除选择的记录吗?',
      icon: <QuestionCircleOutlined style={{color: 'red'}}/>,
      content: `已选择${rows.length}项记录。`,
      onOk() {
        if (dispatch) {
          dispatch({
            type: 'role/deleteRole',
            params: {
              id: rows.join(","),
            },
          }).then(() => {
            setRows([]);
          })
        }
      }
    });
  }

  // list 列
  const columns = [
    {
      title: '角色名称',
      dataIndex: 'name',
      align: 'center',
      render: (val, record) => (
        <RoleEdit roleId={record.id}>
          <Button type="link">{val}</Button>
        </RoleEdit>
      )
    },
    {
      title: '英文名称',
      dataIndex: 'enname',
      align: 'center'
    },
    {
      title: '角色类型',
      dataIndex: 'tails',
      align: 'center',
      render: val => <Tag color="red">{val.roleTypeLabel}</Tag>
    },
    {
      title: '是否可用',
      dataIndex: 'useable',
      align: 'center',
      render: val => <Switch size="small" checked={val === "1"} />
    },
    {
      title: '操作',
      align: 'center',
      dataIndex: 'id',
      render: ( id , record ) => (
        <div>
          <AuthMenu roleId={id} />
          <AuthUser roleId={id} style={{ marginLeft: 10 }}/>
        </div>
      )
    },
  ]

  return (
    <div>
      <PageHeaderWrapper>
        <Card>
          {/* 查询 */}
          <Row style={{ marginBottom: 30 }}>
            <Col span={12}>
              <RoleAdd />
              {
                rows.length > 0 ?
                  <Button type="primary" onClick={batchDelete} style={{ marginLeft: 10}} danger icon={<DeleteOutlined />}>
                    批量删除
                  </Button>
                  : null
              }
            </Col>
            <Col span={12} style={{ textAlign: 'right'}}>
              <Search placeholder="模糊搜索" style={{ maxWidth: 500 }}  onSearch={value => queryRoleList({keyword: value})} enterButton />
            </Col>
          </Row>
          {/* 标签 */}
          <Alert
            style={{ margin: '20px 0 20px 0' }}
            message={
              <div>
                已选择{' '}
                <span style={{ color: '#1690ff', fontWeight: 600, margin: '0 5px' }}>
                  {rows.length}
                </span>{' '}项
                <span style={{ color: '#1690ff', marginLeft: 30, cursor: 'pointer' }}
                      onClick={() => setRows([])}>清空</span>
              </div>
            }
            type="info"
            showIcon
          />
          {/* 列表 */}
          <Table
            defaultExpandAllRows
            dataSource={role.list}
            loading={loading}
            columns={columns}
            rowKey={ "id" }
            rowSelection={{
              selectedRowKeys: rows,
              onChange: keys => setRows(keys),
            }}
            pagination={
              {
                pageSize: role.pagination.pageSize,
                current: role.pagination.current,
                total: role.pagination.total,
                onChange: (page, pageSize) => {
                  queryRoleList({pageNum: page, pageSize: pageSize})
                },
              }
            }
          />
        </Card>
      </PageHeaderWrapper>
    </div>
  )
}


export default connect(({role, loading}) => ({
  loading: loading.models.role,
  role: role,
}))(index);
