import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { EditOutlined } from '@ant-design/icons';
import { Button, Modal, Form, Input, Spin, InputNumber } from 'antd';

/**
 * 编辑字典
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function DictEdit(props,ref) {
  const [ visible, setVisible ] = useState(false);
  const [ parentId, setParentId ] = useState(false);
  const { loading, dictId, dispatch } = props;
  const [ form ] = Form.useForm();

  const preInit = () => {
    form.resetFields();
    setVisible(true);
    if (dispatch) {
      dispatch({
        type: 'dict/getDictById',
        params: { id : dictId  }
      }).then(res => {
        form.setFieldsValue({
          label: res.result.label,
          type: res.result.type,
          sort: res.result.sort,
          description: res.result.description,
        });
        setParentId(res.result.parentId);
      })
    }
  }

  // 提交
  const onSubmit = () => {
    form.validateFields().then(values => {
      if (dispatch) {
        dispatch({
          type: 'dict/updateDict',
          params: {
            ...values,
            parentId: parentId,
            id: dictId,
          }
        }).then(() => {
          setVisible(false);
        })
      }
    })
  }

  return (
    <span>
      <span onClick={() => {preInit();}} style={{ cursor: 'pointer'}}>
        {
          props.children ?
            props.children
            :
            <Button type="primary" shape="circle" icon={<EditOutlined />} />
        }
      </span>
      <Modal
        title="编辑字典"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false) }
        width={600}
      >
        <Spin spinning={loading} >
          <Form
            {...layout}
            form={form}
            initialValues={{
              sort: 10,
            }}
          >
            <Form.Item name="label" label="字典名称" rules={[{ required: true, message: '字典名称不能为空' }]}>
              <Input />
            </Form.Item>
            <Form.Item name="type" label="字典类型" rules={[{ required: true, message: '字典类型不能为空' }]}>
              <Input />
            </Form.Item>
            <Form.Item name="sort" label="排序" rules={[{ required: true, message: '排序不能为空' }]}>
              <InputNumber min={0} step={10} />
            </Form.Item>
            <Form.Item name="description" label="描述">
              <Input.TextArea rows={4} placeholder="描述信息"/>
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </span>
  )
}

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};


export default connect(({dict, loading}) => ({
  loading: loading.models.dict,
}))(DictEdit);
