import React, {useState, useEffect} from 'react';
import {connect} from 'dva';
import {EditOutlined} from '@ant-design/icons';
import {Button, Modal, Form, Input, InputNumber} from 'antd';


/**
 * 编辑 二级字典
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function DictDetailEdit(props, ref) {
  const [visible, setVisible] = useState(false);
  const [parentId, setParentId] = useState(undefined);
  const [type, setType] = useState(undefined);
  const [form] = Form.useForm();
  const { dictId, dispatch } = props;


  // 初始化
  const preInit = () => {
    form.resetFields();
    setVisible(true);
    if (dispatch) {
      dispatch({
        type: 'dict/getDictById',
        params: { id: dictId }
      }).then(res => {
        if (res) {
          form.setFieldsValue({
            label: res.result.label,
            value: res.result.value,
            sort: res.result.sort,
            description: res.result.description,
          });
          setParentId(res.result.parentId);
          setType(res.result.type);
        }
      })
    }
  }

  // 提交
  const onSubmit = () => {
    form.validateFields().then(values => {
      if (dispatch) {
        dispatch({
          type: 'dict/updateDict',
          params: {
            ...values,
            parentId: parentId,
            type: type,
            id: dictId,
          }
        }).then(() => {
          setVisible(false);
        })
      }
    })
  }

  return (
    <span>
      <span onClick={() => preInit()} style={{ cursor: 'pointer'}}>
        {
          props.children ?
            props.children
            :
            <Button type="primary" shape="circle" icon={<EditOutlined />} />
        }
      </span>
      <Modal
        title="编辑字典详情"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false)}
        width={600}
      >
        <Form
          {...layout}
          form={form}
          initialValues={{
            sort: 10,
          }}
        >
          <Form.Item name="label" label="字典名称" rules={[{required: true, message: '字典名称不能为空'}]}>
            <Input/>
          </Form.Item>
          <Form.Item name="value" label="字典值" rules={[{required: true, message: '字典值不能为空'}]}>
            <Input/>
          </Form.Item>
          <Form.Item name="sort" label="排序">
            <InputNumber min={0} step={10} />
          </Form.Item>
          <Form.Item name="description" label="描述">
            <Input.TextArea rows={4} placeholder="描述信息"/>
          </Form.Item>
        </Form>
      </Modal>
    </span>
  )
}

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

export default connect(({dict, loading}) => ({
  loading: loading.models.dict,
}))(DictDetailEdit);
