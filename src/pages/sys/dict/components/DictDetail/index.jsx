import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import router from 'umi/router';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { QuestionCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import { Card, Row, Col, Input, Table, Popconfirm, Button, Modal } from 'antd';
import DictDetailAdd from "@/pages/sys/dict/components/DictDetail/components/DictDetailAdd";
import DictDetailEdit from "@/pages/sys/dict/components/DictDetail/components/DictDetailEdit";

const { confirm } = Modal;
const { Search } = Input;

/**
 * 字典详情列表
 * @param props
 * @param ref
 * @returns {*}
 */
function index(props,ref) {
  const [ rows, setRows ] = useState([]);
  const { params } = props.match;
  const { dispatch, dict, loading } = props;

  // 初始化
  useEffect(() => {
    if (dispatch) {
      dispatch({
        type: 'dict/getDictList',
        params: { parentId: params.dictId }
      });
    }
  },[]);

  const queryDictList = param => {
    if (dispatch) {
      dispatch({
        type: 'dict/getDictList',
        params: {...param, parentId: params.dictId }
      });
    }
  }

  const onDelete = param => {
    if (dispatch) {
      dispatch({
        type: 'dict/deleteDict',
        params: param,
      });
    }
  }

  const batchDelete = () => {
    confirm({
      title: '确定要删除选择的记录吗?',
      icon: <QuestionCircleOutlined style={{color: 'red'}}/>,
      content: `已选择${rows.length}项记录。`,
      onOk() {
        if (dispatch) {
          dispatch({
            type: 'dict/deleteDict',
            params: {
              id: rows.join(","),
              parentId: params.dictId
            },
          }).then(() => {
            setRows([]);
          })
        }
      }
    });
  }

  // 列表
  const columns = [
    {
      title: '字典名称',
      dataIndex: 'label',
      align: 'center',
    },
    {
      title: '字典值',
      dataIndex: 'value',
      align: 'center',
    },
    {
      title: '描述',
      dataIndex: 'description',
      align: 'center',
    },
    {
      title: '排序',
      dataIndex: 'sort',
      align: 'center',
    },
    {
      title: '操作',
      align: 'center',
      dataIndex: 'id',
      render: (id, record) => (
        <div>
          <DictDetailEdit dictId={id}/>
          {/* 删除 */}
          <Popconfirm
            placement="topRight"
            onConfirm={() => onDelete({ id: id, parentId: params.dictId })}
            title="确定要删除记录吗？"
            icon={<QuestionCircleOutlined style={{color: 'red'}}/>}
          >
            <Button type="primary" style={{ marginLeft: 10 }} danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </div>
      )
    },
  ];

  return (
    <div>
      <PageHeaderWrapper
        title="字典详情"
        subTitle={<span style={{ marginLeft: 10 }}>{params.dictLabel}（ {params.dictType} ）</span>}
        onBack={() => router.push('/sys/dict')}
      >
        <Card>
          {/* 操作 */}
          <Row style={{ marginBottom: 20 }}>
            <Col span={12}>
              <DictDetailAdd parentId={params.dictId} type={params.dictType}/>
              {
                rows.length > 0 ?
                  <Button type="primary" onClick={batchDelete} style={{ marginLeft: 10}} danger icon={<DeleteOutlined />}>
                    批量删除
                  </Button>
                : null
              }
            </Col>
            <Col span={12} style={{ textAlign: 'right'}}>
              <Search placeholder="模糊搜索" style={{ maxWidth: 500 }}  onSearch={value => queryDictList({keyword: value})} enterButton />
            </Col>
          </Row>
          {/* 列表 */}
          <Table
            defaultExpandAllRows
            dataSource={dict.list}
            loading={loading}
            columns={columns}
            rowKey={ "id" }
            rowSelection={{
              selectedRowKeys: rows,
              onChange: keys => setRows(keys),
            }}
            pagination={
              {
                pageSize: dict.pagination.pageSize,
                current: dict.pagination.current,
                total: dict.pagination.total,
                onChange: (page, pageSize) => {
                  queryDictList({pageNum: page, pageSize: pageSize})
                },
              }
            }
          />
        </Card>
      </PageHeaderWrapper>
    </div>
  )
}


export default connect(({dict, loading}) => ({
  loading: loading.models.dict,
  dict: dict,
}))(index);
