import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { Button, Modal, Form, Spin, Input, InputNumber } from 'antd'
import { PlusOutlined } from '@ant-design/icons';

/**
 * 新增字典
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function DictAdd(props,ref) {
  const [ visible, setVisible ] = useState(false);
  const { loading, parentId, dispatch } = props;
  const [ form ] = Form.useForm();

  // 初始化
  const preInit = () => {
    form.resetFields();
    setVisible(true);
    if (dispatch) {
      dispatch({
        type: 'dict/getMaxSort',
        params: { parentId: parentId ? parentId : "0" }
      }).then(res => {
        form.setFieldsValue({
          sort: res.result.maxSort,
        })
      })
    }
  }

  // 提交
  const onSubmit = () => {
    form.validateFields().then(values => {
      if (dispatch) {
        dispatch({
          type: 'dict/saveDict',
          params: { ...values, parentId: parentId ? parentId : "0" }
        }).then(() => {
          setVisible(false);
        })
      }
    })
  }

  return (
    <span>
      <Button type="primary" onClick={preInit} icon={<PlusOutlined />}>
        新建字典
      </Button>
      <Modal
        title="新建字典"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false) }
        width={600}
      >
        <Spin spinning={loading} >
          <Form
            {...layout}
            form={form}
            initialValues={{
              sort: 10,
            }}
          >
            <Form.Item name="label" label="字典名称" rules={[{ required: true, message: '字典名称不能为空' }]}>
              <Input />
            </Form.Item>
            <Form.Item name="type" label="字典类型" rules={[{ required: true, message: '字典类型不能为空' }]}>
              <Input />
            </Form.Item>
            <Form.Item name="sort" label="排序" rules={[{ required: true, message: '排序不能为空' }]}>
              <InputNumber min={0} step={10} />
            </Form.Item>
            <Form.Item name="description" label="描述">
              <Input.TextArea rows={4} placeholder="描述信息"/>
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </span>
  )
}

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

export default connect(({dict, loading}) => ({
  loading: loading.models.dict,
}))(DictAdd);
