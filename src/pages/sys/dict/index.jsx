import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import router from 'umi/router';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { QuestionCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import { Card, Button, Form, Input, Alert, Table, Popconfirm, Tag, Modal } from 'antd';
import DictAdd from "@/pages/sys/dict/components/DictAdd";
import DictEdit from "@/pages/sys/dict/components/DictEdit";

const { confirm } = Modal;

/**
 * 字典管理
 * @param props
 * @param ref
 * @returns {*}
 */
function index(props,ref) {
  const [ rows, setRows ] = useState([]);
  const { dict, loading, dispatch } = props;
  const [form] = Form.useForm();

  // 初始化
  useEffect(() => {
    if (dispatch) {
      dispatch({
        type: 'dict/getDictList',
        params: { parentId: 0 }
      });
    }
  },[]);

  const queryDictList = (values) => {
    if (dispatch) {
      dispatch({
        type: 'dict/getDictList',
        params: {...values, parentId: 0 }
      });
    }
  }

  const onDelete = params => {
    if (dispatch) {
      dispatch({
        type: 'dict/deleteDict',
        params: params,
      });
    }
  }

  const batchDelete = () => {
    confirm({
      title: '确定要删除选择的记录吗?',
      icon: <QuestionCircleOutlined style={{color: 'red'}}/>,
      content: `已选择${rows.length}项记录。`,
      onOk() {
        if (dispatch) {
          dispatch({
            type: 'dict/deleteDict',
            params: {
              id: rows.join(","),
              parentId: "0"
            },
          }).then(() => {
            setRows([]);
          })
        }
      }
    });
  }

  const columns = [
    {
      title: '字典名称',
      dataIndex: 'label',
      align: 'center',
      render: (val, record) => (
        <Button type="link"
                onClick={() => router.push(`/sys/dict/detail/${record.type}/${record.label}/${record.id}`)}>
          {val}
        </Button>
      )
    },
    {
      title: '字典类型',
      dataIndex: 'type',
      align: 'center',
      render: (val, record) => (
        <Button type="link"
                onClick={() => router.push(`/sys/dict/detail/${record.type}/${record.label}/${record.id}`)}>
          {val}
        </Button>
      )
    },
    {
      title: '描述',
      dataIndex: 'description',
    },
    {
      title: '排序',
      dataIndex: 'sort',
      align: 'center'
    },
    {
      title: '创建时间',
      dataIndex: 'createDate',
      align: 'center',
      render: val => <Tag.CheckableTag>{val}</Tag.CheckableTag>
    },
    {
      title: '操作',
      align: 'center',
      dataIndex: 'id',
      render: ( id , record ) => (
        <div>
          <DictEdit dictId={id}/>
          {/* 删除 */}
          <Popconfirm
            placement="topRight"
            onConfirm={() => onDelete({ id: id, parentId: record.parentId })}
            title="确定要删除记录吗？"
            icon={<QuestionCircleOutlined style={{color: 'red'}}/>}
          >
            <Button type="primary" style={{ marginLeft: 10 }} danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </div>
      )
    },
  ]

  return (
    <div>
      <PageHeaderWrapper extra={[
          <span key="delete">
            {
              rows.length > 0 ?
                <Button type="primary" onClick={batchDelete} style={{ marginLeft: 10}} danger icon={<DeleteOutlined />}>
                  批量删除
                </Button>
                : null
            }
          </span>,
          <DictAdd key="add"/>
        ]}>
        <Card>
          {/* 查询 */}
          <Form layout="inline" form={form} onFinish={queryDictList}>
            <Form.Item name="label" label="字典名称" >
              <Input  placeholder="字典名称" style={{width: 220 }}/>
            </Form.Item>
            <Form.Item name="type" label="字典类型" style={{ marginLeft: 40 }}>
              <Input  placeholder="字典类型" style={{width: 220 }} />
            </Form.Item>
            <Form.Item style={{ marginLeft: 40 }}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
            </Form.Item>
          </Form>
          {/* 标签 */}
          <Alert
            style={{ margin: '20px 0 20px 0' }}
            message={
              <div>
                已选择{' '}
                <span style={{ color: '#1690ff', fontWeight: 600, margin: '0 5px' }}>
                  {rows.length}
                </span>{' '}项
                <span style={{ color: '#1690ff', marginLeft: 30, cursor: 'pointer' }}
                      onClick={() => setRows([])}>清空</span>
              </div>
            }
            type="info"
            showIcon
          />
          {/* 列表 */}
          <Table
            defaultExpandAllRows
            dataSource={dict.list}
            loading={loading}
            columns={columns}
            rowKey={ "id" }
            rowSelection={{
              selectedRowKeys: rows,
              onChange: keys => setRows(keys),
            }}
            pagination={
              {
                pageSize: dict.pagination.pageSize,
                current: dict.pagination.current,
                total: dict.pagination.total,
                onChange: (page, pageSize) => {
                  queryDictList({pageNum: page, pageSize: pageSize})
                },
              }
            }
          />
        </Card>
      </PageHeaderWrapper>
    </div>
  )
}


export default connect(({dict, loading}) => ({
  loading: loading.models.dict,
  dict: dict
}))(index);
