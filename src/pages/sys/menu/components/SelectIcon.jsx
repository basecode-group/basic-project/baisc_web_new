import React, { useState, useEffect } from 'react';
import { Input, Button, Drawer, Row, Col, Typography } from 'antd';
import {
  StepBackwardOutlined, StepForwardOutlined, FastBackwardOutlined, FastForwardOutlined, ShrinkOutlined, ArrowsAltOutlined, DownOutlined, UpOutlined, LeftOutlined, RightOutlined, CaretUpOutlined, CaretDownOutlined, CaretLeftOutlined, CaretRightOutlined, UpCircleOutlined, DownCircleOutlined, LeftCircleOutlined, RightCircleOutlined, DoubleRightOutlined, DoubleLeftOutlined, VerticalLeftOutlined, VerticalRightOutlined, VerticalAlignTopOutlined, VerticalAlignMiddleOutlined, VerticalAlignBottomOutlined, ForwardOutlined, BackwardOutlined, RollbackOutlined, EnterOutlined, RetweetOutlined, SwapOutlined, SwapLeftOutlined, SwapRightOutlined, ArrowUpOutlined, ArrowDownOutlined, ArrowLeftOutlined, ArrowRightOutlined, PlayCircleOutlined, UpSquareOutlined, DownSquareOutlined, LeftSquareOutlined, RightSquareOutlined, LoginOutlined, LogoutOutlined, MenuFoldOutlined, MenuUnfoldOutlined, BorderBottomOutlined, BorderHorizontalOutlined, BorderInnerOutlined, BorderOuterOutlined, BorderLeftOutlined, BorderRightOutlined, BorderTopOutlined, BorderVerticleOutlined, PicCenterOutlined, PicLeftOutlined, PicRightOutlined, RadiusBottomleftOutlined, RadiusBottomrightOutlined, RadiusUpleftOutlined, RadiusUprightOutlined, FullscreenOutlined, FullscreenExitOutlined,
  QuestionOutlined,QuestionCircleOutlined,PlusOutlined,PlusCircleOutlined,PauseOutlined,PauseCircleOutlined,MinusOutlined,MinusCircleOutlined,PlusSquareOutlined,MinusSquareOutlined,InfoOutlined,InfoCircleOutlined,ExclamationOutlined,ExclamationCircleOutlined,CloseOutlined,CloseCircleOutlined,CloseSquareOutlined,CheckOutlined,CheckCircleOutlined,CheckSquareOutlined,ClockCircleOutlined,WarningOutlined,IssuesCloseOutlined,StopOutlined,
  EditOutlined,FormOutlined,CopyOutlined,ScissorOutlined,DeleteOutlined,SnippetsOutlined,DiffOutlined,HighlightOutlined,AlignCenterOutlined,AlignLeftOutlined,AlignRightOutlined,BgColorsOutlined,BoldOutlined,ItalicOutlined,UnderlineOutlined,StrikethroughOutlined,RedoOutlined,UndoOutlined,ZoomInOutlined,ZoomOutOutlined,FontColorsOutlined,FontSizeOutlined,LineHeightOutlined,DashOutlined,SmallDashOutlined,SortAscendingOutlined,SortDescendingOutlined,DragOutlined,OrderedListOutlined,UnorderedListOutlined,RadiusSettingOutlined,ColumnWidthOutlined,ColumnHeightOutlined,
  AreaChartOutlined,PieChartOutlined,BarChartOutlined,DotChartOutlined,LineChartOutlined,RadarChartOutlined,HeatMapOutlined,FallOutlined,RiseOutlined,StockOutlined,BoxPlotOutlined,FundOutlined,SlidersOutlined,
  AndroidOutlined,AppleOutlined,WindowsOutlined,IeOutlined,ChromeOutlined,GithubOutlined,AliwangwangOutlined,DingdingOutlined,WeiboSquareOutlined,WeiboCircleOutlined,TaobaoCircleOutlined,Html5Outlined,WeiboOutlined,TwitterOutlined,WechatOutlined,YoutubeOutlined,AlipayCircleOutlined,TaobaoOutlined,SkypeOutlined,QqOutlined,MediumWorkmarkOutlined,GitlabOutlined,MediumOutlined,LinkedinOutlined,GooglePlusOutlined,DropboxOutlined,FacebookOutlined,CodepenOutlined,CodeSandboxOutlined,AmazonOutlined,GoogleOutlined,CodepenCircleOutlined,AlipayOutlined,AntDesignOutlined,AntCloudOutlined,AliyunOutlined,ZhihuOutlined,SlackOutlined,SlackSquareOutlined,BehanceOutlined,BehanceSquareOutlined,DribbbleOutlined,DribbbleSquareOutlined,InstagramOutlined,YuqueOutlined,AlibabaOutlined,YahooOutlined,RedditOutlined,SketchOutlined,
  AccountBookOutlined,AimOutlined,AlertOutlined,ApartmentOutlined,ApiOutlined,AppstoreAddOutlined,AppstoreOutlined,AudioOutlined,AudioMutedOutlined,AuditOutlined,BankOutlined,BarcodeOutlined,BarsOutlined,BellOutlined,BlockOutlined,BookOutlined,BorderOutlined,BorderlessTableOutlined,BranchesOutlined,BugOutlined,BuildOutlined,BulbOutlined,CalculatorOutlined,CalendarOutlined,CameraOutlined,CarOutlined,CarryOutOutlined,CiCircleOutlined,CiOutlined,ClearOutlined,CloudDownloadOutlined,CloudOutlined,CloudServerOutlined,CloudSyncOutlined,CloudUploadOutlined,ClusterOutlined,CodeOutlined,CoffeeOutlined,CommentOutlined,CompassOutlined,CompressOutlined,ConsoleSqlOutlined,ContactsOutlined,ContainerOutlined,ControlOutlined,CopyrightCircleOutlined,CopyrightOutlined,CreditCardOutlined,CrownOutlined,CustomerServiceOutlined,DashboardOutlined,DatabaseOutlined,DeleteColumnOutlined,DeleteRowOutlined,DeliveredProcedureOutlined,DeploymentUnitOutlined,DesktopOutlined,DingtalkOutlined,DisconnectOutlined,DislikeOutlined,DollarCircleOutlined,DollarOutlined,DownloadOutlined,EllipsisOutlined,EnvironmentOutlined,EuroCircleOutlined,EuroOutlined,ExceptionOutlined,ExpandAltOutlined,ExpandOutlined,ExperimentOutlined,ExportOutlined,EyeOutlined,EyeInvisibleOutlined,FieldBinaryOutlined,FieldNumberOutlined,FieldStringOutlined,FieldTimeOutlined,FileAddOutlined,FileDoneOutlined,FileExcelOutlined,FileExclamationOutlined,FileOutlined,FileGifOutlined,FileImageOutlined,FileJpgOutlined,FileMarkdownOutlined,FilePdfOutlined,FilePptOutlined,FileProtectOutlined,FileSearchOutlined,FileSyncOutlined,FileTextOutlined,FileUnknownOutlined,FileWordOutlined,FileZipOutlined,FilterOutlined,FireOutlined,FlagOutlined,FolderAddOutlined,FolderOutlined,FolderOpenOutlined,FolderViewOutlined,ForkOutlined,FormatPainterOutlined,FrownOutlined,FunctionOutlined,FundProjectionScreenOutlined,FundViewOutlined,FunnelPlotOutlined,GatewayOutlined,GifOutlined,GiftOutlined,GlobalOutlined,GoldOutlined,GroupOutlined,HddOutlined,HeartOutlined,HistoryOutlined,HomeOutlined,HourglassOutlined,IdcardOutlined,ImportOutlined,InboxOutlined,InsertRowAboveOutlined,InsertRowBelowOutlined,InsertRowLeftOutlined,InsertRowRightOutlined,InsuranceOutlined,InteractionOutlined,KeyOutlined,LaptopOutlined,LayoutOutlined,LikeOutlined,LineOutlined,LinkOutlined,Loading3QuartersOutlined,LoadingOutlined,LockOutlined,MacCommandOutlined,MailOutlined,ManOutlined,MedicineBoxOutlined,MehOutlined,MenuOutlined,MergeCellsOutlined,MessageOutlined,MobileOutlined,MoneyCollectOutlined,MonitorOutlined,MoreOutlined,NodeCollapseOutlined,NodeExpandOutlined,NodeIndexOutlined,NotificationOutlined,NumberOutlined,OneToOneOutlined,PaperClipOutlined,PartitionOutlined,PayCircleOutlined,PercentageOutlined,PhoneOutlined,PictureOutlined,PlaySquareOutlined,PoundCircleOutlined,PoundOutlined,PoweroffOutlined,PrinterOutlined,ProfileOutlined,ProjectOutlined,PropertySafetyOutlined,PullRequestOutlined,PushpinOutlined,QrcodeOutlined,ReadOutlined,ReconciliationOutlined,RedEnvelopeOutlined,ReloadOutlined,RestOutlined,RobotOutlined,RocketOutlined,RotateLeftOutlined,RotateRightOutlined,SafetyCertificateOutlined,SafetyOutlined,SaveOutlined,ScanOutlined,ScheduleOutlined,SearchOutlined,SecurityScanOutlined,SelectOutlined,SendOutlined,SettingOutlined,ShakeOutlined,ShareAltOutlined,ShopOutlined,ShoppingCartOutlined,ShoppingOutlined,SisternodeOutlined,SkinOutlined,SmileOutlined,SolutionOutlined,SoundOutlined,SplitCellsOutlined,StarOutlined,SubnodeOutlined,SwitcherOutlined,SyncOutlined,TableOutlined,TabletOutlined,TagOutlined,TagsOutlined,TeamOutlined,ThunderboltOutlined,ToTopOutlined,ToolOutlined,TrademarkCircleOutlined,TrademarkOutlined,TransactionOutlined,TranslationOutlined,TrophyOutlined,UngroupOutlined,UnlockOutlined,UploadOutlined,UsbOutlined,UserAddOutlined,UserDeleteOutlined,UserOutlined,UserSwitchOutlined,UsergroupAddOutlined,UsergroupDeleteOutlined,VerifiedOutlined,VideoCameraAddOutlined,VideoCameraOutlined,WalletOutlined,WhatsAppOutlined,WifiOutlined,WomanOutlined,
} from '@ant-design/icons';

const { Paragraph } = Typography;

/**
 * 选择图标
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function SelectIcon(props,ref) {
  const [ visible, setVisible ] = useState(false);
  const [ icon, setIcon ] = useState(undefined);
  const [ value, setValue ] = useState(undefined);

  const { initValue, render } = props;

  useEffect(() => {
    changeValue(initValue !== undefined ? initValue : undefined);
  },[])

  const preInit = () => {
    setVisible(true);
  }

  const onSubmit = () => {
    setValue(icon);
    setVisible(false);
    if (render){
      render(icon);
    }
  }

  const changeValue = val => {
    setIcon(val);
    setValue(val);
  }

  return (
    <div>
      <Input.Search
        allowClear
        placeholder="选择图标"
        enterButton="选择"
        value={value}
        onSearch={preInit}
        onChange={val => {
          changeValue(val.target.value);
          setVisible(false);
        }}
      />
      {/* 弹出窗口 */}
      <Drawer
        title="选择图标"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={ 800 }
        footer={
          <div style={{textAlign: 'right',}}>
            <Button onClick={() => setVisible(false)} style={{ marginRight: 8 }}>
              Cancel
            </Button>
            <Button onClick={onSubmit} type="primary">
              Submit
            </Button>
          </div>
        }
      >
        <h2>方向性图标</h2>
        <Row style={{ textAlign: 'center' }}>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("StepBackwardOutlined")} onDoubleClick={onSubmit}>
            <StepBackwardOutlined style={{ fontSize: 25, color: icon === "StepBackwardOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "StepBackwardOutlined" ? "#1690ff":"#545454"}}>StepBackwardOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("StepForwardOutlined")} onDoubleClick={onSubmit}>
            <StepForwardOutlined style={{ fontSize: 25, color: icon === "StepForwardOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "StepForwardOutlined" ? "#1690ff":"#545454"}}>StepForwardOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FastBackwardOutlined")} onDoubleClick={onSubmit}>
            <FastBackwardOutlined style={{ fontSize: 25, color: icon === "FastBackwardOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FastBackwardOutlined" ? "#1690ff":"#545454"}}>FastBackwardOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FastForwardOutlined")} onDoubleClick={onSubmit}>
            <FastForwardOutlined style={{ fontSize: 25, color: icon === "FastForwardOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FastForwardOutlined" ? "#1690ff":"#545454"}}>FastForwardOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ShrinkOutlined")} onDoubleClick={onSubmit}>
            <ShrinkOutlined style={{ fontSize: 25, color: icon === "ShrinkOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ShrinkOutlined" ? "#1690ff":"#545454"}}>ShrinkOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ArrowsAltOutlined")} onDoubleClick={onSubmit}>
            <ArrowsAltOutlined style={{ fontSize: 25, color: icon === "ArrowsAltOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ArrowsAltOutlined" ? "#1690ff":"#545454"}}>ArrowsAltOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DownOutlined")} onDoubleClick={onSubmit}>
            <DownOutlined style={{ fontSize: 25, color: icon === "DownOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DownOutlined" ? "#1690ff":"#545454"}}>DownOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UpOutlined")} onDoubleClick={onSubmit}>
            <UpOutlined style={{ fontSize: 25, color: icon === "UpOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UpOutlined" ? "#1690ff":"#545454"}}>UpOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LeftOutlined")} onDoubleClick={onSubmit}>
            <LeftOutlined style={{ fontSize: 25, color: icon === "LeftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LeftOutlined" ? "#1690ff":"#545454"}}>LeftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RightOutlined")} onDoubleClick={onSubmit}>
            <RightOutlined style={{ fontSize: 25, color: icon === "RightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RightOutlined" ? "#1690ff":"#545454"}}>RightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CaretUpOutlined")} onDoubleClick={onSubmit}>
            <CaretUpOutlined style={{ fontSize: 25, color: icon === "CaretUpOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CaretUpOutlined" ? "#1690ff":"#545454"}}>CaretUpOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CaretDownOutlined")} onDoubleClick={onSubmit}>
            <CaretDownOutlined style={{ fontSize: 25, color: icon === "CaretDownOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CaretDownOutlined" ? "#1690ff":"#545454"}}>CaretDownOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CaretLeftOutlined")} onDoubleClick={onSubmit}>
            <CaretLeftOutlined style={{ fontSize: 25, color: icon === "CaretLeftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CaretLeftOutlined" ? "#1690ff":"#545454"}}>CaretLeftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CaretRightOutlined")} onDoubleClick={onSubmit}>
            <CaretRightOutlined style={{ fontSize: 25, color: icon === "CaretRightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CaretRightOutlined" ? "#1690ff":"#545454"}}>CaretRightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UpCircleOutlined")} onDoubleClick={onSubmit}>
            <UpCircleOutlined style={{ fontSize: 25, color: icon === "UpCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UpCircleOutlined" ? "#1690ff":"#545454"}}>UpCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DownCircleOutlined")} onDoubleClick={onSubmit}>
            <DownCircleOutlined style={{ fontSize: 25, color: icon === "DownCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DownCircleOutlined" ? "#1690ff":"#545454"}}>DownCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LeftCircleOutlined")} onDoubleClick={onSubmit}>
            <LeftCircleOutlined style={{ fontSize: 25, color: icon === "LeftCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LeftCircleOutlined" ? "#1690ff":"#545454"}}>LeftCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RightCircleOutlined")} onDoubleClick={onSubmit}>
            <RightCircleOutlined style={{ fontSize: 25, color: icon === "RightCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RightCircleOutlined" ? "#1690ff":"#545454"}}>RightCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DoubleRightOutlined")} onDoubleClick={onSubmit}>
            <DoubleRightOutlined style={{ fontSize: 25, color: icon === "DoubleRightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DoubleRightOutlined" ? "#1690ff":"#545454"}}>DoubleRightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DoubleLeftOutlined")} onDoubleClick={onSubmit}>
            <DoubleLeftOutlined style={{ fontSize: 25, color: icon === "DoubleLeftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DoubleLeftOutlined" ? "#1690ff":"#545454"}}>DoubleLeftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("VerticalLeftOutlined")} onDoubleClick={onSubmit}>
            <VerticalLeftOutlined style={{ fontSize: 25, color: icon === "VerticalLeftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "VerticalLeftOutlined" ? "#1690ff":"#545454"}}>VerticalLeftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("VerticalRightOutlined")} onDoubleClick={onSubmit}>
            <VerticalRightOutlined style={{ fontSize: 25, color: icon === "VerticalRightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "VerticalRightOutlined" ? "#1690ff":"#545454"}}>VerticalRightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("VerticalAlignTopOutlined")} onDoubleClick={onSubmit}>
            <VerticalAlignTopOutlined style={{ fontSize: 25, color: icon === "VerticalAlignTopOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "VerticalAlignTopOutlined" ? "#1690ff":"#545454"}}>VerticalAlignTopOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("VerticalAlignMiddleOutlined")} onDoubleClick={onSubmit}>
            <VerticalAlignMiddleOutlined style={{ fontSize: 25, color: icon === "VerticalAlignMiddleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "VerticalAlignMiddleOutlined" ? "#1690ff":"#545454"}}>VerticalAlignMiddleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("VerticalAlignBottomOutlined")} onDoubleClick={onSubmit}>
            <VerticalAlignBottomOutlined style={{ fontSize: 25, color: icon === "VerticalAlignBottomOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "VerticalAlignBottomOutlined" ? "#1690ff":"#545454"}}>VerticalAlignBottomOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ForwardOutlined")} onDoubleClick={onSubmit}>
            <ForwardOutlined style={{ fontSize: 25, color: icon === "ForwardOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ForwardOutlined" ? "#1690ff":"#545454"}}>ForwardOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BackwardOutlined")} onDoubleClick={onSubmit}>
            <BackwardOutlined style={{ fontSize: 25, color: icon === "BackwardOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BackwardOutlined" ? "#1690ff":"#545454"}}>BackwardOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RollbackOutlined")} onDoubleClick={onSubmit}>
            <RollbackOutlined style={{ fontSize: 25, color: icon === "RollbackOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RollbackOutlined" ? "#1690ff":"#545454"}}>RollbackOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("EnterOutlined")} onDoubleClick={onSubmit}>
            <EnterOutlined style={{ fontSize: 25, color: icon === "EnterOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "EnterOutlined" ? "#1690ff":"#545454"}}>EnterOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RetweetOutlined")} onDoubleClick={onSubmit}>
            <RetweetOutlined style={{ fontSize: 25, color: icon === "RetweetOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RetweetOutlined" ? "#1690ff":"#545454"}}>RetweetOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SwapOutlined")} onDoubleClick={onSubmit}>
            <SwapOutlined style={{ fontSize: 25, color: icon === "SwapOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SwapOutlined" ? "#1690ff":"#545454"}}>SwapOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SwapLeftOutlined")} onDoubleClick={onSubmit}>
            <SwapLeftOutlined style={{ fontSize: 25, color: icon === "SwapLeftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SwapLeftOutlined" ? "#1690ff":"#545454"}}>SwapLeftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SwapRightOutlined")} onDoubleClick={onSubmit}>
            <SwapRightOutlined style={{ fontSize: 25, color: icon === "SwapRightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SwapRightOutlined" ? "#1690ff":"#545454"}}>SwapRightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ArrowUpOutlined")} onDoubleClick={onSubmit}>
            <ArrowUpOutlined style={{ fontSize: 25, color: icon === "ArrowUpOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ArrowUpOutlined" ? "#1690ff":"#545454"}}>ArrowUpOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ArrowDownOutlined")} onDoubleClick={onSubmit}>
            <ArrowDownOutlined style={{ fontSize: 25, color: icon === "ArrowDownOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ArrowDownOutlined" ? "#1690ff":"#545454"}}>ArrowDownOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ArrowLeftOutlined")} onDoubleClick={onSubmit}>
            <ArrowLeftOutlined style={{ fontSize: 25, color: icon === "ArrowLeftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ArrowLeftOutlined" ? "#1690ff":"#545454"}}>ArrowLeftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ArrowRightOutlined")} onDoubleClick={onSubmit}>
            <ArrowRightOutlined style={{ fontSize: 25, color: icon === "ArrowRightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ArrowRightOutlined" ? "#1690ff":"#545454"}}>ArrowRightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PlayCircleOutlined")} onDoubleClick={onSubmit}>
            <PlayCircleOutlined style={{ fontSize: 25, color: icon === "PlayCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PlayCircleOutlined" ? "#1690ff":"#545454"}}>PlayCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UpSquareOutlined")} onDoubleClick={onSubmit}>
            <UpSquareOutlined style={{ fontSize: 25, color: icon === "UpSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UpSquareOutlined" ? "#1690ff":"#545454"}}>UpSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DownSquareOutlined")} onDoubleClick={onSubmit}>
            <DownSquareOutlined style={{ fontSize: 25, color: icon === "DownSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DownSquareOutlined" ? "#1690ff":"#545454"}}>DownSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LeftSquareOutlined")} onDoubleClick={onSubmit}>
            <LeftSquareOutlined style={{ fontSize: 25, color: icon === "LeftSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LeftSquareOutlined" ? "#1690ff":"#545454"}}>LeftSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RightSquareOutlined")} onDoubleClick={onSubmit}>
            <RightSquareOutlined style={{ fontSize: 25, color: icon === "RightSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RightSquareOutlined" ? "#1690ff":"#545454"}}>RightSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LoginOutlined")} onDoubleClick={onSubmit}>
            <LoginOutlined style={{ fontSize: 25, color: icon === "LoginOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LoginOutlined" ? "#1690ff":"#545454"}}>LoginOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LogoutOutlined")} onDoubleClick={onSubmit}>
            <LogoutOutlined style={{ fontSize: 25, color: icon === "LogoutOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LogoutOutlined" ? "#1690ff":"#545454"}}>LogoutOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MenuFoldOutlined")} onDoubleClick={onSubmit}>
            <MenuFoldOutlined style={{ fontSize: 25, color: icon === "MenuFoldOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MenuFoldOutlined" ? "#1690ff":"#545454"}}>MenuFoldOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MenuUnfoldOutlined")} onDoubleClick={onSubmit}>
            <MenuUnfoldOutlined style={{ fontSize: 25, color: icon === "MenuUnfoldOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MenuUnfoldOutlined" ? "#1690ff":"#545454"}}>MenuUnfoldOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BorderBottomOutlined")} onDoubleClick={onSubmit}>
            <BorderBottomOutlined style={{ fontSize: 25, color: icon === "BorderBottomOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BorderBottomOutlined" ? "#1690ff":"#545454"}}>BorderBottomOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BorderHorizontalOutlined")} onDoubleClick={onSubmit}>
            <BorderHorizontalOutlined style={{ fontSize: 25, color: icon === "BorderHorizontalOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BorderHorizontalOutlined" ? "#1690ff":"#545454"}}>BorderHorizontalOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BorderInnerOutlined")} onDoubleClick={onSubmit}>
            <BorderInnerOutlined style={{ fontSize: 25, color: icon === "BorderInnerOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BorderInnerOutlined" ? "#1690ff":"#545454"}}>BorderInnerOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BorderOuterOutlined")} onDoubleClick={onSubmit}>
            <BorderOuterOutlined style={{ fontSize: 25, color: icon === "BorderOuterOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BorderOuterOutlined" ? "#1690ff":"#545454"}}>BorderOuterOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BorderLeftOutlined")} onDoubleClick={onSubmit}>
            <BorderLeftOutlined style={{ fontSize: 25, color: icon === "BorderLeftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BorderLeftOutlined" ? "#1690ff":"#545454"}}>BorderLeftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BorderRightOutlined")} onDoubleClick={onSubmit}>
            <BorderRightOutlined style={{ fontSize: 25, color: icon === "BorderRightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BorderRightOutlined" ? "#1690ff":"#545454"}}>BorderRightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BorderTopOutlined")} onDoubleClick={onSubmit}>
            <BorderTopOutlined style={{ fontSize: 25, color: icon === "BorderTopOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BorderTopOutlined" ? "#1690ff":"#545454"}}>BorderTopOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BorderVerticleOutlined")} onDoubleClick={onSubmit}>
            <BorderVerticleOutlined style={{ fontSize: 25, color: icon === "BorderVerticleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BorderVerticleOutlined" ? "#1690ff":"#545454"}}>BorderVerticleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PicCenterOutlined")} onDoubleClick={onSubmit}>
            <PicCenterOutlined style={{ fontSize: 25, color: icon === "PicCenterOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PicCenterOutlined" ? "#1690ff":"#545454"}}>PicCenterOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PicLeftOutlined")} onDoubleClick={onSubmit}>
            <PicLeftOutlined style={{ fontSize: 25, color: icon === "PicLeftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PicLeftOutlined" ? "#1690ff":"#545454"}}>PicLeftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PicRightOutlined")} onDoubleClick={onSubmit}>
            <PicRightOutlined style={{ fontSize: 25, color: icon === "PicRightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PicRightOutlined" ? "#1690ff":"#545454"}}>PicRightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RadiusBottomleftOutlined")} onDoubleClick={onSubmit}>
            <RadiusBottomleftOutlined style={{ fontSize: 25, color: icon === "RadiusBottomleftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RadiusBottomleftOutlined" ? "#1690ff":"#545454"}}>RadiusBottomleftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RadiusBottomrightOutlined")} onDoubleClick={onSubmit}>
            <RadiusBottomrightOutlined style={{ fontSize: 25, color: icon === "RadiusBottomrightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RadiusBottomrightOutlined" ? "#1690ff":"#545454"}}>RadiusBottomrightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RadiusUpleftOutlined")} onDoubleClick={onSubmit}>
            <RadiusUpleftOutlined style={{ fontSize: 25, color: icon === "RadiusUpleftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RadiusUpleftOutlined" ? "#1690ff":"#545454"}}>RadiusUpleftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RadiusUprightOutlined")} onDoubleClick={onSubmit}>
            <RadiusUprightOutlined style={{ fontSize: 25, color: icon === "RadiusUprightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RadiusUprightOutlined" ? "#1690ff":"#545454"}}>RadiusUprightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FullscreenOutlined")} onDoubleClick={onSubmit}>
            <FullscreenOutlined style={{ fontSize: 25, color: icon === "FullscreenOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FullscreenOutlined" ? "#1690ff":"#545454"}}>FullscreenOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FullscreenExitOutlined")} onDoubleClick={onSubmit}>
            <FullscreenExitOutlined style={{ fontSize: 25, color: icon === "FullscreenExitOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FullscreenExitOutlined" ? "#1690ff":"#545454"}}>FullscreenExitOutlined</Paragraph></div>
          </Col>
        </Row>
        <h2 style={{ marginTop: 20 }}>提示建议性图标</h2>
        <Row style={{ textAlign: 'center'}}>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("QuestionOutlined")} onDoubleClick={onSubmit}>
            <QuestionOutlined style={{ fontSize: 25, color: icon === "QuestionOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "QuestionOutlined" ? "#1690ff":"#545454"}}>QuestionOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("QuestionCircleOutlined")} onDoubleClick={onSubmit}>
            <QuestionCircleOutlined style={{ fontSize: 25, color: icon === "QuestionCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "QuestionCircleOutlined" ? "#1690ff":"#545454"}}>QuestionCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PlusOutlined")} onDoubleClick={onSubmit}>
            <PlusOutlined style={{ fontSize: 25, color: icon === "PlusOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PlusOutlined" ? "#1690ff":"#545454"}}>PlusOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PlusCircleOutlined")} onDoubleClick={onSubmit}>
            <PlusCircleOutlined style={{ fontSize: 25, color: icon === "PlusCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PlusCircleOutlined" ? "#1690ff":"#545454"}}>PlusCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PauseOutlined")} onDoubleClick={onSubmit}>
            <PauseOutlined style={{ fontSize: 25, color: icon === "PauseOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PauseOutlined" ? "#1690ff":"#545454"}}>PauseOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PauseCircleOutlined")} onDoubleClick={onSubmit}>
            <PauseCircleOutlined style={{ fontSize: 25, color: icon === "PauseCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PauseCircleOutlined" ? "#1690ff":"#545454"}}>PauseCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MinusOutlined")} onDoubleClick={onSubmit}>
            <MinusOutlined style={{ fontSize: 25, color: icon === "MinusOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MinusOutlined" ? "#1690ff":"#545454"}}>MinusOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MinusCircleOutlined")} onDoubleClick={onSubmit}>
            <MinusCircleOutlined style={{ fontSize: 25, color: icon === "MinusCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MinusCircleOutlined" ? "#1690ff":"#545454"}}>MinusCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PlusSquareOutlined")} onDoubleClick={onSubmit}>
            <PlusSquareOutlined style={{ fontSize: 25, color: icon === "PlusSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PlusSquareOutlined" ? "#1690ff":"#545454"}}>PlusSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MinusSquareOutlined")} onDoubleClick={onSubmit}>
            <MinusSquareOutlined style={{ fontSize: 25, color: icon === "MinusSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MinusSquareOutlined" ? "#1690ff":"#545454"}}>MinusSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("InfoOutlined")} onDoubleClick={onSubmit}>
            <InfoOutlined style={{ fontSize: 25, color: icon === "InfoOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "InfoOutlined" ? "#1690ff":"#545454"}}>InfoOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("InfoCircleOutlined")} onDoubleClick={onSubmit}>
            <InfoCircleOutlined style={{ fontSize: 25, color: icon === "InfoCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "InfoCircleOutlined" ? "#1690ff":"#545454"}}>InfoCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ExclamationOutlined")} onDoubleClick={onSubmit}>
            <ExclamationOutlined style={{ fontSize: 25, color: icon === "ExclamationOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ExclamationOutlined" ? "#1690ff":"#545454"}}>ExclamationOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ExclamationCircleOutlined")} onDoubleClick={onSubmit}>
            <ExclamationCircleOutlined style={{ fontSize: 25, color: icon === "ExclamationCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ExclamationCircleOutlined" ? "#1690ff":"#545454"}}>ExclamationCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CloseOutlined")} onDoubleClick={onSubmit}>
            <CloseOutlined style={{ fontSize: 25, color: icon === "CloseOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CloseOutlined" ? "#1690ff":"#545454"}}>CloseOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CloseCircleOutlined")} onDoubleClick={onSubmit}>
            <CloseCircleOutlined style={{ fontSize: 25, color: icon === "CloseCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CloseCircleOutlined" ? "#1690ff":"#545454"}}>CloseCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CloseSquareOutlined")} onDoubleClick={onSubmit}>
            <CloseSquareOutlined style={{ fontSize: 25, color: icon === "CloseSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CloseSquareOutlined" ? "#1690ff":"#545454"}}>CloseSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CheckOutlined")} onDoubleClick={onSubmit}>
            <CheckOutlined style={{ fontSize: 25, color: icon === "CheckOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CheckOutlined" ? "#1690ff":"#545454"}}>CheckOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CheckCircleOutlined")} onDoubleClick={onSubmit}>
            <CheckCircleOutlined style={{ fontSize: 25, color: icon === "CheckCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CheckCircleOutlined" ? "#1690ff":"#545454"}}>CheckCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CheckSquareOutlined")} onDoubleClick={onSubmit}>
            <CheckSquareOutlined style={{ fontSize: 25, color: icon === "CheckSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CheckSquareOutlined" ? "#1690ff":"#545454"}}>CheckSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ClockCircleOutlined")} onDoubleClick={onSubmit}>
            <ClockCircleOutlined style={{ fontSize: 25, color: icon === "ClockCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ClockCircleOutlined" ? "#1690ff":"#545454"}}>ClockCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("WarningOutlined")} onDoubleClick={onSubmit}>
            <WarningOutlined style={{ fontSize: 25, color: icon === "WarningOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "WarningOutlined" ? "#1690ff":"#545454"}}>WarningOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("IssuesCloseOutlined")} onDoubleClick={onSubmit}>
            <IssuesCloseOutlined style={{ fontSize: 25, color: icon === "IssuesCloseOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "IssuesCloseOutlined" ? "#1690ff":"#545454"}}>IssuesCloseOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("StopOutlined")} onDoubleClick={onSubmit}>
            <StopOutlined style={{ fontSize: 25, color: icon === "StopOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "StopOutlined" ? "#1690ff":"#545454"}}>StopOutlined</Paragraph></div>
          </Col>
        </Row>
        <h2 style={{ marginTop: 20 }}>编辑类图标</h2>
        <Row style={{ textAlign: 'center'}}>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("EditOutlined")} onDoubleClick={onSubmit}>
            <EditOutlined style={{ fontSize: 25, color: icon === "EditOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "EditOutlined" ? "#1690ff":"#545454"}}>EditOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FormOutlined")} onDoubleClick={onSubmit}>
            <FormOutlined style={{ fontSize: 25, color: icon === "FormOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FormOutlined" ? "#1690ff":"#545454"}}>FormOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CopyOutlined")} onDoubleClick={onSubmit}>
            <CopyOutlined style={{ fontSize: 25, color: icon === "CopyOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CopyOutlined" ? "#1690ff":"#545454"}}>CopyOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ScissorOutlined")} onDoubleClick={onSubmit}>
            <ScissorOutlined style={{ fontSize: 25, color: icon === "ScissorOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ScissorOutlined" ? "#1690ff":"#545454"}}>ScissorOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DeleteOutlined")} onDoubleClick={onSubmit}>
            <DeleteOutlined style={{ fontSize: 25, color: icon === "DeleteOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DeleteOutlined" ? "#1690ff":"#545454"}}>DeleteOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SnippetsOutlined")} onDoubleClick={onSubmit}>
            <SnippetsOutlined style={{ fontSize: 25, color: icon === "SnippetsOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SnippetsOutlined" ? "#1690ff":"#545454"}}>SnippetsOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DiffOutlined")} onDoubleClick={onSubmit}>
            <DiffOutlined style={{ fontSize: 25, color: icon === "DiffOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DiffOutlined" ? "#1690ff":"#545454"}}>DiffOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("HighlightOutlined")} onDoubleClick={onSubmit}>
            <HighlightOutlined style={{ fontSize: 25, color: icon === "HighlightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "HighlightOutlined" ? "#1690ff":"#545454"}}>HighlightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AlignCenterOutlined")} onDoubleClick={onSubmit}>
            <AlignCenterOutlined style={{ fontSize: 25, color: icon === "AlignCenterOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AlignCenterOutlined" ? "#1690ff":"#545454"}}>AlignCenterOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AlignLeftOutlined")} onDoubleClick={onSubmit}>
            <AlignLeftOutlined style={{ fontSize: 25, color: icon === "AlignLeftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AlignLeftOutlined" ? "#1690ff":"#545454"}}>AlignLeftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AlignRightOutlined")} onDoubleClick={onSubmit}>
            <AlignRightOutlined style={{ fontSize: 25, color: icon === "AlignRightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AlignRightOutlined" ? "#1690ff":"#545454"}}>AlignRightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BgColorsOutlined")} onDoubleClick={onSubmit}>
            <BgColorsOutlined style={{ fontSize: 25, color: icon === "BgColorsOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BgColorsOutlined" ? "#1690ff":"#545454"}}>BgColorsOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BoldOutlined")} onDoubleClick={onSubmit}>
            <BoldOutlined style={{ fontSize: 25, color: icon === "BoldOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BoldOutlined" ? "#1690ff":"#545454"}}>BoldOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ItalicOutlined")} onDoubleClick={onSubmit}>
            <ItalicOutlined style={{ fontSize: 25, color: icon === "ItalicOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ItalicOutlined" ? "#1690ff":"#545454"}}>ItalicOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UnderlineOutlined")} onDoubleClick={onSubmit}>
            <UnderlineOutlined style={{ fontSize: 25, color: icon === "UnderlineOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UnderlineOutlined" ? "#1690ff":"#545454"}}>UnderlineOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("StrikethroughOutlined")} onDoubleClick={onSubmit}>
            <StrikethroughOutlined style={{ fontSize: 25, color: icon === "StrikethroughOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "StrikethroughOutlined" ? "#1690ff":"#545454"}}>StrikethroughOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RedoOutlined")} onDoubleClick={onSubmit}>
            <RedoOutlined style={{ fontSize: 25, color: icon === "RedoOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RedoOutlined" ? "#1690ff":"#545454"}}>RedoOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UndoOutlined")} onDoubleClick={onSubmit}>
            <UndoOutlined style={{ fontSize: 25, color: icon === "UndoOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UndoOutlined" ? "#1690ff":"#545454"}}>UndoOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ZoomInOutlined")} onDoubleClick={onSubmit}>
            <ZoomInOutlined style={{ fontSize: 25, color: icon === "ZoomInOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ZoomInOutlined" ? "#1690ff":"#545454"}}>ZoomInOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ZoomOutOutlined")} onDoubleClick={onSubmit}>
            <ZoomOutOutlined style={{ fontSize: 25, color: icon === "ZoomOutOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ZoomOutOutlined" ? "#1690ff":"#545454"}}>ZoomOutOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FontColorsOutlined")} onDoubleClick={onSubmit}>
            <FontColorsOutlined style={{ fontSize: 25, color: icon === "FontColorsOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FontColorsOutlined" ? "#1690ff":"#545454"}}>FontColorsOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FontSizeOutlined")} onDoubleClick={onSubmit}>
            <FontSizeOutlined style={{ fontSize: 25, color: icon === "FontSizeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FontSizeOutlined" ? "#1690ff":"#545454"}}>FontSizeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LineHeightOutlined")} onDoubleClick={onSubmit}>
            <LineHeightOutlined style={{ fontSize: 25, color: icon === "LineHeightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LineHeightOutlined" ? "#1690ff":"#545454"}}>LineHeightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DashOutlined")} onDoubleClick={onSubmit}>
            <DashOutlined style={{ fontSize: 25, color: icon === "DashOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DashOutlined" ? "#1690ff":"#545454"}}>DashOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SmallDashOutlined")} onDoubleClick={onSubmit}>
            <SmallDashOutlined style={{ fontSize: 25, color: icon === "SmallDashOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SmallDashOutlined" ? "#1690ff":"#545454"}}>SmallDashOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SortAscendingOutlined")} onDoubleClick={onSubmit}>
            <SortAscendingOutlined style={{ fontSize: 25, color: icon === "SortAscendingOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SortAscendingOutlined" ? "#1690ff":"#545454"}}>SortAscendingOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SortDescendingOutlined")} onDoubleClick={onSubmit}>
            <SortDescendingOutlined style={{ fontSize: 25, color: icon === "SortDescendingOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SortDescendingOutlined" ? "#1690ff":"#545454"}}>SortDescendingOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DragOutlined")} onDoubleClick={onSubmit}>
            <DragOutlined style={{ fontSize: 25, color: icon === "DragOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DragOutlined" ? "#1690ff":"#545454"}}>DragOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("OrderedListOutlined")} onDoubleClick={onSubmit}>
            <OrderedListOutlined style={{ fontSize: 25, color: icon === "OrderedListOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "OrderedListOutlined" ? "#1690ff":"#545454"}}>OrderedListOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UnorderedListOutlined")} onDoubleClick={onSubmit}>
            <UnorderedListOutlined style={{ fontSize: 25, color: icon === "UnorderedListOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UnorderedListOutlined" ? "#1690ff":"#545454"}}>UnorderedListOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RadiusSettingOutlined")} onDoubleClick={onSubmit}>
            <RadiusSettingOutlined style={{ fontSize: 25, color: icon === "RadiusSettingOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RadiusSettingOutlined" ? "#1690ff":"#545454"}}>RadiusSettingOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ColumnWidthOutlined")} onDoubleClick={onSubmit}>
            <ColumnWidthOutlined style={{ fontSize: 25, color: icon === "ColumnWidthOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ColumnWidthOutlined" ? "#1690ff":"#545454"}}>ColumnWidthOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ColumnHeightOutlined")} onDoubleClick={onSubmit}>
            <ColumnHeightOutlined style={{ fontSize: 25, color: icon === "ColumnHeightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ColumnHeightOutlined" ? "#1690ff":"#545454"}}>ColumnHeightOutlined</Paragraph></div>
          </Col>
        </Row>
        <h2 style={{ marginTop: 20 }}>数据类图标</h2>
        <Row style={{ textAlign: 'center'}}>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AreaChartOutlined")} onDoubleClick={onSubmit}>
            <AreaChartOutlined style={{ fontSize: 25, color: icon === "AreaChartOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AreaChartOutlined" ? "#1690ff":"#545454"}}>AreaChartOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PieChartOutlined")} onDoubleClick={onSubmit}>
            <PieChartOutlined style={{ fontSize: 25, color: icon === "PieChartOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PieChartOutlined" ? "#1690ff":"#545454"}}>PieChartOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BarChartOutlined")} onDoubleClick={onSubmit}>
            <BarChartOutlined style={{ fontSize: 25, color: icon === "BarChartOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BarChartOutlined" ? "#1690ff":"#545454"}}>BarChartOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DotChartOutlined")} onDoubleClick={onSubmit}>
            <DotChartOutlined style={{ fontSize: 25, color: icon === "DotChartOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DotChartOutlined" ? "#1690ff":"#545454"}}>DotChartOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LineChartOutlined")} onDoubleClick={onSubmit}>
            <LineChartOutlined style={{ fontSize: 25, color: icon === "LineChartOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LineChartOutlined" ? "#1690ff":"#545454"}}>LineChartOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RadarChartOutlined")} onDoubleClick={onSubmit}>
            <RadarChartOutlined style={{ fontSize: 25, color: icon === "RadarChartOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RadarChartOutlined" ? "#1690ff":"#545454"}}>RadarChartOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("HeatMapOutlined")} onDoubleClick={onSubmit}>
            <HeatMapOutlined style={{ fontSize: 25, color: icon === "HeatMapOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "HeatMapOutlined" ? "#1690ff":"#545454"}}>HeatMapOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FallOutlined")} onDoubleClick={onSubmit}>
            <FallOutlined style={{ fontSize: 25, color: icon === "FallOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FallOutlined" ? "#1690ff":"#545454"}}>FallOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RiseOutlined")} onDoubleClick={onSubmit}>
            <RiseOutlined style={{ fontSize: 25, color: icon === "RiseOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RiseOutlined" ? "#1690ff":"#545454"}}>RiseOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("StockOutlined")} onDoubleClick={onSubmit}>
            <StockOutlined style={{ fontSize: 25, color: icon === "StockOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "StockOutlined" ? "#1690ff":"#545454"}}>StockOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BoxPlotOutlined")} onDoubleClick={onSubmit}>
            <BoxPlotOutlined style={{ fontSize: 25, color: icon === "BoxPlotOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BoxPlotOutlined" ? "#1690ff":"#545454"}}>BoxPlotOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FundOutlined")} onDoubleClick={onSubmit}>
            <FundOutlined style={{ fontSize: 25, color: icon === "FundOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FundOutlined" ? "#1690ff":"#545454"}}>FundOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SlidersOutlined")} onDoubleClick={onSubmit}>
            <SlidersOutlined style={{ fontSize: 25, color: icon === "SlidersOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SlidersOutlined" ? "#1690ff":"#545454"}}>SlidersOutlined</Paragraph></div>
          </Col>
        </Row>
        <h2 style={{ marginTop: 20 }}>品牌和标识</h2>
        <Row style={{ textAlign: 'center'}}>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AndroidOutlined")} onDoubleClick={onSubmit}>
            <AndroidOutlined style={{ fontSize: 25, color: icon === "AndroidOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AndroidOutlined" ? "#1690ff":"#545454"}}>AndroidOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AppleOutlined")} onDoubleClick={onSubmit}>
            <AppleOutlined style={{ fontSize: 25, color: icon === "AppleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AppleOutlined" ? "#1690ff":"#545454"}}>AppleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("WindowsOutlined")} onDoubleClick={onSubmit}>
            <WindowsOutlined style={{ fontSize: 25, color: icon === "WindowsOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "WindowsOutlined" ? "#1690ff":"#545454"}}>WindowsOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("IeOutlined")} onDoubleClick={onSubmit}>
            <IeOutlined style={{ fontSize: 25, color: icon === "IeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "IeOutlined" ? "#1690ff":"#545454"}}>IeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ChromeOutlined")} onDoubleClick={onSubmit}>
            <ChromeOutlined style={{ fontSize: 25, color: icon === "ChromeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ChromeOutlined" ? "#1690ff":"#545454"}}>ChromeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("GithubOutlined")} onDoubleClick={onSubmit}>
            <GithubOutlined style={{ fontSize: 25, color: icon === "GithubOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "GithubOutlined" ? "#1690ff":"#545454"}}>GithubOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AliwangwangOutlined")} onDoubleClick={onSubmit}>
            <AliwangwangOutlined style={{ fontSize: 25, color: icon === "AliwangwangOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AliwangwangOutlined" ? "#1690ff":"#545454"}}>AliwangwangOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DingdingOutlined")} onDoubleClick={onSubmit}>
            <DingdingOutlined style={{ fontSize: 25, color: icon === "DingdingOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DingdingOutlined" ? "#1690ff":"#545454"}}>DingdingOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("WeiboSquareOutlined")} onDoubleClick={onSubmit}>
            <WeiboSquareOutlined style={{ fontSize: 25, color: icon === "WeiboSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "WeiboSquareOutlined" ? "#1690ff":"#545454"}}>WeiboSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("WeiboCircleOutlined")} onDoubleClick={onSubmit}>
            <WeiboCircleOutlined style={{ fontSize: 25, color: icon === "WeiboCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "WeiboCircleOutlined" ? "#1690ff":"#545454"}}>WeiboCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TaobaoCircleOutlined")} onDoubleClick={onSubmit}>
            <TaobaoCircleOutlined style={{ fontSize: 25, color: icon === "TaobaoCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TaobaoCircleOutlined" ? "#1690ff":"#545454"}}>TaobaoCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("Html5Outlined")} onDoubleClick={onSubmit}>
            <Html5Outlined style={{ fontSize: 25, color: icon === "Html5Outlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "Html5Outlined" ? "#1690ff":"#545454"}}>Html5Outlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("WeiboOutlined")} onDoubleClick={onSubmit}>
            <WeiboOutlined style={{ fontSize: 25, color: icon === "WeiboOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "WeiboOutlined" ? "#1690ff":"#545454"}}>WeiboOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TwitterOutlined")} onDoubleClick={onSubmit}>
            <TwitterOutlined style={{ fontSize: 25, color: icon === "TwitterOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TwitterOutlined" ? "#1690ff":"#545454"}}>TwitterOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("WechatOutlined")} onDoubleClick={onSubmit}>
            <WechatOutlined style={{ fontSize: 25, color: icon === "WechatOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "WechatOutlined" ? "#1690ff":"#545454"}}>WechatOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("YoutubeOutlined")} onDoubleClick={onSubmit}>
            <YoutubeOutlined style={{ fontSize: 25, color: icon === "YoutubeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "YoutubeOutlined" ? "#1690ff":"#545454"}}>YoutubeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AlipayCircleOutlined")} onDoubleClick={onSubmit}>
            <AlipayCircleOutlined style={{ fontSize: 25, color: icon === "AlipayCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AlipayCircleOutlined" ? "#1690ff":"#545454"}}>AlipayCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TaobaoOutlined")} onDoubleClick={onSubmit}>
            <TaobaoOutlined style={{ fontSize: 25, color: icon === "TaobaoOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TaobaoOutlined" ? "#1690ff":"#545454"}}>TaobaoOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SkypeOutlined")} onDoubleClick={onSubmit}>
            <SkypeOutlined style={{ fontSize: 25, color: icon === "SkypeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SkypeOutlined" ? "#1690ff":"#545454"}}>SkypeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("QqOutlined")} onDoubleClick={onSubmit}>
            <QqOutlined style={{ fontSize: 25, color: icon === "QqOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "QqOutlined" ? "#1690ff":"#545454"}}>QqOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MediumWorkmarkOutlined")} onDoubleClick={onSubmit}>
            <MediumWorkmarkOutlined style={{ fontSize: 25, color: icon === "MediumWorkmarkOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MediumWorkmarkOutlined" ? "#1690ff":"#545454"}}>MediumWorkmarkOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("GitlabOutlined")} onDoubleClick={onSubmit}>
            <GitlabOutlined style={{ fontSize: 25, color: icon === "GitlabOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "GitlabOutlined" ? "#1690ff":"#545454"}}>GitlabOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MediumOutlined")} onDoubleClick={onSubmit}>
            <MediumOutlined style={{ fontSize: 25, color: icon === "MediumOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MediumOutlined" ? "#1690ff":"#545454"}}>MediumOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LinkedinOutlined")} onDoubleClick={onSubmit}>
            <LinkedinOutlined style={{ fontSize: 25, color: icon === "LinkedinOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LinkedinOutlined" ? "#1690ff":"#545454"}}>LinkedinOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("GooglePlusOutlined")} onDoubleClick={onSubmit}>
            <GooglePlusOutlined style={{ fontSize: 25, color: icon === "GooglePlusOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "GooglePlusOutlined" ? "#1690ff":"#545454"}}>GooglePlusOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DropboxOutlined")} onDoubleClick={onSubmit}>
            <DropboxOutlined style={{ fontSize: 25, color: icon === "DropboxOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DropboxOutlined" ? "#1690ff":"#545454"}}>DropboxOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FacebookOutlined")} onDoubleClick={onSubmit}>
            <FacebookOutlined style={{ fontSize: 25, color: icon === "FacebookOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FacebookOutlined" ? "#1690ff":"#545454"}}>FacebookOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CodepenOutlined")} onDoubleClick={onSubmit}>
            <CodepenOutlined style={{ fontSize: 25, color: icon === "CodepenOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CodepenOutlined" ? "#1690ff":"#545454"}}>CodepenOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CodeSandboxOutlined")} onDoubleClick={onSubmit}>
            <CodeSandboxOutlined style={{ fontSize: 25, color: icon === "CodeSandboxOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CodeSandboxOutlined" ? "#1690ff":"#545454"}}>CodeSandboxOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AmazonOutlined")} onDoubleClick={onSubmit}>
            <AmazonOutlined style={{ fontSize: 25, color: icon === "AmazonOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AmazonOutlined" ? "#1690ff":"#545454"}}>AmazonOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("GoogleOutlined")} onDoubleClick={onSubmit}>
            <GoogleOutlined style={{ fontSize: 25, color: icon === "GoogleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "GoogleOutlined" ? "#1690ff":"#545454"}}>GoogleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CodepenCircleOutlined")} onDoubleClick={onSubmit}>
            <CodepenCircleOutlined style={{ fontSize: 25, color: icon === "CodepenCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CodepenCircleOutlined" ? "#1690ff":"#545454"}}>CodepenCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AlipayOutlined")} onDoubleClick={onSubmit}>
            <AlipayOutlined style={{ fontSize: 25, color: icon === "AlipayOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AlipayOutlined" ? "#1690ff":"#545454"}}>AlipayOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AntDesignOutlined")} onDoubleClick={onSubmit}>
            <AntDesignOutlined style={{ fontSize: 25, color: icon === "AntDesignOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AntDesignOutlined" ? "#1690ff":"#545454"}}>AntDesignOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AntCloudOutlined")} onDoubleClick={onSubmit}>
            <AntCloudOutlined style={{ fontSize: 25, color: icon === "AntCloudOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AntCloudOutlined" ? "#1690ff":"#545454"}}>AntCloudOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AliyunOutlined")} onDoubleClick={onSubmit}>
            <AliyunOutlined style={{ fontSize: 25, color: icon === "AliyunOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AliyunOutlined" ? "#1690ff":"#545454"}}>AliyunOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ZhihuOutlined")} onDoubleClick={onSubmit}>
            <ZhihuOutlined style={{ fontSize: 25, color: icon === "ZhihuOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ZhihuOutlined" ? "#1690ff":"#545454"}}>ZhihuOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SlackOutlined")} onDoubleClick={onSubmit}>
            <SlackOutlined style={{ fontSize: 25, color: icon === "SlackOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SlackOutlined" ? "#1690ff":"#545454"}}>SlackOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SlackSquareOutlined")} onDoubleClick={onSubmit}>
            <SlackSquareOutlined style={{ fontSize: 25, color: icon === "SlackSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SlackSquareOutlined" ? "#1690ff":"#545454"}}>SlackSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BehanceOutlined")} onDoubleClick={onSubmit}>
            <BehanceOutlined style={{ fontSize: 25, color: icon === "BehanceOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BehanceOutlined" ? "#1690ff":"#545454"}}>BehanceOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BehanceSquareOutlined")} onDoubleClick={onSubmit}>
            <BehanceSquareOutlined style={{ fontSize: 25, color: icon === "BehanceSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BehanceSquareOutlined" ? "#1690ff":"#545454"}}>BehanceSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DribbbleOutlined")} onDoubleClick={onSubmit}>
            <DribbbleOutlined style={{ fontSize: 25, color: icon === "DribbbleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DribbbleOutlined" ? "#1690ff":"#545454"}}>DribbbleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DribbbleSquareOutlined")} onDoubleClick={onSubmit}>
            <DribbbleSquareOutlined style={{ fontSize: 25, color: icon === "DribbbleSquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DribbbleSquareOutlined" ? "#1690ff":"#545454"}}>DribbbleSquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("InstagramOutlined")} onDoubleClick={onSubmit}>
            <InstagramOutlined style={{ fontSize: 25, color: icon === "InstagramOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "InstagramOutlined" ? "#1690ff":"#545454"}}>InstagramOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("YuqueOutlined")} onDoubleClick={onSubmit}>
            <YuqueOutlined style={{ fontSize: 25, color: icon === "YuqueOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "YuqueOutlined" ? "#1690ff":"#545454"}}>YuqueOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AlibabaOutlined")} onDoubleClick={onSubmit}>
            <AlibabaOutlined style={{ fontSize: 25, color: icon === "AlibabaOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AlibabaOutlined" ? "#1690ff":"#545454"}}>AlibabaOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("YahooOutlined")} onDoubleClick={onSubmit}>
            <YahooOutlined style={{ fontSize: 25, color: icon === "YahooOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "YahooOutlined" ? "#1690ff":"#545454"}}>YahooOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RedditOutlined")} onDoubleClick={onSubmit}>
            <RedditOutlined style={{ fontSize: 25, color: icon === "RedditOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RedditOutlined" ? "#1690ff":"#545454"}}>RedditOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SketchOutlined")} onDoubleClick={onSubmit}>
            <SketchOutlined style={{ fontSize: 25, color: icon === "SketchOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SketchOutlined" ? "#1690ff":"#545454"}}>SketchOutlined</Paragraph></div>
          </Col>
        </Row>
        <h2 style={{ marginTop: 20 }}>网站通用图标</h2>
        <Row style={{ textAlign: 'center'}}>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AccountBookOutlined")} onDoubleClick={onSubmit}>
            <AccountBookOutlined style={{ fontSize: 25, color: icon === "AccountBookOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AccountBookOutlined" ? "#1690ff":"#545454"}}>AccountBookOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AimOutlined")} onDoubleClick={onSubmit}>
            <AimOutlined style={{ fontSize: 25, color: icon === "AimOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AimOutlined" ? "#1690ff":"#545454"}}>AimOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AlertOutlined")} onDoubleClick={onSubmit}>
            <AlertOutlined style={{ fontSize: 25, color: icon === "AlertOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AlertOutlined" ? "#1690ff":"#545454"}}>AlertOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ApartmentOutlined")} onDoubleClick={onSubmit}>
            <ApartmentOutlined style={{ fontSize: 25, color: icon === "ApartmentOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ApartmentOutlined" ? "#1690ff":"#545454"}}>ApartmentOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ApiOutlined")} onDoubleClick={onSubmit}>
            <ApiOutlined style={{ fontSize: 25, color: icon === "ApiOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ApiOutlined" ? "#1690ff":"#545454"}}>ApiOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AppstoreAddOutlined")} onDoubleClick={onSubmit}>
            <AppstoreAddOutlined style={{ fontSize: 25, color: icon === "AppstoreAddOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AppstoreAddOutlined" ? "#1690ff":"#545454"}}>AppstoreAddOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AppstoreOutlined")} onDoubleClick={onSubmit}>
            <AppstoreOutlined style={{ fontSize: 25, color: icon === "AppstoreOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AppstoreOutlined" ? "#1690ff":"#545454"}}>AppstoreOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AudioOutlined")} onDoubleClick={onSubmit}>
            <AudioOutlined style={{ fontSize: 25, color: icon === "AudioOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AudioOutlined" ? "#1690ff":"#545454"}}>AudioOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AudioMutedOutlined")} onDoubleClick={onSubmit}>
            <AudioMutedOutlined style={{ fontSize: 25, color: icon === "AudioMutedOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AudioMutedOutlined" ? "#1690ff":"#545454"}}>AudioMutedOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("AuditOutlined")} onDoubleClick={onSubmit}>
            <AuditOutlined style={{ fontSize: 25, color: icon === "AuditOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "AuditOutlined" ? "#1690ff":"#545454"}}>AuditOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BankOutlined")} onDoubleClick={onSubmit}>
            <BankOutlined style={{ fontSize: 25, color: icon === "BankOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BankOutlined" ? "#1690ff":"#545454"}}>BankOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BarcodeOutlined")} onDoubleClick={onSubmit}>
            <BarcodeOutlined style={{ fontSize: 25, color: icon === "BarcodeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BarcodeOutlined" ? "#1690ff":"#545454"}}>BarcodeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BarsOutlined")} onDoubleClick={onSubmit}>
            <BarsOutlined style={{ fontSize: 25, color: icon === "BarsOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BarsOutlined" ? "#1690ff":"#545454"}}>BarsOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BellOutlined")} onDoubleClick={onSubmit}>
            <BellOutlined style={{ fontSize: 25, color: icon === "BellOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BellOutlined" ? "#1690ff":"#545454"}}>BellOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BlockOutlined")} onDoubleClick={onSubmit}>
            <BlockOutlined style={{ fontSize: 25, color: icon === "BlockOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BlockOutlined" ? "#1690ff":"#545454"}}>BlockOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BookOutlined")} onDoubleClick={onSubmit}>
            <BookOutlined style={{ fontSize: 25, color: icon === "BookOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BookOutlined" ? "#1690ff":"#545454"}}>BookOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BorderOutlined")} onDoubleClick={onSubmit}>
            <BorderOutlined style={{ fontSize: 25, color: icon === "BorderOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BorderOutlined" ? "#1690ff":"#545454"}}>BorderOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BorderlessTableOutlined")} onDoubleClick={onSubmit}>
            <BorderlessTableOutlined style={{ fontSize: 25, color: icon === "BorderlessTableOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BorderlessTableOutlined" ? "#1690ff":"#545454"}}>BorderlessTableOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BranchesOutlined")} onDoubleClick={onSubmit}>
            <BranchesOutlined style={{ fontSize: 25, color: icon === "BranchesOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BranchesOutlined" ? "#1690ff":"#545454"}}>BranchesOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BugOutlined")} onDoubleClick={onSubmit}>
            <BugOutlined style={{ fontSize: 25, color: icon === "BugOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BugOutlined" ? "#1690ff":"#545454"}}>BugOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BuildOutlined")} onDoubleClick={onSubmit}>
            <BuildOutlined style={{ fontSize: 25, color: icon === "BuildOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BuildOutlined" ? "#1690ff":"#545454"}}>BuildOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("BulbOutlined")} onDoubleClick={onSubmit}>
            <BulbOutlined style={{ fontSize: 25, color: icon === "BulbOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "BulbOutlined" ? "#1690ff":"#545454"}}>BulbOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CalculatorOutlined")} onDoubleClick={onSubmit}>
            <CalculatorOutlined style={{ fontSize: 25, color: icon === "CalculatorOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CalculatorOutlined" ? "#1690ff":"#545454"}}>CalculatorOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CalendarOutlined")} onDoubleClick={onSubmit}>
            <CalendarOutlined style={{ fontSize: 25, color: icon === "CalendarOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CalendarOutlined" ? "#1690ff":"#545454"}}>CalendarOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CameraOutlined")} onDoubleClick={onSubmit}>
            <CameraOutlined style={{ fontSize: 25, color: icon === "CameraOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CameraOutlined" ? "#1690ff":"#545454"}}>CameraOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CarOutlined")} onDoubleClick={onSubmit}>
            <CarOutlined style={{ fontSize: 25, color: icon === "CarOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CarOutlined" ? "#1690ff":"#545454"}}>CarOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CarryOutOutlined")} onDoubleClick={onSubmit}>
            <CarryOutOutlined style={{ fontSize: 25, color: icon === "CarryOutOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CarryOutOutlined" ? "#1690ff":"#545454"}}>CarryOutOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CiCircleOutlined")} onDoubleClick={onSubmit}>
            <CiCircleOutlined style={{ fontSize: 25, color: icon === "CiCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CiCircleOutlined" ? "#1690ff":"#545454"}}>CiCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CiOutlined")} onDoubleClick={onSubmit}>
            <CiOutlined style={{ fontSize: 25, color: icon === "CiOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CiOutlined" ? "#1690ff":"#545454"}}>CiOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ClearOutlined")} onDoubleClick={onSubmit}>
            <ClearOutlined style={{ fontSize: 25, color: icon === "ClearOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ClearOutlined" ? "#1690ff":"#545454"}}>ClearOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CloudDownloadOutlined")} onDoubleClick={onSubmit}>
            <CloudDownloadOutlined style={{ fontSize: 25, color: icon === "CloudDownloadOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CloudDownloadOutlined" ? "#1690ff":"#545454"}}>CloudDownloadOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CloudOutlined")} onDoubleClick={onSubmit}>
            <CloudOutlined style={{ fontSize: 25, color: icon === "CloudOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CloudOutlined" ? "#1690ff":"#545454"}}>CloudOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CloudServerOutlined")} onDoubleClick={onSubmit}>
            <CloudServerOutlined style={{ fontSize: 25, color: icon === "CloudServerOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CloudServerOutlined" ? "#1690ff":"#545454"}}>CloudServerOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CloudSyncOutlined")} onDoubleClick={onSubmit}>
            <CloudSyncOutlined style={{ fontSize: 25, color: icon === "CloudSyncOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CloudSyncOutlined" ? "#1690ff":"#545454"}}>CloudSyncOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CloudUploadOutlined")} onDoubleClick={onSubmit}>
            <CloudUploadOutlined style={{ fontSize: 25, color: icon === "CloudUploadOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CloudUploadOutlined" ? "#1690ff":"#545454"}}>CloudUploadOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ClusterOutlined")} onDoubleClick={onSubmit}>
            <ClusterOutlined style={{ fontSize: 25, color: icon === "ClusterOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ClusterOutlined" ? "#1690ff":"#545454"}}>ClusterOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CodeOutlined")} onDoubleClick={onSubmit}>
            <CodeOutlined style={{ fontSize: 25, color: icon === "CodeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CodeOutlined" ? "#1690ff":"#545454"}}>CodeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CoffeeOutlined")} onDoubleClick={onSubmit}>
            <CoffeeOutlined style={{ fontSize: 25, color: icon === "CoffeeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CoffeeOutlined" ? "#1690ff":"#545454"}}>CoffeeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CommentOutlined")} onDoubleClick={onSubmit}>
            <CommentOutlined style={{ fontSize: 25, color: icon === "CommentOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CommentOutlined" ? "#1690ff":"#545454"}}>CommentOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CompassOutlined")} onDoubleClick={onSubmit}>
            <CompassOutlined style={{ fontSize: 25, color: icon === "CompassOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CompassOutlined" ? "#1690ff":"#545454"}}>CompassOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CompressOutlined")} onDoubleClick={onSubmit}>
            <CompressOutlined style={{ fontSize: 25, color: icon === "CompressOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CompressOutlined" ? "#1690ff":"#545454"}}>CompressOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ConsoleSqlOutlined")} onDoubleClick={onSubmit}>
            <ConsoleSqlOutlined style={{ fontSize: 25, color: icon === "ConsoleSqlOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ConsoleSqlOutlined" ? "#1690ff":"#545454"}}>ConsoleSqlOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ContactsOutlined")} onDoubleClick={onSubmit}>
            <ContactsOutlined style={{ fontSize: 25, color: icon === "ContactsOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ContactsOutlined" ? "#1690ff":"#545454"}}>ContactsOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ContainerOutlined")} onDoubleClick={onSubmit}>
            <ContainerOutlined style={{ fontSize: 25, color: icon === "ContainerOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ContainerOutlined" ? "#1690ff":"#545454"}}>ContainerOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ControlOutlined")} onDoubleClick={onSubmit}>
            <ControlOutlined style={{ fontSize: 25, color: icon === "ControlOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ControlOutlined" ? "#1690ff":"#545454"}}>ControlOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CopyrightCircleOutlined")} onDoubleClick={onSubmit}>
            <CopyrightCircleOutlined style={{ fontSize: 25, color: icon === "CopyrightCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CopyrightCircleOutlined" ? "#1690ff":"#545454"}}>CopyrightCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CopyrightOutlined")} onDoubleClick={onSubmit}>
            <CopyrightOutlined style={{ fontSize: 25, color: icon === "CopyrightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CopyrightOutlined" ? "#1690ff":"#545454"}}>CopyrightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CreditCardOutlined")} onDoubleClick={onSubmit}>
            <CreditCardOutlined style={{ fontSize: 25, color: icon === "CreditCardOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CreditCardOutlined" ? "#1690ff":"#545454"}}>CreditCardOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CrownOutlined")} onDoubleClick={onSubmit}>
            <CrownOutlined style={{ fontSize: 25, color: icon === "CrownOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CrownOutlined" ? "#1690ff":"#545454"}}>CrownOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("CustomerServiceOutlined")} onDoubleClick={onSubmit}>
            <CustomerServiceOutlined style={{ fontSize: 25, color: icon === "CustomerServiceOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "CustomerServiceOutlined" ? "#1690ff":"#545454"}}>CustomerServiceOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DashboardOutlined")} onDoubleClick={onSubmit}>
            <DashboardOutlined style={{ fontSize: 25, color: icon === "DashboardOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DashboardOutlined" ? "#1690ff":"#545454"}}>DashboardOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DatabaseOutlined")} onDoubleClick={onSubmit}>
            <DatabaseOutlined style={{ fontSize: 25, color: icon === "DatabaseOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DatabaseOutlined" ? "#1690ff":"#545454"}}>DatabaseOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DeleteColumnOutlined")} onDoubleClick={onSubmit}>
            <DeleteColumnOutlined style={{ fontSize: 25, color: icon === "DeleteColumnOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DeleteColumnOutlined" ? "#1690ff":"#545454"}}>DeleteColumnOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DeleteRowOutlined")} onDoubleClick={onSubmit}>
            <DeleteRowOutlined style={{ fontSize: 25, color: icon === "DeleteRowOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DeleteRowOutlined" ? "#1690ff":"#545454"}}>DeleteRowOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DeliveredProcedureOutlined")} onDoubleClick={onSubmit}>
            <DeliveredProcedureOutlined style={{ fontSize: 25, color: icon === "DeliveredProcedureOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DeliveredProcedureOutlined" ? "#1690ff":"#545454"}}>DeliveredProcedureOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DeploymentUnitOutlined")} onDoubleClick={onSubmit}>
            <DeploymentUnitOutlined style={{ fontSize: 25, color: icon === "DeploymentUnitOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DeploymentUnitOutlined" ? "#1690ff":"#545454"}}>DeploymentUnitOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DesktopOutlined")} onDoubleClick={onSubmit}>
            <DesktopOutlined style={{ fontSize: 25, color: icon === "DesktopOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DesktopOutlined" ? "#1690ff":"#545454"}}>DesktopOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DingtalkOutlined")} onDoubleClick={onSubmit}>
            <DingtalkOutlined style={{ fontSize: 25, color: icon === "DingtalkOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DingtalkOutlined" ? "#1690ff":"#545454"}}>DingtalkOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DisconnectOutlined")} onDoubleClick={onSubmit}>
            <DisconnectOutlined style={{ fontSize: 25, color: icon === "DisconnectOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DisconnectOutlined" ? "#1690ff":"#545454"}}>DisconnectOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DislikeOutlined")} onDoubleClick={onSubmit}>
            <DislikeOutlined style={{ fontSize: 25, color: icon === "DislikeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DislikeOutlined" ? "#1690ff":"#545454"}}>DislikeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DollarCircleOutlined")} onDoubleClick={onSubmit}>
            <DollarCircleOutlined style={{ fontSize: 25, color: icon === "DollarCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DollarCircleOutlined" ? "#1690ff":"#545454"}}>DollarCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DollarOutlined")} onDoubleClick={onSubmit}>
            <DollarOutlined style={{ fontSize: 25, color: icon === "DollarOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DollarOutlined" ? "#1690ff":"#545454"}}>DollarOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("DownloadOutlined")} onDoubleClick={onSubmit}>
            <DownloadOutlined style={{ fontSize: 25, color: icon === "DownloadOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "DownloadOutlined" ? "#1690ff":"#545454"}}>DownloadOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("EllipsisOutlined")} onDoubleClick={onSubmit}>
            <EllipsisOutlined style={{ fontSize: 25, color: icon === "EllipsisOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "EllipsisOutlined" ? "#1690ff":"#545454"}}>EllipsisOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("EnvironmentOutlined")} onDoubleClick={onSubmit}>
            <EnvironmentOutlined style={{ fontSize: 25, color: icon === "EnvironmentOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "EnvironmentOutlined" ? "#1690ff":"#545454"}}>EnvironmentOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("EuroCircleOutlined")} onDoubleClick={onSubmit}>
            <EuroCircleOutlined style={{ fontSize: 25, color: icon === "EuroCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "EuroCircleOutlined" ? "#1690ff":"#545454"}}>EuroCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("EuroOutlined")} onDoubleClick={onSubmit}>
            <EuroOutlined style={{ fontSize: 25, color: icon === "EuroOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "EuroOutlined" ? "#1690ff":"#545454"}}>EuroOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ExceptionOutlined")} onDoubleClick={onSubmit}>
            <ExceptionOutlined style={{ fontSize: 25, color: icon === "ExceptionOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ExceptionOutlined" ? "#1690ff":"#545454"}}>ExceptionOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ExpandAltOutlined")} onDoubleClick={onSubmit}>
            <ExpandAltOutlined style={{ fontSize: 25, color: icon === "ExpandAltOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ExpandAltOutlined" ? "#1690ff":"#545454"}}>ExpandAltOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ExpandOutlined")} onDoubleClick={onSubmit}>
            <ExpandOutlined style={{ fontSize: 25, color: icon === "ExpandOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ExpandOutlined" ? "#1690ff":"#545454"}}>ExpandOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ExperimentOutlined")} onDoubleClick={onSubmit}>
            <ExperimentOutlined style={{ fontSize: 25, color: icon === "ExperimentOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ExperimentOutlined" ? "#1690ff":"#545454"}}>ExperimentOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ExportOutlined")} onDoubleClick={onSubmit}>
            <ExportOutlined style={{ fontSize: 25, color: icon === "ExportOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ExportOutlined" ? "#1690ff":"#545454"}}>ExportOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("EyeOutlined")} onDoubleClick={onSubmit}>
            <EyeOutlined style={{ fontSize: 25, color: icon === "EyeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "EyeOutlined" ? "#1690ff":"#545454"}}>EyeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("EyeInvisibleOutlined")} onDoubleClick={onSubmit}>
            <EyeInvisibleOutlined style={{ fontSize: 25, color: icon === "EyeInvisibleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "EyeInvisibleOutlined" ? "#1690ff":"#545454"}}>EyeInvisibleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FieldBinaryOutlined")} onDoubleClick={onSubmit}>
            <FieldBinaryOutlined style={{ fontSize: 25, color: icon === "FieldBinaryOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FieldBinaryOutlined" ? "#1690ff":"#545454"}}>FieldBinaryOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FieldNumberOutlined")} onDoubleClick={onSubmit}>
            <FieldNumberOutlined style={{ fontSize: 25, color: icon === "FieldNumberOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FieldNumberOutlined" ? "#1690ff":"#545454"}}>FieldNumberOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FieldStringOutlined")} onDoubleClick={onSubmit}>
            <FieldStringOutlined style={{ fontSize: 25, color: icon === "FieldStringOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FieldStringOutlined" ? "#1690ff":"#545454"}}>FieldStringOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FieldTimeOutlined")} onDoubleClick={onSubmit}>
            <FieldTimeOutlined style={{ fontSize: 25, color: icon === "FieldTimeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FieldTimeOutlined" ? "#1690ff":"#545454"}}>FieldTimeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileAddOutlined")} onDoubleClick={onSubmit}>
            <FileAddOutlined style={{ fontSize: 25, color: icon === "FileAddOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileAddOutlined" ? "#1690ff":"#545454"}}>FileAddOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileDoneOutlined")} onDoubleClick={onSubmit}>
            <FileDoneOutlined style={{ fontSize: 25, color: icon === "FileDoneOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileDoneOutlined" ? "#1690ff":"#545454"}}>FileDoneOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileExcelOutlined")} onDoubleClick={onSubmit}>
            <FileExcelOutlined style={{ fontSize: 25, color: icon === "FileExcelOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileExcelOutlined" ? "#1690ff":"#545454"}}>FileExcelOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileExclamationOutlined")} onDoubleClick={onSubmit}>
            <FileExclamationOutlined style={{ fontSize: 25, color: icon === "FileExclamationOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileExclamationOutlined" ? "#1690ff":"#545454"}}>FileExclamationOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileOutlined")} onDoubleClick={onSubmit}>
            <FileOutlined style={{ fontSize: 25, color: icon === "FileOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileOutlined" ? "#1690ff":"#545454"}}>FileOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileGifOutlined")} onDoubleClick={onSubmit}>
            <FileGifOutlined style={{ fontSize: 25, color: icon === "FileGifOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileGifOutlined" ? "#1690ff":"#545454"}}>FileGifOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileImageOutlined")} onDoubleClick={onSubmit}>
            <FileImageOutlined style={{ fontSize: 25, color: icon === "FileImageOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileImageOutlined" ? "#1690ff":"#545454"}}>FileImageOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileJpgOutlined")} onDoubleClick={onSubmit}>
            <FileJpgOutlined style={{ fontSize: 25, color: icon === "FileJpgOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileJpgOutlined" ? "#1690ff":"#545454"}}>FileJpgOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileMarkdownOutlined")} onDoubleClick={onSubmit}>
            <FileMarkdownOutlined style={{ fontSize: 25, color: icon === "FileMarkdownOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileMarkdownOutlined" ? "#1690ff":"#545454"}}>FileMarkdownOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FilePdfOutlined")} onDoubleClick={onSubmit}>
            <FilePdfOutlined style={{ fontSize: 25, color: icon === "FilePdfOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FilePdfOutlined" ? "#1690ff":"#545454"}}>FilePdfOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FilePptOutlined")} onDoubleClick={onSubmit}>
            <FilePptOutlined style={{ fontSize: 25, color: icon === "FilePptOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FilePptOutlined" ? "#1690ff":"#545454"}}>FilePptOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileProtectOutlined")} onDoubleClick={onSubmit}>
            <FileProtectOutlined style={{ fontSize: 25, color: icon === "FileProtectOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileProtectOutlined" ? "#1690ff":"#545454"}}>FileProtectOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileSearchOutlined")} onDoubleClick={onSubmit}>
            <FileSearchOutlined style={{ fontSize: 25, color: icon === "FileSearchOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileSearchOutlined" ? "#1690ff":"#545454"}}>FileSearchOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileSyncOutlined")} onDoubleClick={onSubmit}>
            <FileSyncOutlined style={{ fontSize: 25, color: icon === "FileSyncOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileSyncOutlined" ? "#1690ff":"#545454"}}>FileSyncOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileTextOutlined")} onDoubleClick={onSubmit}>
            <FileTextOutlined style={{ fontSize: 25, color: icon === "FileTextOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileTextOutlined" ? "#1690ff":"#545454"}}>FileTextOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileUnknownOutlined")} onDoubleClick={onSubmit}>
            <FileUnknownOutlined style={{ fontSize: 25, color: icon === "FileUnknownOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileUnknownOutlined" ? "#1690ff":"#545454"}}>FileUnknownOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileWordOutlined")} onDoubleClick={onSubmit}>
            <FileWordOutlined style={{ fontSize: 25, color: icon === "FileWordOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileWordOutlined" ? "#1690ff":"#545454"}}>FileWordOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FileZipOutlined")} onDoubleClick={onSubmit}>
            <FileZipOutlined style={{ fontSize: 25, color: icon === "FileZipOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FileZipOutlined" ? "#1690ff":"#545454"}}>FileZipOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FilterOutlined")} onDoubleClick={onSubmit}>
            <FilterOutlined style={{ fontSize: 25, color: icon === "FilterOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FilterOutlined" ? "#1690ff":"#545454"}}>FilterOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FireOutlined")} onDoubleClick={onSubmit}>
            <FireOutlined style={{ fontSize: 25, color: icon === "FireOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FireOutlined" ? "#1690ff":"#545454"}}>FireOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FlagOutlined")} onDoubleClick={onSubmit}>
            <FlagOutlined style={{ fontSize: 25, color: icon === "FlagOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FlagOutlined" ? "#1690ff":"#545454"}}>FlagOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FolderAddOutlined")} onDoubleClick={onSubmit}>
            <FolderAddOutlined style={{ fontSize: 25, color: icon === "FolderAddOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FolderAddOutlined" ? "#1690ff":"#545454"}}>FolderAddOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FolderOutlined")} onDoubleClick={onSubmit}>
            <FolderOutlined style={{ fontSize: 25, color: icon === "FolderOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FolderOutlined" ? "#1690ff":"#545454"}}>FolderOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FolderOpenOutlined")} onDoubleClick={onSubmit}>
            <FolderOpenOutlined style={{ fontSize: 25, color: icon === "FolderOpenOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FolderOpenOutlined" ? "#1690ff":"#545454"}}>FolderOpenOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FolderViewOutlined")} onDoubleClick={onSubmit}>
            <FolderViewOutlined style={{ fontSize: 25, color: icon === "FolderViewOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FolderViewOutlined" ? "#1690ff":"#545454"}}>FolderViewOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ForkOutlined")} onDoubleClick={onSubmit}>
            <ForkOutlined style={{ fontSize: 25, color: icon === "ForkOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ForkOutlined" ? "#1690ff":"#545454"}}>ForkOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FormatPainterOutlined")} onDoubleClick={onSubmit}>
            <FormatPainterOutlined style={{ fontSize: 25, color: icon === "FormatPainterOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FormatPainterOutlined" ? "#1690ff":"#545454"}}>FormatPainterOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FrownOutlined")} onDoubleClick={onSubmit}>
            <FrownOutlined style={{ fontSize: 25, color: icon === "FrownOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FrownOutlined" ? "#1690ff":"#545454"}}>FrownOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FunctionOutlined")} onDoubleClick={onSubmit}>
            <FunctionOutlined style={{ fontSize: 25, color: icon === "FunctionOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FunctionOutlined" ? "#1690ff":"#545454"}}>FunctionOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FundProjectionScreenOutlined")} onDoubleClick={onSubmit}>
            <FundProjectionScreenOutlined style={{ fontSize: 25, color: icon === "FundProjectionScreenOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FundProjectionScreenOutlined" ? "#1690ff":"#545454"}}>FundProjectionScreenOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FundViewOutlined")} onDoubleClick={onSubmit}>
            <FundViewOutlined style={{ fontSize: 25, color: icon === "FundViewOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FundViewOutlined" ? "#1690ff":"#545454"}}>FundViewOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("FunnelPlotOutlined")} onDoubleClick={onSubmit}>
            <FunnelPlotOutlined style={{ fontSize: 25, color: icon === "FunnelPlotOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "FunnelPlotOutlined" ? "#1690ff":"#545454"}}>FunnelPlotOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("GatewayOutlined")} onDoubleClick={onSubmit}>
            <GatewayOutlined style={{ fontSize: 25, color: icon === "GatewayOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "GatewayOutlined" ? "#1690ff":"#545454"}}>GatewayOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("GifOutlined")} onDoubleClick={onSubmit}>
            <GifOutlined style={{ fontSize: 25, color: icon === "GifOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "GifOutlined" ? "#1690ff":"#545454"}}>GifOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("GiftOutlined")} onDoubleClick={onSubmit}>
            <GiftOutlined style={{ fontSize: 25, color: icon === "GiftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "GiftOutlined" ? "#1690ff":"#545454"}}>GiftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("GlobalOutlined")} onDoubleClick={onSubmit}>
            <GlobalOutlined style={{ fontSize: 25, color: icon === "GlobalOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "GlobalOutlined" ? "#1690ff":"#545454"}}>GlobalOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("GoldOutlined")} onDoubleClick={onSubmit}>
            <GoldOutlined style={{ fontSize: 25, color: icon === "GoldOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "GoldOutlined" ? "#1690ff":"#545454"}}>GoldOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("GroupOutlined")} onDoubleClick={onSubmit}>
            <GroupOutlined style={{ fontSize: 25, color: icon === "GroupOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "GroupOutlined" ? "#1690ff":"#545454"}}>GroupOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("HddOutlined")} onDoubleClick={onSubmit}>
            <HddOutlined style={{ fontSize: 25, color: icon === "HddOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "HddOutlined" ? "#1690ff":"#545454"}}>HddOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("HeartOutlined")} onDoubleClick={onSubmit}>
            <HeartOutlined style={{ fontSize: 25, color: icon === "HeartOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "HeartOutlined" ? "#1690ff":"#545454"}}>HeartOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("HistoryOutlined")} onDoubleClick={onSubmit}>
            <HistoryOutlined style={{ fontSize: 25, color: icon === "HistoryOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "HistoryOutlined" ? "#1690ff":"#545454"}}>HistoryOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("HomeOutlined")} onDoubleClick={onSubmit}>
            <HomeOutlined style={{ fontSize: 25, color: icon === "HomeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "HomeOutlined" ? "#1690ff":"#545454"}}>HomeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("HourglassOutlined")} onDoubleClick={onSubmit}>
            <HourglassOutlined style={{ fontSize: 25, color: icon === "HourglassOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "HourglassOutlined" ? "#1690ff":"#545454"}}>HourglassOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("IdcardOutlined")} onDoubleClick={onSubmit}>
            <IdcardOutlined style={{ fontSize: 25, color: icon === "IdcardOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "IdcardOutlined" ? "#1690ff":"#545454"}}>IdcardOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ImportOutlined")} onDoubleClick={onSubmit}>
            <ImportOutlined style={{ fontSize: 25, color: icon === "ImportOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ImportOutlined" ? "#1690ff":"#545454"}}>ImportOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("InboxOutlined")} onDoubleClick={onSubmit}>
            <InboxOutlined style={{ fontSize: 25, color: icon === "InboxOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "InboxOutlined" ? "#1690ff":"#545454"}}>InboxOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("InsertRowAboveOutlined")} onDoubleClick={onSubmit}>
            <InsertRowAboveOutlined style={{ fontSize: 25, color: icon === "InsertRowAboveOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "InsertRowAboveOutlined" ? "#1690ff":"#545454"}}>InsertRowAboveOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("InsertRowBelowOutlined")} onDoubleClick={onSubmit}>
            <InsertRowBelowOutlined style={{ fontSize: 25, color: icon === "InsertRowBelowOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "InsertRowBelowOutlined" ? "#1690ff":"#545454"}}>InsertRowBelowOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("InsertRowLeftOutlined")} onDoubleClick={onSubmit}>
            <InsertRowLeftOutlined style={{ fontSize: 25, color: icon === "InsertRowLeftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "InsertRowLeftOutlined" ? "#1690ff":"#545454"}}>InsertRowLeftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("InsertRowRightOutlined")} onDoubleClick={onSubmit}>
            <InsertRowRightOutlined style={{ fontSize: 25, color: icon === "InsertRowRightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "InsertRowRightOutlined" ? "#1690ff":"#545454"}}>InsertRowRightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("InsuranceOutlined")} onDoubleClick={onSubmit}>
            <InsuranceOutlined style={{ fontSize: 25, color: icon === "InsuranceOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "InsuranceOutlined" ? "#1690ff":"#545454"}}>InsuranceOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("InteractionOutlined")} onDoubleClick={onSubmit}>
            <InteractionOutlined style={{ fontSize: 25, color: icon === "InteractionOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "InteractionOutlined" ? "#1690ff":"#545454"}}>InteractionOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("KeyOutlined")} onDoubleClick={onSubmit}>
            <KeyOutlined style={{ fontSize: 25, color: icon === "KeyOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "KeyOutlined" ? "#1690ff":"#545454"}}>KeyOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LaptopOutlined")} onDoubleClick={onSubmit}>
            <LaptopOutlined style={{ fontSize: 25, color: icon === "LaptopOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LaptopOutlined" ? "#1690ff":"#545454"}}>LaptopOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LayoutOutlined")} onDoubleClick={onSubmit}>
            <LayoutOutlined style={{ fontSize: 25, color: icon === "LayoutOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LayoutOutlined" ? "#1690ff":"#545454"}}>LayoutOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LikeOutlined")} onDoubleClick={onSubmit}>
            <LikeOutlined style={{ fontSize: 25, color: icon === "LikeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LikeOutlined" ? "#1690ff":"#545454"}}>LikeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LineOutlined")} onDoubleClick={onSubmit}>
            <LineOutlined style={{ fontSize: 25, color: icon === "LineOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LineOutlined" ? "#1690ff":"#545454"}}>LineOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LinkOutlined")} onDoubleClick={onSubmit}>
            <LinkOutlined style={{ fontSize: 25, color: icon === "LinkOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LinkOutlined" ? "#1690ff":"#545454"}}>LinkOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("Loading3QuartersOutlined")} onDoubleClick={onSubmit}>
            <Loading3QuartersOutlined style={{ fontSize: 25, color: icon === "Loading3QuartersOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "Loading3QuartersOutlined" ? "#1690ff":"#545454"}}>Loading3QuartersOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LoadingOutlined")} onDoubleClick={onSubmit}>
            <LoadingOutlined style={{ fontSize: 25, color: icon === "LoadingOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LoadingOutlined" ? "#1690ff":"#545454"}}>LoadingOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("LockOutlined")} onDoubleClick={onSubmit}>
            <LockOutlined style={{ fontSize: 25, color: icon === "LockOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "LockOutlined" ? "#1690ff":"#545454"}}>LockOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MacCommandOutlined")} onDoubleClick={onSubmit}>
            <MacCommandOutlined style={{ fontSize: 25, color: icon === "MacCommandOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MacCommandOutlined" ? "#1690ff":"#545454"}}>MacCommandOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MailOutlined")} onDoubleClick={onSubmit}>
            <MailOutlined style={{ fontSize: 25, color: icon === "MailOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MailOutlined" ? "#1690ff":"#545454"}}>MailOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ManOutlined")} onDoubleClick={onSubmit}>
            <ManOutlined style={{ fontSize: 25, color: icon === "ManOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ManOutlined" ? "#1690ff":"#545454"}}>ManOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MedicineBoxOutlined")} onDoubleClick={onSubmit}>
            <MedicineBoxOutlined style={{ fontSize: 25, color: icon === "MedicineBoxOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MedicineBoxOutlined" ? "#1690ff":"#545454"}}>MedicineBoxOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MehOutlined")} onDoubleClick={onSubmit}>
            <MehOutlined style={{ fontSize: 25, color: icon === "MehOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MehOutlined" ? "#1690ff":"#545454"}}>MehOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MenuOutlined")} onDoubleClick={onSubmit}>
            <MenuOutlined style={{ fontSize: 25, color: icon === "MenuOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MenuOutlined" ? "#1690ff":"#545454"}}>MenuOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MergeCellsOutlined")} onDoubleClick={onSubmit}>
            <MergeCellsOutlined style={{ fontSize: 25, color: icon === "MergeCellsOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MergeCellsOutlined" ? "#1690ff":"#545454"}}>MergeCellsOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MessageOutlined")} onDoubleClick={onSubmit}>
            <MessageOutlined style={{ fontSize: 25, color: icon === "MessageOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MessageOutlined" ? "#1690ff":"#545454"}}>MessageOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MobileOutlined")} onDoubleClick={onSubmit}>
            <MobileOutlined style={{ fontSize: 25, color: icon === "MobileOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MobileOutlined" ? "#1690ff":"#545454"}}>MobileOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MoneyCollectOutlined")} onDoubleClick={onSubmit}>
            <MoneyCollectOutlined style={{ fontSize: 25, color: icon === "MoneyCollectOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MoneyCollectOutlined" ? "#1690ff":"#545454"}}>MoneyCollectOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MonitorOutlined")} onDoubleClick={onSubmit}>
            <MonitorOutlined style={{ fontSize: 25, color: icon === "MonitorOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MonitorOutlined" ? "#1690ff":"#545454"}}>MonitorOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("MoreOutlined")} onDoubleClick={onSubmit}>
            <MoreOutlined style={{ fontSize: 25, color: icon === "MoreOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "MoreOutlined" ? "#1690ff":"#545454"}}>MoreOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("NodeCollapseOutlined")} onDoubleClick={onSubmit}>
            <NodeCollapseOutlined style={{ fontSize: 25, color: icon === "NodeCollapseOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "NodeCollapseOutlined" ? "#1690ff":"#545454"}}>NodeCollapseOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("NodeExpandOutlined")} onDoubleClick={onSubmit}>
            <NodeExpandOutlined style={{ fontSize: 25, color: icon === "NodeExpandOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "NodeExpandOutlined" ? "#1690ff":"#545454"}}>NodeExpandOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("NodeIndexOutlined")} onDoubleClick={onSubmit}>
            <NodeIndexOutlined style={{ fontSize: 25, color: icon === "NodeIndexOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "NodeIndexOutlined" ? "#1690ff":"#545454"}}>NodeIndexOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("NotificationOutlined")} onDoubleClick={onSubmit}>
            <NotificationOutlined style={{ fontSize: 25, color: icon === "NotificationOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "NotificationOutlined" ? "#1690ff":"#545454"}}>NotificationOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("NumberOutlined")} onDoubleClick={onSubmit}>
            <NumberOutlined style={{ fontSize: 25, color: icon === "NumberOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "NumberOutlined" ? "#1690ff":"#545454"}}>NumberOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("OneToOneOutlined")} onDoubleClick={onSubmit}>
            <OneToOneOutlined style={{ fontSize: 25, color: icon === "OneToOneOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "OneToOneOutlined" ? "#1690ff":"#545454"}}>OneToOneOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PaperClipOutlined")} onDoubleClick={onSubmit}>
            <PaperClipOutlined style={{ fontSize: 25, color: icon === "PaperClipOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PaperClipOutlined" ? "#1690ff":"#545454"}}>PaperClipOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PartitionOutlined")} onDoubleClick={onSubmit}>
            <PartitionOutlined style={{ fontSize: 25, color: icon === "PartitionOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PartitionOutlined" ? "#1690ff":"#545454"}}>PartitionOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PayCircleOutlined")} onDoubleClick={onSubmit}>
            <PayCircleOutlined style={{ fontSize: 25, color: icon === "PayCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PayCircleOutlined" ? "#1690ff":"#545454"}}>PayCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PercentageOutlined")} onDoubleClick={onSubmit}>
            <PercentageOutlined style={{ fontSize: 25, color: icon === "PercentageOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PercentageOutlined" ? "#1690ff":"#545454"}}>PercentageOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PhoneOutlined")} onDoubleClick={onSubmit}>
            <PhoneOutlined style={{ fontSize: 25, color: icon === "PhoneOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PhoneOutlined" ? "#1690ff":"#545454"}}>PhoneOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PictureOutlined")} onDoubleClick={onSubmit}>
            <PictureOutlined style={{ fontSize: 25, color: icon === "PictureOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PictureOutlined" ? "#1690ff":"#545454"}}>PictureOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PlaySquareOutlined")} onDoubleClick={onSubmit}>
            <PlaySquareOutlined style={{ fontSize: 25, color: icon === "PlaySquareOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PlaySquareOutlined" ? "#1690ff":"#545454"}}>PlaySquareOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PoundCircleOutlined")} onDoubleClick={onSubmit}>
            <PoundCircleOutlined style={{ fontSize: 25, color: icon === "PoundCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PoundCircleOutlined" ? "#1690ff":"#545454"}}>PoundCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PoundOutlined")} onDoubleClick={onSubmit}>
            <PoundOutlined style={{ fontSize: 25, color: icon === "PoundOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PoundOutlined" ? "#1690ff":"#545454"}}>PoundOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PoweroffOutlined")} onDoubleClick={onSubmit}>
            <PoweroffOutlined style={{ fontSize: 25, color: icon === "PoweroffOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PoweroffOutlined" ? "#1690ff":"#545454"}}>PoweroffOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PrinterOutlined")} onDoubleClick={onSubmit}>
            <PrinterOutlined style={{ fontSize: 25, color: icon === "PrinterOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PrinterOutlined" ? "#1690ff":"#545454"}}>PrinterOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ProfileOutlined")} onDoubleClick={onSubmit}>
            <ProfileOutlined style={{ fontSize: 25, color: icon === "ProfileOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ProfileOutlined" ? "#1690ff":"#545454"}}>ProfileOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ProjectOutlined")} onDoubleClick={onSubmit}>
            <ProjectOutlined style={{ fontSize: 25, color: icon === "ProjectOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ProjectOutlined" ? "#1690ff":"#545454"}}>ProjectOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PropertySafetyOutlined")} onDoubleClick={onSubmit}>
            <PropertySafetyOutlined style={{ fontSize: 25, color: icon === "PropertySafetyOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PropertySafetyOutlined" ? "#1690ff":"#545454"}}>PropertySafetyOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PullRequestOutlined")} onDoubleClick={onSubmit}>
            <PullRequestOutlined style={{ fontSize: 25, color: icon === "PullRequestOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PullRequestOutlined" ? "#1690ff":"#545454"}}>PullRequestOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("PushpinOutlined")} onDoubleClick={onSubmit}>
            <PushpinOutlined style={{ fontSize: 25, color: icon === "PushpinOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "PushpinOutlined" ? "#1690ff":"#545454"}}>PushpinOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("QrcodeOutlined")} onDoubleClick={onSubmit}>
            <QrcodeOutlined style={{ fontSize: 25, color: icon === "QrcodeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "QrcodeOutlined" ? "#1690ff":"#545454"}}>QrcodeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ReadOutlined")} onDoubleClick={onSubmit}>
            <ReadOutlined style={{ fontSize: 25, color: icon === "ReadOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ReadOutlined" ? "#1690ff":"#545454"}}>ReadOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ReconciliationOutlined")} onDoubleClick={onSubmit}>
            <ReconciliationOutlined style={{ fontSize: 25, color: icon === "ReconciliationOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ReconciliationOutlined" ? "#1690ff":"#545454"}}>ReconciliationOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RedEnvelopeOutlined")} onDoubleClick={onSubmit}>
            <RedEnvelopeOutlined style={{ fontSize: 25, color: icon === "RedEnvelopeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RedEnvelopeOutlined" ? "#1690ff":"#545454"}}>RedEnvelopeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ReloadOutlined")} onDoubleClick={onSubmit}>
            <ReloadOutlined style={{ fontSize: 25, color: icon === "ReloadOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ReloadOutlined" ? "#1690ff":"#545454"}}>ReloadOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RestOutlined")} onDoubleClick={onSubmit}>
            <RestOutlined style={{ fontSize: 25, color: icon === "RestOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RestOutlined" ? "#1690ff":"#545454"}}>RestOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RobotOutlined")} onDoubleClick={onSubmit}>
            <RobotOutlined style={{ fontSize: 25, color: icon === "RobotOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RobotOutlined" ? "#1690ff":"#545454"}}>RobotOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RocketOutlined")} onDoubleClick={onSubmit}>
            <RocketOutlined style={{ fontSize: 25, color: icon === "RocketOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RocketOutlined" ? "#1690ff":"#545454"}}>RocketOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RotateLeftOutlined")} onDoubleClick={onSubmit}>
            <RotateLeftOutlined style={{ fontSize: 25, color: icon === "RotateLeftOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RotateLeftOutlined" ? "#1690ff":"#545454"}}>RotateLeftOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("RotateRightOutlined")} onDoubleClick={onSubmit}>
            <RotateRightOutlined style={{ fontSize: 25, color: icon === "RotateRightOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "RotateRightOutlined" ? "#1690ff":"#545454"}}>RotateRightOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SafetyCertificateOutlined")} onDoubleClick={onSubmit}>
            <SafetyCertificateOutlined style={{ fontSize: 25, color: icon === "SafetyCertificateOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SafetyCertificateOutlined" ? "#1690ff":"#545454"}}>SafetyCertificateOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SafetyOutlined")} onDoubleClick={onSubmit}>
            <SafetyOutlined style={{ fontSize: 25, color: icon === "SafetyOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SafetyOutlined" ? "#1690ff":"#545454"}}>SafetyOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SaveOutlined")} onDoubleClick={onSubmit}>
            <SaveOutlined style={{ fontSize: 25, color: icon === "SaveOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SaveOutlined" ? "#1690ff":"#545454"}}>SaveOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ScanOutlined")} onDoubleClick={onSubmit}>
            <ScanOutlined style={{ fontSize: 25, color: icon === "ScanOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ScanOutlined" ? "#1690ff":"#545454"}}>ScanOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ScheduleOutlined")} onDoubleClick={onSubmit}>
            <ScheduleOutlined style={{ fontSize: 25, color: icon === "ScheduleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ScheduleOutlined" ? "#1690ff":"#545454"}}>ScheduleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SearchOutlined")} onDoubleClick={onSubmit}>
            <SearchOutlined style={{ fontSize: 25, color: icon === "SearchOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SearchOutlined" ? "#1690ff":"#545454"}}>SearchOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SecurityScanOutlined")} onDoubleClick={onSubmit}>
            <SecurityScanOutlined style={{ fontSize: 25, color: icon === "SecurityScanOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SecurityScanOutlined" ? "#1690ff":"#545454"}}>SecurityScanOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SelectOutlined")} onDoubleClick={onSubmit}>
            <SelectOutlined style={{ fontSize: 25, color: icon === "SelectOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SelectOutlined" ? "#1690ff":"#545454"}}>SelectOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SendOutlined")} onDoubleClick={onSubmit}>
            <SendOutlined style={{ fontSize: 25, color: icon === "SendOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SendOutlined" ? "#1690ff":"#545454"}}>SendOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SettingOutlined")} onDoubleClick={onSubmit}>
            <SettingOutlined style={{ fontSize: 25, color: icon === "SettingOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SettingOutlined" ? "#1690ff":"#545454"}}>SettingOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ShakeOutlined")} onDoubleClick={onSubmit}>
            <ShakeOutlined style={{ fontSize: 25, color: icon === "ShakeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ShakeOutlined" ? "#1690ff":"#545454"}}>ShakeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ShareAltOutlined")} onDoubleClick={onSubmit}>
            <ShareAltOutlined style={{ fontSize: 25, color: icon === "ShareAltOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ShareAltOutlined" ? "#1690ff":"#545454"}}>ShareAltOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ShopOutlined")} onDoubleClick={onSubmit}>
            <ShopOutlined style={{ fontSize: 25, color: icon === "ShopOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ShopOutlined" ? "#1690ff":"#545454"}}>ShopOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ShoppingCartOutlined")} onDoubleClick={onSubmit}>
            <ShoppingCartOutlined style={{ fontSize: 25, color: icon === "ShoppingCartOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ShoppingCartOutlined" ? "#1690ff":"#545454"}}>ShoppingCartOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ShoppingOutlined")} onDoubleClick={onSubmit}>
            <ShoppingOutlined style={{ fontSize: 25, color: icon === "ShoppingOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ShoppingOutlined" ? "#1690ff":"#545454"}}>ShoppingOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SisternodeOutlined")} onDoubleClick={onSubmit}>
            <SisternodeOutlined style={{ fontSize: 25, color: icon === "SisternodeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SisternodeOutlined" ? "#1690ff":"#545454"}}>SisternodeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SkinOutlined")} onDoubleClick={onSubmit}>
            <SkinOutlined style={{ fontSize: 25, color: icon === "SkinOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SkinOutlined" ? "#1690ff":"#545454"}}>SkinOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SmileOutlined")} onDoubleClick={onSubmit}>
            <SmileOutlined style={{ fontSize: 25, color: icon === "SmileOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SmileOutlined" ? "#1690ff":"#545454"}}>SmileOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SolutionOutlined")} onDoubleClick={onSubmit}>
            <SolutionOutlined style={{ fontSize: 25, color: icon === "SolutionOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SolutionOutlined" ? "#1690ff":"#545454"}}>SolutionOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SoundOutlined")} onDoubleClick={onSubmit}>
            <SoundOutlined style={{ fontSize: 25, color: icon === "SoundOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SoundOutlined" ? "#1690ff":"#545454"}}>SoundOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SplitCellsOutlined")} onDoubleClick={onSubmit}>
            <SplitCellsOutlined style={{ fontSize: 25, color: icon === "SplitCellsOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SplitCellsOutlined" ? "#1690ff":"#545454"}}>SplitCellsOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("StarOutlined")} onDoubleClick={onSubmit}>
            <StarOutlined style={{ fontSize: 25, color: icon === "StarOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "StarOutlined" ? "#1690ff":"#545454"}}>StarOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SubnodeOutlined")} onDoubleClick={onSubmit}>
            <SubnodeOutlined style={{ fontSize: 25, color: icon === "SubnodeOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SubnodeOutlined" ? "#1690ff":"#545454"}}>SubnodeOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SwitcherOutlined")} onDoubleClick={onSubmit}>
            <SwitcherOutlined style={{ fontSize: 25, color: icon === "SwitcherOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SwitcherOutlined" ? "#1690ff":"#545454"}}>SwitcherOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("SyncOutlined")} onDoubleClick={onSubmit}>
            <SyncOutlined style={{ fontSize: 25, color: icon === "SyncOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "SyncOutlined" ? "#1690ff":"#545454"}}>SyncOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TableOutlined")} onDoubleClick={onSubmit}>
            <TableOutlined style={{ fontSize: 25, color: icon === "TableOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TableOutlined" ? "#1690ff":"#545454"}}>TableOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TabletOutlined")} onDoubleClick={onSubmit}>
            <TabletOutlined style={{ fontSize: 25, color: icon === "TabletOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TabletOutlined" ? "#1690ff":"#545454"}}>TabletOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TagOutlined")} onDoubleClick={onSubmit}>
            <TagOutlined style={{ fontSize: 25, color: icon === "TagOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TagOutlined" ? "#1690ff":"#545454"}}>TagOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TagsOutlined")} onDoubleClick={onSubmit}>
            <TagsOutlined style={{ fontSize: 25, color: icon === "TagsOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TagsOutlined" ? "#1690ff":"#545454"}}>TagsOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TeamOutlined")} onDoubleClick={onSubmit}>
            <TeamOutlined style={{ fontSize: 25, color: icon === "TeamOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TeamOutlined" ? "#1690ff":"#545454"}}>TeamOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ThunderboltOutlined")} onDoubleClick={onSubmit}>
            <ThunderboltOutlined style={{ fontSize: 25, color: icon === "ThunderboltOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ThunderboltOutlined" ? "#1690ff":"#545454"}}>ThunderboltOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ToTopOutlined")} onDoubleClick={onSubmit}>
            <ToTopOutlined style={{ fontSize: 25, color: icon === "ToTopOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ToTopOutlined" ? "#1690ff":"#545454"}}>ToTopOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("ToolOutlined")} onDoubleClick={onSubmit}>
            <ToolOutlined style={{ fontSize: 25, color: icon === "ToolOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "ToolOutlined" ? "#1690ff":"#545454"}}>ToolOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TrademarkCircleOutlined")} onDoubleClick={onSubmit}>
            <TrademarkCircleOutlined style={{ fontSize: 25, color: icon === "TrademarkCircleOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TrademarkCircleOutlined" ? "#1690ff":"#545454"}}>TrademarkCircleOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TrademarkOutlined")} onDoubleClick={onSubmit}>
            <TrademarkOutlined style={{ fontSize: 25, color: icon === "TrademarkOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TrademarkOutlined" ? "#1690ff":"#545454"}}>TrademarkOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TransactionOutlined")} onDoubleClick={onSubmit}>
            <TransactionOutlined style={{ fontSize: 25, color: icon === "TransactionOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TransactionOutlined" ? "#1690ff":"#545454"}}>TransactionOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TranslationOutlined")} onDoubleClick={onSubmit}>
            <TranslationOutlined style={{ fontSize: 25, color: icon === "TranslationOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TranslationOutlined" ? "#1690ff":"#545454"}}>TranslationOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("TrophyOutlined")} onDoubleClick={onSubmit}>
            <TrophyOutlined style={{ fontSize: 25, color: icon === "TrophyOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "TrophyOutlined" ? "#1690ff":"#545454"}}>TrophyOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UngroupOutlined")} onDoubleClick={onSubmit}>
            <UngroupOutlined style={{ fontSize: 25, color: icon === "UngroupOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UngroupOutlined" ? "#1690ff":"#545454"}}>UngroupOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UnlockOutlined")} onDoubleClick={onSubmit}>
            <UnlockOutlined style={{ fontSize: 25, color: icon === "UnlockOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UnlockOutlined" ? "#1690ff":"#545454"}}>UnlockOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UploadOutlined")} onDoubleClick={onSubmit}>
            <UploadOutlined style={{ fontSize: 25, color: icon === "UploadOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UploadOutlined" ? "#1690ff":"#545454"}}>UploadOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UsbOutlined")} onDoubleClick={onSubmit}>
            <UsbOutlined style={{ fontSize: 25, color: icon === "UsbOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UsbOutlined" ? "#1690ff":"#545454"}}>UsbOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UserAddOutlined")} onDoubleClick={onSubmit}>
            <UserAddOutlined style={{ fontSize: 25, color: icon === "UserAddOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UserAddOutlined" ? "#1690ff":"#545454"}}>UserAddOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UserDeleteOutlined")} onDoubleClick={onSubmit}>
            <UserDeleteOutlined style={{ fontSize: 25, color: icon === "UserDeleteOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UserDeleteOutlined" ? "#1690ff":"#545454"}}>UserDeleteOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UserOutlined")} onDoubleClick={onSubmit}>
            <UserOutlined style={{ fontSize: 25, color: icon === "UserOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UserOutlined" ? "#1690ff":"#545454"}}>UserOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UserSwitchOutlined")} onDoubleClick={onSubmit}>
            <UserSwitchOutlined style={{ fontSize: 25, color: icon === "UserSwitchOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UserSwitchOutlined" ? "#1690ff":"#545454"}}>UserSwitchOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UsergroupAddOutlined")} onDoubleClick={onSubmit}>
            <UsergroupAddOutlined style={{ fontSize: 25, color: icon === "UsergroupAddOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UsergroupAddOutlined" ? "#1690ff":"#545454"}}>UsergroupAddOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("UsergroupDeleteOutlined")} onDoubleClick={onSubmit}>
            <UsergroupDeleteOutlined style={{ fontSize: 25, color: icon === "UsergroupDeleteOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "UsergroupDeleteOutlined" ? "#1690ff":"#545454"}}>UsergroupDeleteOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("VerifiedOutlined")} onDoubleClick={onSubmit}>
            <VerifiedOutlined style={{ fontSize: 25, color: icon === "VerifiedOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "VerifiedOutlined" ? "#1690ff":"#545454"}}>VerifiedOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("VideoCameraAddOutlined")} onDoubleClick={onSubmit}>
            <VideoCameraAddOutlined style={{ fontSize: 25, color: icon === "VideoCameraAddOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "VideoCameraAddOutlined" ? "#1690ff":"#545454"}}>VideoCameraAddOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("VideoCameraOutlined")} onDoubleClick={onSubmit}>
            <VideoCameraOutlined style={{ fontSize: 25, color: icon === "VideoCameraOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "VideoCameraOutlined" ? "#1690ff":"#545454"}}>VideoCameraOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("WalletOutlined")} onDoubleClick={onSubmit}>
            <WalletOutlined style={{ fontSize: 25, color: icon === "WalletOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "WalletOutlined" ? "#1690ff":"#545454"}}>WalletOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("WhatsAppOutlined")} onDoubleClick={onSubmit}>
            <WhatsAppOutlined style={{ fontSize: 25, color: icon === "WhatsAppOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "WhatsAppOutlined" ? "#1690ff":"#545454"}}>WhatsAppOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("WifiOutlined")} onDoubleClick={onSubmit}>
            <WifiOutlined style={{ fontSize: 25, color: icon === "WifiOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "WifiOutlined" ? "#1690ff":"#545454"}}>WifiOutlined</Paragraph></div>
          </Col>
          <Col span={4} style={{ padding: '0 10px'}} onClick={() => setIcon("WomanOutlined")} onDoubleClick={onSubmit}>
            <WomanOutlined style={{ fontSize: 25, color: icon === "WomanOutlined" ? "#1690ff":"#545454" }} />
            <div><Paragraph ellipsis style={{ fontSize: 12, marginTop: 3, color: icon === "WomanOutlined" ? "#1690ff":"#545454"}}>WomanOutlined</Paragraph></div>
          </Col>
        </Row>
      </Drawer>
    </div>
  )
}


export default SelectIcon;
