import React, { useState, useEffect } from 'react';
import { PlusOutlined } from '@ant-design/icons';
import {connect} from 'dva';
import { Form, Input, Button, Drawer, InputNumber, Switch, Spin } from 'antd';
import SelectIcon from "@/pages/sys/menu/components/SelectIcon";
import AntIcon from "@/pages/common/AntIcon";

/**
 * 新增菜单
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function MenuAdd(props,ref) {
  const [ visible, setVisible ] = useState(false);
  const [isShow, setIsShow] = useState(true);
  const [ icon, setIcon ] = useState(false);
  const [form] = Form.useForm();
  const { loading, dispatch, parentId, parentName } = props;

  // 初始化
  const preInit = () => {
    form.resetFields();
    setVisible(true);
    if (dispatch) {
      dispatch({
        type: 'menu/getMaxSort',
        params: { parentId: parentId ? parentId : "0" }
      }).then(res => {
        if (res){
          form.setFieldsValue({
            sort: res.result.maxSort
          })
        }
      })
    }
  }


  // 提交表单
  const onSubmit = (values) => {
    form.validateFields()
      .then(values => {
        if (dispatch) {
          dispatch({
            type: 'menu/saveMenu',
            params: {
             ...values,
              icon: icon,
              parentId: parentId ? parentId : "0",
              isShow: isShow? '1' : '0'
            },
          }).then(() => {
            setVisible(false);
          })
        }
      })
  }

  return (
    <span>
      <span onClick={preInit} style={{ cursor: 'pointer' }}>
        {
          props.children !== undefined ?
            props.children
          :
            <Button type="primary" icon={<PlusOutlined />}>
              新建
            </Button>
        }
      </span>
      {/* 弹出窗口 */}
      <Drawer
        title="新建菜单"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={ 650 }
        footer={
          <div style={{textAlign: 'right',}}>
            <Button onClick={() => setVisible(false)} style={{ marginRight: 8 }}>
              取消
            </Button>
            <Button onClick={onSubmit} type="primary">
              确定
            </Button>
          </div>
        }
      >
        <Spin spinning={loading} >
          <Form
            {...layout}
            form={form}
            initialValues={{
              sort: 10,
              parentId: parentName
            }}
          >
            {
              parentId ?
                <Form.Item name="parentId" label="父级菜单" rules={[{ required: true, message: '菜单名称不能为空' }]}>
                  <Input disabled={true}/>
                </Form.Item>
              : null
            }
            <Form.Item name="name" label="菜单名称" rules={[{ required: true, message: '菜单名称不能为空' }]}>
              <Input />
            </Form.Item>
            <Form.Item name="enName" label="菜单标识" rules={[{ required: true, message: '菜单名称不能为空' }]}>
              <Input />
            </Form.Item>
            <Form.Item name="href" label="链接" rules={[{ required: true, message: '链接不能为空' }]}>
              <Input/>
            </Form.Item>
            <Form.Item name="sort" label="排序" rules={[{ required: true, message: '排序不能为空' }]}>
              <InputNumber min={0} step={10} />
            </Form.Item>
            <Form.Item name="icon" label="图标">
              <SelectIcon render={val => setIcon(val)} />
            </Form.Item>
            <Form.Item label="是否显示">
              <Switch defaultChecked checked={isShow} onChange={val => setIsShow(val)}/>
            </Form.Item>
            <Form.Item name="component" label="目标路由">
              <Input />
            </Form.Item>
            <Form.Item name="permission" label="授权标识">
              <Input />
            </Form.Item>
          </Form>
        </Spin>
      </Drawer>
    </span>
  )
}

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

export default connect(({menu, loading}) => ({
  loading: loading.models.menu,
}))(MenuAdd);
