import React, { useState, useEffect } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import {connect} from 'dva';
import { DeleteOutlined, QuestionCircleOutlined, ApartmentOutlined } from '@ant-design/icons';
import { Card, Row, Col, Input, Table, Tag, Switch, Popconfirm, Button } from 'antd';
import MenuAdd from "@/pages/sys/menu/components/MenuAdd";
import MenuEdit from "@/pages/sys/menu/components/MenuEdit";
import AntIcon from "@/pages/common/AntIcon";

const { Search } = Input;


/**
 * 菜单管理
 * @param props
 * @param ref
 * @returns {*}
 */
function index(props,ref) {
  const { loading, list, dispatch } = props;

  // 初始化
  useEffect(() => {
    if (dispatch) {
      dispatch({
        type: 'menu/getMenuList',
      });
    }
  },[])

  const deleteMenu = id => {
    if (dispatch) {
      dispatch({
        type: 'menu/deleteMenu',
        params: { id: id },
      })
    }
  }

  const columns = [
    {
      title: '菜单名称',
      dataIndex: 'name',
      render: (val,record) => (
        <MenuEdit menuId={record.id}>
          <Button type="link">{val} （ {record.enName} ）</Button>
        </MenuEdit>
      )
    },
    {
      title: '链接',
      dataIndex: 'href',
      render: val => <Tag>{val}</Tag>
    },
    {
      title: '图标',
      dataIndex: 'icon',
      render: val => <AntIcon type={val}/>
    },
    {
      title: '排序',
      dataIndex: 'sort',
      align: 'center'
    },
    {
      title: '隐藏',
      dataIndex: 'isShow',
      render: val => <Switch checked={val === '1'} size="small" />
    },
    {
      title: '授权',
      dataIndex: 'permission',
    },
    {
      title: '操作',
      align: 'center',
      dataIndex: 'id',
      render: (id, record) => (
        <div>
          <MenuAdd parentId={id} parentName={record.name}>
            <Button type="primary" style={{ marginLeft: 10 }} shape="circle" icon={<ApartmentOutlined />} />
          </MenuAdd>
          {/* 删除 */}
          <Popconfirm
            placement="topRight"
            onConfirm={() => deleteMenu(id)}
            title="确定要删除记录吗？"
            icon={<QuestionCircleOutlined style={{color: 'red'}}/>}
          >
            <Button type="primary" style={{ marginLeft: 10 }} danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </div>
      )
    },
  ];
  return (
    <div>
      <PageHeaderWrapper>
        <Card>
          {/* 查询 */}
          <Row style={{ marginBottom: 20 }}>
            <Col span={12}>
              <MenuAdd />
            </Col>
            <Col span={12} style={{ textAlign: 'right'}}>
              <Search placeholder="模糊搜索" style={{ maxWidth: 500 }}  onSearch={value => {
                if (dispatch) {
                  dispatch({
                    type: 'menu/getMenuList',
                    params: { keyword: value }
                  });
                }
              }} enterButton />
            </Col>
          </Row>
          {/* 列表 */}
          <Table
            key={list}
            defaultExpandAllRows
            dataSource={list}
            loading={loading}
            columns={columns}
            rowKey={ "id" }
          />
        </Card>
      </PageHeaderWrapper>
    </div>
  )
}

export default connect(({menu, loading}) => ({
  loading: loading.models.menu,
  list: menu.list,
}))(index);
