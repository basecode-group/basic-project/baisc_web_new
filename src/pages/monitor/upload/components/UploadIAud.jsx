import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { Card, Upload, message, Spin } from 'antd';
import { InboxOutlined, CopyOutlined } from '@ant-design/icons';
import copy from 'copy-to-clipboard';

const { Dragger } = Upload;

/**
 * 上传音频视频
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function UploadIAud(props,ref) {
  const { dispatch, uploadUrl, visitUrl, loading } = props;
  const [ fileList, setFileList ] = useState([]);

  useEffect(() => {
    if (dispatch) {
      dispatch({
        type: 'upload/getUploadConfig',
      });
    }
  },[])

  const uploadProps = {
    accept: "image/*",
    headers: {
      Authorization: localStorage.getItem('Authorization'),
    },
    name: 'file',
    multiple: true,
    listType: 'picture',
    action: `${uploadUrl}/sys/grant/file/uploadAud`,
    onChange(info) {
      const { status } = info.file;
      if (status !== 'uploading') {
      }
      if (status === 'done') {
        // 获取上传地址
        const { response } = info.file;
        if (response !== undefined) {
          if (response.code !== '00000') {
            info.fileList.forEach( file => {
              file.status = 'error';
            })
            setFileList(info.fileList);
            message.error(`${info.file.name} 文件上传失败，${response.msg}`)
          } else {
            setFileList(info.fileList);
            message.success(`${info.file.name} 文件上传成功.`);
          }
        } else {
          message.error(`${info.file.name} 文件上传失败.`);
        }
      } else if (status === 'error') {
        message.error(`${info.file.name} 文件上传失败.`);
      }
    },
    defaultFileList: [...fileList],
    showUploadList: {
      showRemoveIcon: true,
    },
    onPreview: file => {
      if (file.response.result.filesMap.file !== undefined) {
        copy(visitUrl + '/' + file.response.result.filesMap.file[0].path);
        message.success("文件地址复制成功");
      } else {
        message.error("文件复制失败！");
      }
    }
  };

  return (
    <span>
      <Card>
        <Spin spinning={loading}>
          <div style={{ width: '80%', margin: '0 auto' , marginTop: 20, marginBottom: 30 }}>
            <Dragger {...uploadProps}>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">单击或将文件拖到该区域以上传</p>
            <p className="ant-upload-hint">
              支持单次或批量上传。 严格禁止非开发工作人员上传其他带文件
            </p>
          </Dragger>
          </div>
        </Spin>
        </Card>
    </span>
  )
}


export default connect(({upload, loading}) => ({
  loading: loading.models.upload,
  uploadUrl: upload.uploadUrl,
  visitUrl: upload.visitUrl,
}))(UploadIAud);
