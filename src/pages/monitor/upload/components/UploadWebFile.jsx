import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { Card, Input, Typography, Button, message, Spin } from 'antd';
import { CloudUploadOutlined, SwapOutlined } from '@ant-design/icons';
import {CopyToClipboard} from 'react-copy-to-clipboard';

const { Paragraph } = Typography;
const { Search } = Input;

/**
 * 上传网络文件
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function UploadWebFile(props,ref) {
  const { dispatch, loading, fileUrl } = props;

  const uploadFile = url => {
    if (dispatch) {
      dispatch({
        type: 'upload/uploadWebFile',
        params: {
          fileNetWorkPath: url,
        }
      }).then(res => {
        if (res !== undefined) {
          message.success('文件上传成功');
        }
      });
    }
  }

  return (
    <span>
      <Card>
        <div style={{ width: '80%', margin: '0 auto' , marginTop: 20, marginBottom: 30 }}>
          <Spin spinning={loading}>
            <Search
              placeholder="网络文件地址"
              enterButton={
                <span><CloudUploadOutlined />上传文件</span>
              }
              size="large"
              onSearch={value => uploadFile(value)}
            />
            <div style={{ textAlign: 'center', margin: '20px 0'}}>
              <SwapOutlined style={{ transform: 'rotate(90deg)', fontSize: 18 }}/>
            </div>
            <div style={{ textAlign: 'center', margin: '20px 0'}}>
              <CopyToClipboard text={fileUrl}
                               onCopy={() => message.success("文件地址复制成功")} >
                <Button type="link">
                  <Paragraph ellipsis style={{ color: '#c41d7f' }}>
                    {fileUrl}
                  </Paragraph>
                </Button>
              </CopyToClipboard>
            </div>
          </Spin>
        </div>
      </Card>
    </span>
  )
}


export default connect(({upload, loading}) => ({
  loading: loading.models.upload,
  fileUrl: upload.fileUrl
}))(UploadWebFile);
