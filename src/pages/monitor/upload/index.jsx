import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import UploadImage from "@/pages/monitor/upload/components/UploadImage";
import UploadFile from "@/pages/monitor/upload/components/UploadFile";
import UploadIAud from "@/pages/monitor/upload/components/UploadIAud";
import UploadWebFile from "@/pages/monitor/upload/components/UploadWebFile";

/**
 * 文件上传服务
 * @param props
 * @param ref
 * @returns {*}
 */
function index(props,ref) {
  const [ tabKey, setTabKey ] = useState("1");

  return (
    <span>
      <PageHeaderWrapper
        subTitle={"点击上传预览文件，可直接复制URL"}
        content={ "文件上传服务，基于FastDFS开发的文件服务系统，此处调用文件上传接口，具体请参考 swagger 接口。" }
        tabList={[
          { key: '1', tab: '上传图片' },
          { key: '2', tab: '上传文件' },
          { key: '3', tab: '上传音频/视频' },
          { key: '4', tab: '上传网络文件' },
        ]}
        tabActiveKey={tabKey}
        onTabChange={key => setTabKey(key)}
      >
        {
          tabKey === '1' ?
            <UploadImage />
          : tabKey === '2' ?
            <UploadFile />
          : tabKey === '3' ?
            <UploadIAud />
          : tabKey === '4' ?
            <UploadWebFile />
          : null
        }
      </PageHeaderWrapper>
    </span>
  )
}


export default connect(({upload, loading}) => ({
  loading: loading.models.upload,
}))(index);
