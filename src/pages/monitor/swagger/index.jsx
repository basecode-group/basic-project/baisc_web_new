import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Spin, Tooltip, message, Select, Card} from 'antd';
import logo from '@/assets/image/swagger_logo.png';
import IconFont from "@/pages/common/IconFont";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import SwaggerUI from 'swagger-ui-react';
import 'swagger-ui-react/swagger-ui.css';

const { Option } = Select;

/**
 * swagger ui
 * @param props
 * @param ref
 * @returns {*}
 */
function index(props,ref) {
  const { dispatch, resource, visitUrl, loading } = props;
  const [ json, setJson ] = useState({});
  const [selectUrl, setSelectUrl] = useState('');

  useEffect(() => {
    if (dispatch) {
      dispatch({
        type: 'swagger/getSwaggerResource',
      }).then(res => {
        const url = res.filter(item => 'users-route' === item.name)[0].url;
        // const url = res.length > 0 ? res[0].url : '';
        setSelectUrl(url);
      })
    }
  },[])

  return (
    <span>
      <PageHeaderWrapper title={
        <div style={{ marginTop: 20 }}>
          <img style={{ height: 45 }} src={logo} />
        </div>
      } subTitle={
        <Tooltip placement="right" title={<span>复制token</span>}>
          <div>
            <CopyToClipboard text={localStorage.getItem('Authorization')} onCopy={() => message.success('复制成功')}>
              <IconFont type="icon-tokenguanli" style={{ fontSize: 20, color: '#08c' }}/>
            </CopyToClipboard>
          </div>
        </Tooltip>
      } extra={
        <div style={{ marginTop: 30 }}>
            <span style={{ marginRight: 10 }}>
              {/* eslint-disable-next-line max-len */}
              <span style={{ fontWeight: 500, fontSize: 16, marginRight: 15 }}>Select a spec</span>
                <Select
                  style={{ minWidth: 260 }}
                  value={selectUrl}
                  key={selectUrl}
                  onChange={val => {
                    setSelectUrl(val);
                  }}
                >
                  {
                    resource.map((item, key) => (
                      <Option value={item.url} key={key}>{item.name}</Option>
                    ))
                  }
                </Select>
              </span>
        </div>
      }>
        <Spin spinning={loading}>
          <Card className="swagger-content" style={{ background: '#fafafa', margin: '0 auto' }}>
            <SwaggerUI
              // key={selectUrl}
              url={selectUrl}
              spec={json}
              docExpansion={'none'}
              onComplete={(swaggerUi) => {
                swaggerUi.preauthorizeApiKey('1', localStorage.getItem('Authorization'));
              }}
              requestInterceptor={req => {
                req.url = req.url.replace("http://127.0.0.1", "http://123.57.173.38:8010");
                req.headers = {
                  Authorization: localStorage.getItem('Authorization')
                }
                return req;
              }}
            />
          </Card>
        </Spin>
      </PageHeaderWrapper>
    </span>
  )
}


export default connect(({swagger, loading}) => ({
  loading: loading.models.swagger,
  resource: swagger.resource,
  visitUrl: swagger.visitUrl,
}))(index);
