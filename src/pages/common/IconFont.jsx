import React, { Component } from 'react';
import { createFromIconfontCN } from '@ant-design/icons';

const IconFonts = createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_1533614_45u39cy686o.js',
});

/**
 *  使用第三方图库，https://www.iconfont.cn/
 */
// eslint-disable-next-line react/prefer-stateless-function
export default class IconFont extends Component {
  render() {
    return (
      <IconFonts
        style={{ ...this.props.style }}
        type={this.props.type}
        onClick={this.props.onClick}
      />
    );
  }
}
