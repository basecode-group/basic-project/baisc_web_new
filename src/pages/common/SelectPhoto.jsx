import React, {useState, useEffect} from 'react';
import {Avatar, Modal, Row, Col, Radio, Input} from 'antd';
import {UserOutlined} from '@ant-design/icons';
import { TwitterPicker } from 'react-color';
import icon1 from '@/assets/avatars/1.jpg';
import icon2 from '@/assets/avatars/2.jpg';
import icon3 from '@/assets/avatars/3.jpg';
import icon4 from '@/assets/avatars/4.jpg';
import icon5 from '@/assets/avatars/5.jpg';
import icon6 from '@/assets/avatars/6.jpg';
import icon7 from '@/assets/avatars/7.jpg';
import icon8 from '@/assets/avatars/8.jpg';
import icon9 from '@/assets/avatars/9.png';
import icon10 from '@/assets/avatars/10.jpeg';
import icon11 from '@/assets/avatars/11.jpeg';
import icon12 from '@/assets/avatars/12.png';

/**
 *  选择头像
 *  回调方法 function renderFun(String)
 *  初始化  photo
 *  是否启用编辑 isEdit false
 *  尺寸，  size small larger
 */
function SelectPhoto(props, ref) {
  const [visible, setVisible] = useState(false);
  const [select, setSelect] = useState(true);
  const [showData, setShowData] = useState({});
  const [background, setBackground] = useState('#cccccc');
  const [fontColor, setFontColor] = useState('#FFFFFF');
  const [fontValue, setFontValue] = useState('');
  const [key, setKey] = useState(0);
  const { renderFun, photo, size, isEdit } = props;

  // 页面加载
  useEffect(() => {
    let photoJson = photo;
    if (photoJson === undefined || photoJson === '') {
      photoJson = "{\"select\":true,\"key\":0}";
    }
    try {
      const rsPhoto = JSON.parse(photoJson);
      if (rsPhoto.select) {
        // 初始化数据
        setSelect(rsPhoto.select);
        setKey(rsPhoto.key);
        setShowData({
          select: rsPhoto.select,
          key: rsPhoto.key,
        })
      } else {
        // 初始化数据
        setSelect(rsPhoto.select);
        setBackground(rsPhoto.background);
        setFontColor(rsPhoto.fontColor);
        setFontValue(rsPhoto.fontValue);
        setShowData({
          select: rsPhoto.select,
          background: rsPhoto.background,
          fontColor: rsPhoto.fontColor,
          fontValue: rsPhoto.fontValue,
        });
      }
      // eslint-disable-next-line no-empty
    } catch (err) {
    }
  },[]);

  // 初始化
  const preInit = () => {
    if (isEdit) {
      setVisible(true);
    }
  }

  // 提交
  const onSubmit = () => {
    const iconInfo = {select: select};
    if (select) {
      iconInfo['key'] = key;
    } else {
      iconInfo['background'] = background;
      iconInfo['fontColor'] = fontColor;
      iconInfo['fontValue'] = fontValue;
    }
    // 返回结果
    const rsPhoto = JSON.stringify(iconInfo);
    if (renderFun !== undefined) {
      renderFun(rsPhoto);
    }
    setVisible(false);
  }

  return (
    <span>
      <span style={{...props.style, cursor: 'pointer'}} onClick={preInit}>
        {
          showData.select ?
            <span>
              {
                size === "small" ? defaultIcon[showData.key]
                  : size === "mini" ? defaultIcon25[showData.key]
                  : defaultIcon50[showData.key]
              }
            </span>
          :
            <Avatar size={
                size !== undefined && size === 'small' ? 32
                  : size !== undefined && size === 'mini' ? 25 : 50
              }
              style={{
                backgroundColor: showData.background,
                color: showData.fontColor,
              }}
            >
              {showData.fontValue !== '' ? (
                <div>{showData.fontValue}</div>
              ) : (
                <UserOutlined
                  type="user"
                  style={{
                    fontSize: size !== undefined && size === 'small' ? 18 : 25,
                  }}
                />
              )}
            </Avatar>
        }
      </span>
      <Modal
        title="选择头像"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false)}
        width={700}
      >
        {/* 头像 */}
        <Row gutter={10}>
          <Col span={24} style={{textAlign: 'center'}}>
            {
              select ? defaultIcon50[key]
                :
                <Avatar
                  size={50}
                  style={{
                    cursor: 'pointer',
                    backgroundColor: background,
                    color: fontColor,
                  }}
                >
                  {
                    fontValue !== '' ? <div>{fontValue}</div>
                     : <UserOutlined style={{ fontSize: 25 }} />
                  }
                </Avatar>
            }
          </Col>
        </Row>

        {/* 选择 */}
        <div style={{marginTop: 20, textAlign: 'center',}}>
          <Radio.Group onChange={val => setSelect(val.target.value)} defaultValue={select}>
            <Radio.Button value={true}>系统头像</Radio.Button>
            <Radio.Button value={false}>自定义头像</Radio.Button>
          </Radio.Group>

          {/* 选择区域 */}
          {
            select ?
              <div>
                <Row gutter={10} style={{marginTop: 20, textAlign: 'center',}}>
                  {
                    defaultIcon.map((item, key) => (
                      <Col md={3} xs={6} key={key} style={{margin: '8px 0 8px 0', cursor: 'pointer',}}
                           key={key} onClick={() => setKey(key)}
                      >{item}</Col>
                    ))
                  }
                </Row>
              </div>
            :
              <Row gutter={10} style={{ marginTop: 20 }}>
                <Col md={12} xs={24} style={{ marginBottom: 10 }}>
                  <div style={{ marginLeft: 60 }}>
                    <TwitterPicker
                      width={205}
                      triangle="hide"
                      colors={[...defaultColors, '#cccccc']}
                      onChange={color => setBackground(color.hex)}
                      color={background}
                    />
                    <div style={{marginLeft: -45, fontWeight: 700, color: '#999',}}>
                      {' '}
                      BackgroundColor
                    </div>
                  </div>
                </Col>
                <Col md={12} xs={24} style={{ marginBottom: 10 }}>
                  <div style={{ marginLeft: 60 }}>
                    <TwitterPicker
                      width={205}
                      triangle="hide"
                      onChange={color => setFontColor(color.hex)}
                      colors={[...defaultColors, '#ffffff']}
                      color={fontColor}
                    />
                    <div style={{marginLeft: -55, fontWeight: 700, color: '#999',}}>
                      {' '}
                      FontColor
                    </div>
                  </div>
                </Col>
                <Col span={24} style={{ marginBottom: 20 }}>
                  <Row>
                    <Col span={11} style={{textAlign: 'right', fontWeight: 700, color: '#999', marginTop: 5 }}>
                      ICON:&nbsp;&nbsp;&nbsp;&nbsp;
                    </Col>
                    <Col span={12} style={{ textAlign: 'left' }}>
                      <Input
                        style={{ width: 100 }}
                        placeholder="ICON"
                        value={fontValue}
                        onChange={e => setFontValue(e.target.value)}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
          }
        </div>
      </Modal>
    </span>
  )
}


const defaultIcon25 = [
  <Avatar size={25} key={-2}>
    <UserOutlined />
  </Avatar>,
  <Avatar size={25} style={{backgroundColor: '#00a2ae'}}>
    Admin
  </Avatar>,
  <Avatar size={25} style={{backgroundColor: '#FF6900'}}>
    USER
  </Avatar>,
  <Avatar size={25} style={{color: '#f56a00', backgroundColor: '#fde3cf'}}>
    U
  </Avatar>,
  <Avatar size={25} src={icon12}/>,
  <Avatar size={25} src={icon1}/>,
  <Avatar size={25} src={icon2}/>,
  <Avatar size={25} src={icon3}/>,
  <Avatar size={25} src={icon4}/>,
  <Avatar size={25} src={icon5}/>,
  <Avatar size={25} src={icon6}/>,
  <Avatar size={25} src={icon7}/>,
  <Avatar size={25} src={icon8}/>,
  <Avatar size={25} src={icon9}/>,
  <Avatar size={25} src={icon10}/>,
  <Avatar size={25} src={icon11}/>,
];

const defaultIcon = [
  <Avatar key={-2} icon={<UserOutlined />}/>,
  <Avatar style={{backgroundColor: '#00a2ae'}}>Admin</Avatar>,
  <Avatar style={{backgroundColor: '#FF6900'}}>USER</Avatar>,
  <Avatar
    style={{
      color: '#f56a00',
      backgroundColor: '#fde3cf',
    }}
  >
    U
  </Avatar>,
  <Avatar src={icon12}/>,
  <Avatar src={icon1}/>,
  <Avatar src={icon2}/>,
  <Avatar src={icon3}/>,
  <Avatar src={icon4}/>,
  <Avatar src={icon5}/>,
  <Avatar src={icon6}/>,
  <Avatar src={icon7}/>,
  <Avatar src={icon8}/>,
  <Avatar src={icon9}/>,
  <Avatar src={icon10}/>,
  <Avatar src={icon11}/>,
];

const defaultIcon50 = [
  <Avatar size={50} key={-2}>
    <UserOutlined style={{fontSize: 25}}/>
  </Avatar>,
  <Avatar size={50} style={{backgroundColor: '#00a2ae'}}>
    Admin
  </Avatar>,
  <Avatar size={50} style={{backgroundColor: '#FF6900'}}>
    USER
  </Avatar>,
  <Avatar
    size={50}
    style={{
      color: '#f56a00',
      backgroundColor: '#fde3cf',
    }}
  >
    U
  </Avatar>,
  <Avatar size={50} src={icon12}/>,
  <Avatar size={50} src={icon1}/>,
  <Avatar size={50} src={icon2}/>,
  <Avatar size={50} src={icon3}/>,
  <Avatar size={50} src={icon4}/>,
  <Avatar size={50} src={icon5}/>,
  <Avatar size={50} src={icon6}/>,
  <Avatar size={50} src={icon7}/>,
  <Avatar size={50} src={icon8}/>,
  <Avatar size={50} src={icon9}/>,
  <Avatar size={50} src={icon10}/>,
  <Avatar size={50} src={icon11}/>,
];

// 基础颜色集
const defaultColors = [
  '#FF6900',
  '#FCB900',
  '#7BDCB5',
  '#00D084',
  '#8ED1FC',
  '#0693E3',
  '#00a2ae',
  '#EB144C',
  '#F78DA7',
  '#9900EF',
];

export default SelectPhoto;
