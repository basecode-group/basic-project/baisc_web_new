export default {
  navTheme: 'light',
  // 拂晓蓝
  primaryColor: '#1890ff',
  layout: 'sidemenu',
  contentWidth: 'Fluid',
  fixedHeader: true,
  autoHideHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  menu: {
    locale: false,
    disableLocal: true,
  },
  title: 'Mea-Web',
  pwa: false,
  iconfontUrl: '',
  http: 'http://123.57.173.38:8010'
};
